Diagrammatic Monte Carlo for the Fröhlich polaron
=================================================

These codes are intended to supplement our lecture notes which are published as
[SciPost Phys. Lect. Notes 2][10].
Please refer to this main text for an in-depth discussion of the method,
particularly the update scheme and strategies regarding the Fourier transforms.
This README focuses on the technical details in order to run the codes and
reproduce the data shown in the figures of the main text.

Project Structure
-----------------

We provide three essentially independent codes for the simulations considered in
the various parts of the main text:

  * [`Two_Level_System`][1] relates to the two-level toy model considered in
    part II of the main text. Due to the relative simplicity and negligible
    runtime required to obtain converged results, only a single-core executable
    is generated.
  * [`Froehlich_Greenfun_Bare`][2] implements the bare diagrammatic
    expansion of the Green function of the Fröhlich polaron as discussed in part
    III of the main text. Single-core and MPI-capable executables can be built.
  * The [`Froehlich_Selfenergy`][3] code serves both to sample the bare
    expansion of the self-energy (cf. part IV) as well as its _bold_ expansion
    based on the NCA propagator rather than the free one (cf. part V). Separate
    executables for either version using MPI are built along with a single-core
    version of the former.
    
  [1]: Two_Level_System#two-level-system
  [2]: Froehlich_Greenfun_Bare#fr%C3%B6hlich-polaron-bare-expansion-for-the-green-function
  [3]: Froehlich_Selfenergy#fr%C3%B6hlich-polaron-self-energy-formalism

Requirements
------------

These codes are based on the [ALPSCore](https://github.com/ALPSCore/ALPSCore)
library. Refer to [their website](http://alpscore.org/) for installation
instructions. At the time of this writing, ALPSCore imposed the following system
requirements:

  * C++ compiler, along with suitable MPI wrappers (e.g. OpenMPI)
  * CMake build system (version 2.8.12 or later),
  * Boost headers and `program_options` library (version 1.54 or later),
  * HDF5 library version 1.8.x (version 1.10 does _not_ work, see below).
  
Beyond these, our codes require

  * a C++11-capable compiler,
  * FFTW3 library for the Fourier transform in the self-energy code (both bare
    and bold).

#### Regarding Boost and C++11

Boost will conditionally compile enums to use C++11's scoped enums when
available and otherwise fall back to type-unsafe enums. Since Boost headers are
used directly in our code and included indirected through ALPSCore headers, one
has be cautious of linker errors due to a mismatch of C++11 and C++03 symbols.
We recommend using a C++11 compiler throughout (GCC 6+ uses C++11 by default, or
provide the `-DCMAKE_CXX_FLAGS="-std=c++11"` flag to CMake) to build a local
copy of Boost, and compile ALPSCore and our codes. ALPSCore can be advised to use
the local version of Boost by providing the
`-DBOOST_ROOT=/path/to/boost/install` flag to CMake when building the ALPSCore
libraries.

### Regarding HDF5 1.10

There is a known upstream bug in HDF5 1.10 that persisted at least until version
1.10.1. We advise to use version 1.8.19 for the time being. If compatibility to
the 1.10-format is urgently required, 1.10 may be patched to resolve the bug.
Refer to ALPSCore [issue #290](https://github.com/ALPSCore/ALPSCore/issues/290)
for details regarding this workaround.

Building and Installation
-------------------------

### Building and installing ALPSCore

Detailed instructions on
[how to build ALPSCore](https://github.com/ALPSCore/ALPSCore/wiki/Installation)
can be fournd in the project's wiki. The procedure revolves around the following:

    $ cd alpscore
    $ mkdir build.tmp && cd build.tmp
    $ cmake ..
    $ make -jN
    $ make test
    $ make install
    
Replace `N` with the number of processors you want to use to build, e.g. `-j8`.
You may want to specify additional flags to `cmake`:

  * `-DCMAKE_INSTALL_PREFIX=$HOME/.local`, or another custom install location.
    This is required if you don't have permission to write at the default
    install prefix (`/usr/local`). Mind that ALPSCore installs a CMake script
    that has to be picked up by CMake when building our codes. Thus, any
    non-standard install location needs to be matched by a
    `-DCMAKE_PREFIX_PATH=<...>` flag when configuring the client code.
  * If a local version of boost has been install
    ([see above](#regarding-boost-and-c11)), point CMake to it by specifying
    `-DBOOST_ROOT=/path/to/boost/install`. Otherwise your local version may not
    be found, or be shadowed by an incompatible version.

### Building our client codes

Our codes also use CMake to configure the build environment. The procedure is
analogous to ALPSCore's, e.g.:

    $ cd Froehlich_Selfenergy
    $ mkdir build && cd build
    $ cmake ..
    $ make -jN all

Again, provide `-DCMAKE_PREFIX_PATH=/path/to/alpscore/install` if ALPSCore has
been installed in a non-standard location. Refer to the READMEs in the
subdirectories of the individual codes for additional flags to customize their
behavior.

General Usage
-------------

### Running a simulation from a parameter file

Simulation parameters may be specified in an INI-style parameter file, e.g.

    sweeps = 1000000
    thermalization = 10000
    timelimit = 18000
    checkpointtime = 3600

followed by simulation-specific parameters. The parameter files which have been
use to produce some of the figures in the main text are provided in the `params`
subdirectory of the individual codes. Consult the READMEs in the subdirectories
for details. Assuming this file is saved as `job.ini`, the simulation is started
by running (in the case of the `Froehlich_Greenfun_Bare` code):

    $ ./froehlich_green_bare_single job.ini

which would run it on a single core. After an initial thermalization phase of
10000 sweeps, the code would run for another one million sweeps while measuring
the observables after each one. The code would run for at most 5 hours (=18000
seconds), which is useful when working on a cluster with wallclock constraints.
A `timelimit` of 0 means that no time limit is imposed at all.

When the desired number of sweeps has been performed or the job ran out of time,
the results are written to `job.out.h5`. Every hour (=3600 seconds), a
checkpoint file `job.clone.h5` is written out which contains the measurement
data as well as the configuration and can be used to restart the code in case
more samples are desired or the job aborted permaturely due to a node failure. A
final checkpoint is also written before the code terminates. 
`checkpointtime = 0` disables the periodic checkpoints but will still produce
the final checkpoint.

#### Using MPI parallelization

To run the simulation on multiple cores or even nodes, use the executable
without the `_single` suffix in combination with the MPI wrapper script:

    $ mpiexec -n $NUM_MPI ./froehlich_green_bare job.ini
    
This will start `$NUM_MPI` independent simulations with identical parameters
(with the exception of the random seed: each MPI process uses its own RNG,
seeded with the `SEED` parameter plus its MPI rank).

Each MPI process will produce independent checkpoint files, enumerated by the
MPI rank, e.g. `job.clone<RANK>.h5`, but only one output file `job.out.h5` will
be written which contains the collected results from all MPI processes.

Periodically, the total number of measurements among all the processes will be
accumulated. If it exceeds the value specified in the `sweeps` parameter (or the
`timelimit` is reached), the simulation will terminate. Since this check
requires synchronization of the processes, it is not done after each sweep but
rather at intervals between `Tmin` and `Tmax` seconds (which may be specified in
the parameters file). Thus, when working on a cluster with wallclock
constraints, one should reserve at least `timelimit+Tmax` seconds to avoid
premature forceful termination of the job.

Keep in mind that the thermalization phase has to be done for each MPI process
independently, i.e. `$NUM_MPI` × `thermalization` sweeps will be carried out in
total before measurement samples can be taken.

### Overriding parameters and defaults

The complete list of all configurable parameters can be obtained by calling the
executables with the `--help` flag:

    $ ./froehlich_green_bare --help
    DiagMC simulation of the Froehlich polaron: Green function via bare expansion
      --N_hist arg (=1000)                  number of updates until counting 
                                            configuration towards histograms
      --N_loop arg (=1)                     number of histogram samples gathered 
                                            before measuring
      --SEED arg (=42)                      PRNG seed
      --Tmax arg (=600)                     maximum time to check if simulation has
                                            finished
      --Tmin arg (=1)                       minimum time to check if simulation has
                                            finished
      --alpha arg (=5)                      coupling constant
    [...]

Most parameters provide default values, but some don't to force the user to
consciously specify them (e.g. the chemical potential).

Any parameter may be overridden on the command line, e.g.

    $ ./froehlich_green_bare_single job.ini --sweeps=10000
    
Parameters provided on the command line take precedence over those in the
parameter file or in the parameters stored in a checkpoint (see below).

### Resuming a simulation from a checkpoint

In case the simulation terminated because it reached the `timelimit`, or the job
terminated due to e.g. a node failure, one can resume the simulation from the
checkpoint:

    $ ./froehlich_green_bare_single job.clone.h5

Both codes that support checkpointing (`Froehlich_Greenfun_Bare` and
`Froehlich_Selfenergy`) will produce the exact same Markov chain after resuming
from a checkpoint compared to an uninterrupted simulation. Thus, periodic
checkpoints can also be instrumental in debugging.

In case MPI is used, any one checkpoint file can be specified on the command
line and the individual MPI processes will find their respective checkpoint file
automatically:

    $ mpiexec -n $NUM_MPI ./froehlich_green_bare job.clone0.h5
    
However, `$NUM_MPI` needs to match the amount used in the previous run.

When resuming, the `timelimit` is basically reset. In case the simulation
terminated because it had taken `sweeps` measurements but the results turned out
unsatisfactory, one can override the `sweeps` parameter on the command line to
sample further:

    $ ./froehlich_green_bare_single job.clone.h5 --sweeps=10000000

Not all parameters can be overridden when resuming from a checkpoint, though.
Changing the coupling constant `--alpha` mid-way would be plainly wrong, thus
such an attempt would be ignored.

### Understanding the output

Simulation results are output in a HDF5 data file `job.out.h5` containing the
simulation parameters, measurements of the observables along with binning
analyses, and derived quantities (normalized Green function / self-energy) along
with the Jackknife metadata. This file follows the standard format used in the
example codes of the ALPSCore project; please refer to their documentation for
details.

In addition, our Green function and self-energy codes generate ASCII text files
holding various quantities of interest which can be directly input in a
visualization program such as `gnuplot`. These file are discribed in more detail
in the subdirectory READMEs.

Acknowledgements
----------------

These codes make use of the [ALPSCore library][4], based on the original
[ALPS project][5]. ALPSCore makes use of the [HDF5 data format][6], as well as
the [Boost C++ libraries][7]. Further, we rely on the [FFTW3 library][8] for the
Fourier transform necessary for the Dyson equation in the self-energy formalism.
Finally, the [Faddeeva package][9]'s implementation of the Dawson function is
used in the calculation of the first order of the self-energy.

  [4]: http://alpscore.org/
  [5]: http://alps.comp-phys.org/
  [6]: https://www.hdfgroup.org/
  [7]: http://www.boost.org/
  [8]: http://www.fftw.org/
  [9]: http://ab-initio.mit.edu/wiki/index.php/Faddeeva_Package
  [10]: https://doi.org/10.21468/SciPostPhysLectNotes.2

License
-------

Copyright © 2017  Jonas Greitemann and Lode Pollet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available in the
file [LICENSE.txt](LICENSE.txt).
