// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "tls.hpp"
#include "copyright.hpp"

#include <string>
#include <iostream>
#include <stdexcept>

#include <alps/mc/api.hpp>
#include <alps/mc/stop_callback.hpp>


int main(int argc, const char *argv[]) {
    try {
        typedef alps::parameters_type<tls_sim>::type params_type;
        params_type parameters(argc, argv, "/parameters"); // reads from HDF5 if need be

        tls_sim::define_parameters(parameters);

        if (parameters.help_requested(std::cerr))
            return 1; // Stop if help requested.

        // legal
        std::cout << tls_sim::code_name() << std::endl;
        print_copyright(std::cout);

        tls_sim sim(parameters);

        sim.run(alps::stop_callback(std::size_t(parameters["timelimit"])));

        using alps::collect_results;
        alps::results_type<tls_sim>::type results = collect_results(sim);
        sim.print_exact_answer(std::cout);
        std::cout << results << std::endl;

        // Saving to the output file
        std::string output_file = parameters["outputfile"];
        alps::hdf5::archive ar(output_file, "w");
        ar["/parameters"] << parameters;
        ar["/simulation/results"] << results;

        sim.print_update_statistics(std::cout);
    } catch (std::exception const & e) {
        std::cerr << "Caught exception: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
