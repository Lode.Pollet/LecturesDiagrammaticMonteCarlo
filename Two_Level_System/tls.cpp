// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "tls.hpp"

#include <alps/params/convenience_params.hpp>

#include <boost/lambda/lambda.hpp>


std::string tls_sim::code_name() {
    return "Tutorial example: Simulation of Two Level System";
}


void tls_sim::define_parameters(parameters_type& params) {
    // Adds the parameters of the base class
    alps::mcbase::define_parameters(params);
    // Adds the convenience parameters (for save/load)
    // followed by the problem specific parameters
    alps::define_convenience_parameters(params)
        .description(tls_sim::code_name())
        .define<size_t>("sweeps", 10000000, "Total sweeps")
        .define<size_t>("thermalization", 1000000, "Number of thermalization sweeps")
        .define<double>("h_mag", "Field in z direction")
        .define<double>("gamma", "Field in x direction")
        .define<double>("beta", "inverse temperature")
        .define<double>("p_flip", 0.2, "probability for using SPINFLIP update")
    ;
}

tls_sim::tls_sim(parameters_type const & params)
    : alps::mcbase(params)
    , sweeps(0)
    , thermalization_sweeps(size_t(parameters["thermalization"]))
    , total_sweeps(size_t(parameters["sweeps"]))
    , beta(double(parameters["beta"]))
    , h_mag(double(parameters["h_mag"]))
    , gamma(double(parameters["gamma"]))
    , p_flip(double(parameters["p_flip"]))
    , nv(0)
{
    n0 = (random() < 0.5 ? -1 : 1); // initial occupation
    nv = 0;
    cur_mag = n0;
    conf.clear();
    update_statistics.resize(5);
    for (int i=0; i < 5; i++)
        update_statistics[i].resize(REMOVE+1);
    Hist.resize(20);
    for (size_t i = 0; i < statistics_count; i++) {
        for (size_t j = 0; j < update_count; j++)
            update_statistics[i][j] = 0.;
    }
    for (size_t i= 0; i < Hist.size(); i++)
        Hist[i] = 0.;

    p_insert = p_flip + (1. - p_flip) / 2.;

    measurements
        << alps::accumulators::FullBinningAccumulator<double>("mag_z")
        << alps::accumulators::FullBinningAccumulator<double>("mag_z_bis")
        << alps::accumulators::FullBinningAccumulator<double>("mag_x")
        ;
    std::cout << "Simulation parameters:\n";
    for (auto kv : parameters) {
        std::cout << kv.first << " = ";
        print(std::cout, kv.second, true);
        std::cout << '\n';
    }
    std::cout << "\n\n\n";
    std::cout << "Initial values : n0 = " << n0 << " nv = " << nv << "\n\n";
}

void tls_sim::update() {
    double q = random();
    if (q < p_flip) {
        int a = FLIP_UPDATE();
        update_statistics[total_attempted][FLIP] += 1.;
        update_statistics[a][FLIP] += 1.;
    }
    else if (q < p_insert) {
        int a = INSERT_UPDATE();
        update_statistics[total_attempted][INSERT] += 1.;
        update_statistics[a][INSERT] += 1.;
    }
    else {
        int a = REMOVE_UPDATE();
        update_statistics[total_attempted][REMOVE] += 1.;
        update_statistics[a][REMOVE] += 1.;

    }
    if (nv/2 < Hist.size())
        Hist[nv/2] += 1.;
    return;
}

void tls_sim::measure() {
    sweeps++;
    if (sweeps > thermalization_sweeps) {
        measurements["mag_z"] << double(n0);
        measurements["mag_z_bis"] << cur_mag;
        measurements["mag_x"] << -1. * nv / beta / gamma;
    }
    return;
}

double tls_sim::fraction_completed() const {
    return (sweeps < thermalization_sweeps ? 0.
            : (sweeps - thermalization_sweeps) / double(total_sweeps));
}

void tls_sim::print_conf() {
    std::cout << "\n Configuration : " << nv << "\t" << n0 << "\t"
              << cur_mag << "\n";
    for (auto it = conf.begin(); it != conf.end(); ++it) {
        std::cout << it->get_time() << "\t" << it->before() << "\t"
                  << it->after() << "\n";
    }
}

int tls_sim::FLIP_UPDATE() {
    // special treatment if there are no kinks
    if (nv == 0) {
        double ratio = exp(beta * h_mag * 2 * n0);
        if (Metropolis(ratio)) {
            // Metropolis accepted
            n0 = -n0;
            cur_mag *= -1;
            return accepted;
        }
        else {
            return rejected;
        }
    }
    return impossible;
}

int tls_sim::INSERT_UPDATE() {
    if (nv == 0) {
        double t1 = random() * beta;
#ifdef WITH_LOG
        double dt;
        if (h_mag * n0 < 0) {
            dt = log(random())/(2*h_mag * n0);
            if (dt > beta)
                return impossible;
        }
        else {
            dt = random() * beta;
        }
#else
        double dt = random() * beta;
#endif
        double t2 = t1 + dt;
        if (t2 > beta)
            t2 -= beta;  // maybe should be done better with an exponential random number cancelling the weights
        double wx,wy, pxy, pyx;
#ifdef WITH_LOG
        if (h_mag * n0 < 0) {
            wx = 1.;
            wy = gamma * gamma;
            pxy = -2.*h_mag * n0/beta;
            pyx = 0.5;
        }
        else {
            wx = exp(-dt * h_mag * n0);
            wy = gamma * gamma * exp(dt * h_mag * n0);
            pxy = 1./(beta * beta);
            pyx = 0.5;
        }
#else
        wx = exp(-dt * h_mag * n0);
        wy = gamma * gamma * exp(dt * h_mag * n0);
        pxy = 1./(beta * beta);
        pyx = 1./(nv + 2.);
#endif

        double ratio = wy * pyx/wx/pxy;
        if (Metropolis(ratio)) {
            nv = 2;
            vertex_type v1;
            v1.time(t1);
            vertex_type v2;
            v2.time(t2);
            v1.before(n0);
            v1.after(-n0);
            v2.before(-n0);
            v2.after(n0);
            cur_mag -= 2 * n0 * dt/beta;
            if (t2 > t1) {
                std::list<vertex_type>::iterator itl = conf.insert(conf.end(), v2);
                itl = conf.insert(itl, v1);
            }
            else {
                std::list<vertex_type>::iterator itl = conf.insert(conf.end(), v1);
                itl = conf.insert(itl, v2);
                n0 = - n0;
            }
            return accepted;
        }  else {
            return rejected;
        }
    } // .... nv == 0
    else { // vertices already present
        // select time of new vertex
        double t1 = random() * beta;
        // find where it is chronologically in the list
        std::list<vertex_type>::iterator it1 = conf.begin();
        double tinit = 0.;
        double tfin = it1->get_time();
        int nocc = it1->before();
        while (!(t1 > tinit && tfin > t1)) {
            ++it1;
            tinit = tfin; tfin = it1->get_time();
            if (it1 == conf.end()) {
                tfin = beta;
                it1 = conf.begin();
            }
            nocc = -nocc;
        }
        // time interval over which nothing changes
        double dt2 = it1->get_time() - t1;
        if (dt2 < 0) dt2 += beta;
        // select second time
#ifdef WITH_LOG
        double dt;
        if (h_mag * nocc < 0) {
            dt = log(random())/(2*h_mag * nocc);
            if (dt > dt2)
                return impossible;
        }
        else {
            dt = random() * dt2;
        }
#else
        double dt = random() * dt2;
#endif
        double t2 = t1 + dt;
        if (t2 > beta) t2 -= beta;
        double wx, wy, pxy, pyx;
#ifdef WITH_LOG
        if (h_mag * nocc < 0) {
            wx = 1.;
            wy = gamma * gamma;
            pxy = (-2*h_mag * nocc)/(beta);
            pyx = 1./(nv + 2.);
        }
        else {
            wx = exp(-dt * h_mag * nocc);
            wy = gamma * gamma * exp(dt * h_mag * nocc);
            pxy = 1./(beta * dt2);
            pyx = 1./(nv + 2.);
        }
#else
        wx = exp(-dt * h_mag * nocc);
        wy = gamma * gamma * exp(dt * h_mag * nocc);
        pxy = 1./(beta * dt2);
        pyx = 1./(nv + 2.);
#endif
        double ratio = wy * pyx/wx/pxy;
        if (Metropolis(ratio)) {
            nv += 2;
            vertex_type v1;
            v1.time(t1);
            vertex_type v2;
            v2.time(t2);
            v1.before(nocc);
            v1.after(-nocc);
            v2.before(-nocc);
            v2.after(nocc);
            cur_mag -= 2 * nocc * dt/beta;
            if (t2 > t1) {
                if (t2 > it1->get_time() ) {
                    std::list<vertex_type>::iterator itl = conf.insert(conf.end(), v2);
                    itl = conf.insert(itl, v1);
                }
                else {
                    std::list<vertex_type>::iterator itl = conf.insert(it1, v2);
                    itl = conf.insert(itl, v1);
                }
            }
            else {
                std::list<vertex_type>::iterator itl = conf.insert(conf.end(), v1);
                itl = conf.insert(it1, v2);
                n0 = - n0;
            }
            return accepted;
        }  else {
            return rejected;
        }
    }
    return impossible;
}


int tls_sim::REMOVE_UPDATE() {
    if (nv < 2)
        return impossible;
    int vrtx = static_cast<int>(random() * nv); // choose random vertex
    std::list<vertex_type>::iterator it1 = conf.begin();
    for (int i=0; i < vrtx; i++)
        ++it1;
    std::list<vertex_type>::iterator it2 = it1;
    ++it2;
    if (it2 == conf.end()) it2 = conf.begin();
    double dt = it2->get_time() - it1->get_time(); if (dt < 0) dt += beta;
    int nold = it1->after();
    double wx = gamma * gamma * exp(-dt * h_mag * nold);
    double wy = exp(dt * h_mag * nold);
    double pxy = 1./(nv * 1.);
    double pyx;
    if (nv == 2) {
#ifdef WITH_LOG
        if (it1->before() * h_mag < 0) {
            wx = gamma*gamma;
            wy = 1.;
            pyx = (-2*h_mag * it1->before())/beta;
        } else {
            pyx = 1./(beta * beta);
        }

#else
        pyx = 1./(beta * beta);
#endif

    }
    else {
#ifdef WITH_LOG
        if (it1->before() * h_mag < 0) {
            wx = gamma * gamma;
            wy = 1.;
            pxy = 1./(nv * 1.);
            pyx = -2. * h_mag * it1->before()/beta;
        } else {
            std::list<vertex_type>::iterator it3 = it2; ++it3;
            if (it3 == conf.end())
                it3 = conf.begin();
            double dt2 = it3->get_time() - it1->get_time();
            if (dt2 < 0)
                dt2 += beta;
            pyx = 1./(beta * dt2);
        }
#else
        std::list<vertex_type>::iterator it3 = it2; ++it3;
        if (it3 == conf.end())
            it3 = conf.begin();
        double dt2 = it3->get_time() - it1->get_time();
        if (dt2 < 0)
            dt2 += beta;
        pyx = 1./(beta * dt2);
#endif
    }
    double ratio = wy * pyx/wx/pxy;
    if (Metropolis(ratio)) {
        nv -= 2;
        cur_mag -= 2 * it1->after() * dt/beta;
        if (it2->get_time() < it1->get_time())
            n0 = -n0;
        conf.erase(it1);
        conf.erase(it2);
        return accepted;
    }  else {
        return rejected;
    }
}


void tls_sim::print_exact_answer(std::ostream& os) const {
    double x = sqrt(gamma * gamma + h_mag * h_mag);
    os << "# Exact answer : \n";
    os << "# Magnetization(x) : " << -gamma/x * tanh(beta * x) << "\n";
    os << "# Magnetization(z) : " << -h_mag/x * tanh(beta * x) << "\n\n";
    return;
}


void tls_sim::print_update_statistics(std::ostream& os) const
{
    std::vector<std::string> name;
    name.push_back("SPIN_FLIP  ");
    name.push_back("INSERT     ");
    name.push_back("REMOVE     ");
    os << "\n\n# UPDATE STATISTICS";
    os << "\n" << "# row 1 : all updates"
       << "\n" << "# row 2 : possible updates"
       << "\n" << "# row 3 : rejected updates"
       << "\n" << "# row 4 : accepted updates"
       << "\n" << "# row 5 : acceptance factor (possible)"
       << "\n" << "# row 6 : acceptance factor (total)";
    for (int i = 0; i < update_count; i++) {
        os << "\n" << name[i] << "\t"
           << update_statistics[total_attempted][i] << "\t"
           << (update_statistics[total_attempted][i] - update_statistics[impossible][i]) << "\t"
           << update_statistics[rejected][i] << "\t"
           << update_statistics[accepted][i] << "\t"
           << update_statistics[accepted][i] / (update_statistics[total_attempted][i]
                                                - update_statistics[impossible][i]) << "\t"
           << update_statistics[accepted][i] / update_statistics[total_attempted][i];
    }
    os << "\n# Histogram : ";
    double s = 0.;
    for (int i=0 ; i < Hist.size(); i++)
        s += Hist[i];
    for (int i =0; i < Hist.size(); i++)
        std::cout << Hist[i]/s << "\t";
    std::cout << "\n";
}
