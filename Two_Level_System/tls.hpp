// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <alps/hdf5/archive.hpp>
#include <alps/hdf5/vector.hpp>

#include <alps/params.hpp>
#include <alps/accumulators.hpp>
#include <alps/mc/mcbase.hpp>
#include <alps/mc/api.hpp>

#include <boost/function.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <vector>
#include <string>
#include <list>


class vertex_type {
public:
    void time(const double t) { mtime = t;}
    void before(const int n) { n_before = n;}
    void after(const int n) { n_after = n;}
    double get_time() const { return mtime;}
    int before() const { return n_before;}
    int after() const { return n_after;}
private:
    double mtime;
    int n_before;
    int n_after;
};


enum statistics_tag {
    impossible,
    rejected,
    accepted,
    total_attempted,
    statistics_count
};


class tls_sim : public alps::mcbase {
public:
    enum {
        FLIP,
        INSERT,
        REMOVE,
        update_count
    };

    tls_sim(parameters_type const & params);

    void update();
    void measure();
    double fraction_completed() const;

    static std::string code_name();
    static void define_parameters(parameters_type&);

    void print_conf();
    int FLIP_UPDATE();
    int INSERT_UPDATE();
    int REMOVE_UPDATE();
    void print_update_statistics(std::ostream&) const;
    void print_exact_answer(std::ostream&) const;

private:
    size_t sweeps;
    size_t thermalization_sweeps;
    size_t total_sweeps;
    double beta;
    double h_mag;
    double gamma;
    double p_flip, p_insert;

    // internal variables
    size_t nv;      // number of vertices
    int n0;         // occupation number through periodic boundaries in imag time
    double cur_mag; // current magnetization (z)
    std::vector<std::vector<double> > update_statistics;
    std::vector<double> Hist;

    std::list<vertex_type> conf; // configuration stored as a list

    bool Metropolis(double x) {
        return x > 1 || random() < x;
    }
};
