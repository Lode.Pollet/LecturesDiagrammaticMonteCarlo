Two-Level System
================

Please refer to the [README in the parent directory][1] for general information
on our codes. Refer to part II of the main text for a discussion of the model,
update scheme, and results.

  [1]: ../README.md#diagrammatic-monte-carlo-for-the-fr%C3%B6hlich-polaron

<div align="center">
<img src="http://latex.codecogs.com/svg.latex?\dpi{120}&space;H=h\sigma_z&plus;\Gamma\sigma_x" />
</div>

Executables
-----------

  * `two_level_system`: serial executable, no checkpointing

Configuration Options
---------------------

  * `-DCMAKE_BUILD_TYPE=(Release | Debug)`: `Debug` compiles unoptimized code
    with debug symbols (`-g`); `Release` compiles optimized code and should be
    used in a production setting.

Parameters
----------

Consult `./two_level_system --help` for a complete list of supported parameters
and their default values.

#### Model parameters
  * `beta`: inverse temperature _β_
  * `gamma`: transverse field _Γ_ (in _x_-direction)
  * `h_mag`: parallel field _h_ (in _z_-direction)

#### Monte Carlo parameters
  * `thermalization`: number of update sweeps before starting to measure; this
    has to be done for each MPI process separately
  * `sweeps`: number of update sweeps (and interwoven measurements) to be
    carried out after thermalization
  * `p_flip` (double, \[0,1\]): probability to select the SPINFLIP update; the
    probabilities for the INSERT and REMOVE update are inferred evenly from the
    remainder, i.e. `p_insert = p_remove = (1 - p_flip)/2`.

#### Simulation control 
  * `timelimit`: seconds after which simulation will shutdown upon next check,
    even if `sweeps` have not been completed; 0 means no time limit imposed

### Output files
  * `job.out.h5`: HDF5 file containing ALPSCore observables and binning data

### Example parameter files

  * `low_temp.ini`: simulation at _β_=10
  * `high_temp_noflip.ini`: simulation at _β_=0.1 without the SPINFLIP update,
    exhibits large autocorrelation time for _σ\_z_ observable
  * `high_temp.ini`: same as above, but with SPINFLIP update turned on. This
    cures the update problem (cf. II E in the main text).
