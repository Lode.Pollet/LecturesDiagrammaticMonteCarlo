cmake_minimum_required(VERSION 2.8.12)
cmake_policy(SET CMP0060 NEW)
project(two_level_system CXX)

SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

add_executable(${PROJECT_NAME} tls.cpp main.cpp)
find_package(ALPSCore REQUIRED mc accumulators params)

# Use ALPSCore_LIBRARIES variable to link to ALPSCore 
target_link_libraries(${PROJECT_NAME} ${ALPSCore_LIBRARIES})
