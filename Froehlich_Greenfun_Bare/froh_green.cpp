// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "froh_green.hpp"

#include <algorithm>

#include <alps/params/convenience_params.hpp>

#include <boost/lambda/lambda.hpp>


std::string diagmc::code_name() {
    return "DiagMC simulation of the Froehlich polaron: "
        "Green function via bare expansion";
}


void diagmc::define_parameters(parameters_type& params)
{
    // If the parameters are restored, they are already defined
    if (params.is_restored()) {
        return;
    }

    // Adds the parameters of the base class
    alps::mcbase::define_parameters(params);
    // Adds the convenience parameters (for save/load)
    // followed by the problem specific parameters
    alps::define_convenience_parameters(params)
        .description(diagmc::code_name())
        .define<unsigned long>("thermalization", "Number of thermalization sweeps")
        .define<unsigned long>("sweeps", "Total sweeps")
        .define<double>("mass", 1.0, "electron mass")
        .define<double>("omega_p", 1.0, "phonon frequency")
        .define<double>("alpha", 5.0, "coupling constant")
        .define<double>("mu_zero", "chemical potential")
        .define<double>("p_init", 0., "external momentum")
        .define<size_t>("max_order", 100, "maximum expansion order")
        .define<double>("p_insert", 0.2, "probability for insert update")
        .define<double>("p_remove", 0.2, "probability for remove update")
        .define<double>("p_swap", 0.2, "probability for swap update")
        .define<double>("p_extend", 0.2, "probability for extend update")
        .define<double>("p_to_fake", 0.2, "probability for to_fake update")
        .define<double>("p_from_fake", 0.5, "probability for from_fake update")
        .define<double>("p_change_time", 0.5, "probability for change_time update")
        .define<double>("max_time", 20.0, "maximum time")
        .define<size_t>("number_of_times", 100, "number of time grid points")
        .define<size_t>("N_loop", 1,
                        "number of histogram samples gathered before measuring")
        .define<size_t>("N_hist", 1000,
                        "number of updates until counting configuration "
                        "towards histograms")
    ;
}

diagmc::diagmc(parameters_type const & params, size_t seed_offset)
    : alps::mcbase(params, seed_offset)
    , rng(long(parameters["SEED"]) + seed_offset)
    , uni_dist(0,1)
    , sweeps(0)
    , thermalization_sweeps((unsigned long)(parameters["thermalization"]))
    , total_sweeps((unsigned long)(parameters["sweeps"]))
    , N_hist(size_t(parameters["N_hist"]))
    , N_loop(size_t(parameters["N_loop"]))
    , number_of_times(size_t(parameters["number_of_times"]))
    , max_time(double(parameters["max_time"]))
    , mu_zero(double(parameters["mu_zero"]))
    , alpha(double(parameters["alpha"]))
    , omega_p(double(parameters["omega_p"]))
    , p_init(double(parameters["p_init"]))
    , mass(double(parameters["mass"]))
    , max_order(double(parameters["max_order"]))
    , update_statistics(boost::extents[statistics_count][update_count])
{
    vertex_amplitude_sq = 4 * M_PI * alpha * pow(omega_p, 1.5) / sqrt(2 * mass);
    mu = mu_zero;
    measurements
        << alps::accumulators::FullBinningAccumulator<std::vector<double> >("Greenfun")
        << alps::accumulators::FullBinningAccumulator<std::vector<double> >("Greenfun_0")
        << alps::accumulators::FullBinningAccumulator<std::vector<double> >("Greenfun_1")
        << alps::accumulators::FullBinningAccumulator<double>("MCfake")
        << alps::accumulators::FullBinningAccumulator<double>("order")
        << alps::accumulators::MeanAccumulator<std::vector<double> >("order_frequencies")
        ;
    std::cout << std::setprecision(12);

    initialize_update_prob();

    initialize_coordinates();
    order_frequencies.resize(max_order);
    hist_green.resize(number_of_times);
    hist_green_zero.resize(number_of_times);
    hist_green_one.resize(number_of_times);

    init_diagram();
}


void diagmc::initialize_update_prob(update_tag begin, update_tag end) {
    double norm = 0.;
    for (size_t upd = begin; upd < end; ++upd) {
        if (parameters.defined("p_" + update_names[upd])) {
            update_prob[upd] = double(parameters["p_" + update_names[upd]]);
            if (update_prob[upd] < 0) {
                throw std::runtime_error("Negative update probability: "
                                         + update_names[upd]);
            }
        } else {
            update_prob[upd] = 0.;
        }
        norm += update_prob[upd];
    }
    if (abs(norm - 1.) > 1e-10) {
        std::cerr << "Update probabilities don't add up to one.\n"
                  << "Renormalizing..." << std::endl;
        for (size_t upd = begin; upd < end; ++upd) {
            update_prob[upd] /= norm;
            parameters["p_" + update_names[upd]] = update_prob[upd];
        }
    }
    norm = 0.;
    for (size_t upd = begin; upd < end; ++upd) {
        norm += update_prob[upd];
        update_prob_cuml[upd] = norm;
    }
}


void diagmc::initialize_update_prob() {
    initialize_update_prob(insert, from_fake);
    initialize_update_prob(from_fake, update_count);
}


void diagmc::initialize_coordinates() {
    bin_coord_time.resize(number_of_times);
    bin_size_time.resize(number_of_times);
    dbin_radial_time = max_time / number_of_times;
    for (int i = 0; i < number_of_times; i++) {
        double r1 = i * dbin_radial_time;
        double r2 = (i+1) * dbin_radial_time;
        bin_coord_time[i] = 0.5 * (r1 + r2);
        bin_size_time[i] = r2 - r1;
    }
}

void diagmc::init_diagram() {
    diagram.clear();
    double tau_init = 1.0;
    diagram_p(0) = p_init;
    for (int i = 1; i < dim; i++) diagram_p(i) = 0.;
    diagram_time = tau_init;
    order = 0;
#ifdef DEBUGMODE
    diagram_weight = propagator(diagram_p, mu, diagram_time);

    std::cout << "\t# Initial time     of the fake diagram : " <<  diagram_time << "\n";
    std::cout << "\t# Initial momentum of the fake diagram : " <<  diagram_p    << "\n";
#endif
}


void diagmc::reset_update_statistics() {
    for (Matrix_index i= 0; i < statistics_count; i++) {
        for (Matrix_index j = 0; j < update_count; j++) {
            update_statistics[i][j] = 0;
        }
    }
}

void diagmc::reset_hists() {
    for (int32_t i = 0; i < number_of_times; i++) {
        hist_green[i] = 0.;
        hist_green_zero[i] = 0.;
        hist_green_one[i] = 0.;
    }
    std::fill(order_frequencies.begin(), order_frequencies.end(), 0.);
    MCfake = 0.0;
    hist_order = 0.;
}


void diagmc::fill_hists() {
    int itime = index_time( diagram_time);
    hist_green[itime] += 1.;
    hist_order += order;
    order_frequencies[order] += 1.;
    if (order == 0) {
        MCfake += 1.;
        hist_green_zero[itime] += 1.;
    }
    if (order == 1) hist_green_one[itime] += 1.;
}

void diagmc::measure() {
    sweeps++;
    if (sweeps < thermalization_sweeps)
        return;
    std::vector<double> GF(number_of_times, 0.);
    std::vector<double> GF0(number_of_times, 0.);
    std::vector<double> GF1(number_of_times, 0.);
    for (size_t j=0; j < number_of_times; j++) {
        GF[j] = hist_green[j]/bin_size_time[j];
        GF0[j] = hist_green_zero[j]/bin_size_time[j];
        GF1[j] = hist_green_one[j]/bin_size_time[j];
    }
    for (auto it = order_frequencies.begin(); it != order_frequencies.end(); ++it)
        *it /= N_loop;
    measurements["Greenfun"] << GF;
    measurements["Greenfun_0"] << GF0;
    measurements["Greenfun_1"] << GF1;
    measurements["MCfake"] << MCfake;
    measurements["order"] << hist_order / N_loop;
    measurements["order_frequencies"] << order_frequencies;
}

double diagmc::fraction_completed() const {
    return (sweeps > thermalization_sweeps
            ? (sweeps - thermalization_sweeps) / double(total_sweeps)
            : 0.);
}

double diagmc::get_green_one(const double tau) const {
    // calculation of G(p=0, tau) with a single phonon line arc in three dimensions.
    double prefactor = vertex_amplitude_sq * exp(mu * tau) * sqrt(mass) / (pow(2*M_PI, 1.5));
    double g = 0.;
    int Npoints = 1000;
    for (int i = 0; i < Npoints; i++) {
        double h1 = tau/Npoints;
        double tau_1 = i * h1;
        double g2 = 0.;
        for (int j = 0; j < Npoints; j++) {
            double h2 = (tau - tau_1) / Npoints;
            double tau_2 =  (j+0.5) * h2;
            //cout << "get_green_one : " << tau << "\t" << tau_1 << "\t" << tau_2 << "\t" <<
            g2 += (exp(- omega_p * tau_2  )-1.) / sqrt(tau_2) * h2;
        }
        g += (2 * sqrt(tau - tau_1) + g2) * h1;
    }
    g *= prefactor;
    return g;
}

void diagmc::test_conf(const Diagram_type& d) const {
#ifdef DEBUGMODE
    double weight = 1.;
    double tprev = 0.;
#endif

    if (order > 0) {
        Diagram_type::const_iterator it = d.begin();
        Diagram_type::const_iterator itend = d.end(); --itend;
        if ( (it->in() - diagram_p).r_sq() > 1e-8)
            throw invalid_configuration("external momentum different from "
                                        "incoming momentum at first vertex.");
        if ( (it->in() - itend->out()).r_sq() > 1e-8 )
            throw invalid_configuration("In and out momenta not equal.");
        if (d.size()/2 != order)
            throw invalid_configuration("Problem with size/order");
        Diagram_type::const_iterator itn = it; ++itn;
        for (; it!=itend; ++it, ++itn) {
            if (it->time() > diagram_time)
                throw invalid_configuration("Time too large on vertex.");
            if (!(it->v() == (it->link())->v() ))
                throw invalid_configuration("Different interaction lines on "
                                            "linked vertices.");
            if ( (it->out()-itn->in()).r_sq() > 1e-8)
                throw invalid_configuration("Change of momentum on a "
                                            "propagator line?");
            if (itn->time() <= it->time())
                throw invalid_configuration("Chronological order broken in diagram.");
            if ( (it->link())->time() > it->time()) {
                if ((it->in() - it->out() - it->v() ).r_sq()  > 1.e-8)
                    throw invalid_configuration("Momentum conservation not ok "
                                                "at vertex with outgoing "
                                                "interaction line");
            }
            else {
                if ((it->in() - it->out() + it->v() ).r_sq()  > 1.e-8)
                    throw invalid_configuration("Momentum conservation not ok "
                                                "at vertex with incoming "
                                                "interaction line");
            }
#ifdef DEBUGMODE
            weight *= propagator(it->in(), mu, it->time() - tprev) ;
            tprev = it->time();
            if (it->link()->time() > it->time())
                weight *= (exp(-omega_p * (it->link()->time() - it->time()))
                           / (pow(2*M_PI, dim)) * vertex_amplitude_sq
                           / it->v().r_sq());
            std::cout << "# Weight : " << it->time() << "\t" << weight << "\n";
#endif
        }
#ifdef DEBUGMODE
        Diagram_type::const_iterator itp = itend; --itp;
        weight *= (propagator(diagram_p, mu, diagram_time - itend->time())
                   * propagator(itend->in(), mu, itend->time() - itp->time()));
        std::cout << "# Weight : " << itend->time() << "\t" << weight << "\n";
        if (std::fabs(weight - diagram_weight) > 1e-8) {
            std::cout << weight << "\t" << diagram_weight << "\t"
                      << diagram_time << "\t" << itend->time() << "\n";
            throw invalid_configuration("Weight of diagram not OK.\n");
        }
#endif
    }
    else {
#ifdef DEBUGMODE
        weight = propagator(diagram_p, mu, diagram_time);
        if (std::fabs(weight - diagram_weight) > 1e-8) {
            std::cout << weight << "\t" << diagram_weight << "\t"
                      << diagram_time << "\n";
            throw invalid_configuration("Weight of diagram not OK.\n");
        }
#endif
    }
}
