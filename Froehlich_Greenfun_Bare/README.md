Fröhlich Polaron: Bare Expansion for the Green Function
=======================================================

Please refer to the [README in the parent directory][1] for general information
on our codes. Refer to part III of the main text for a discussion of the model,
update scheme, and results.

  [1]: ../README.md#diagrammatic-monte-carlo-for-the-fr%C3%B6hlich-polaron

Executables
-----------

  * `froehlich_green_bare_single`: serial executable, supports checkpointing
  * `froehlich_green_bare`: parallelized with MPI, supports checkpointing

Configuration Options
---------------------

  * `-DDIAG=(LIST | AVL | LIST_STACK)`: select data structure to be used to
    represent the diagram, as discussed in Sec. III D of the main text:
    - `LIST`: doubly-linked list (default),
    - `AVL`: binary self-balancing (AVL) search tree,
    - `LIST_STACK`: doubly-linked list of vertices combined with contiguously
      stored (but unsorted) vector of iterators for drawing vertices randomly.
  * `-DDEBUGMODE=1`: enable verbose debug mode; prints diagnostics for each and
    every update.
  * `-DCMAKE_BUILD_TYPE=(Release | Debug)`: `Debug` compiles unoptimized code
    with debug symbols (`-g`); `Release` compiles optimized code and should be
    used in a production setting.

Parameters
----------

Consult `./froehlich_green_bare --help` for a complete list of supported
parameters and their default values.

#### Model parameters
  * `alpha`: electron-phonon coupling constant _α_
  * `mass`: electron mass _m_
  * `omega_p`: phonon frequency (Einstein dispersion) _ω\_ph_
  * `mu_zero`: chemical potential _μ_
  * `p_init`: external momentum

#### Discretization parameters
  * `max_time`: maximum imaginary time _τ_ of the diagram
  * `number_of_times`: number of equidistant grid points in imaginary time

  Note that during sampling the imaginary time is continuous but the Green
  function is measured according to a histogram in imaginary time.

#### Monte Carlo parameters
  * `thermalization`: number of update sweeps before starting to measure; this
    has to be done for each MPI process separately
  * `sweeps`: number of update sweeps (and interwoven measurements) to be
    carried out after thermalization
  * `N_hist`: number of elementary updates to be carried out before histogram
    counters are increased; increase this if autocorrelations are strong
  * `N_loop`: number of histogram samples to be taken before the histograms are
    measured and the sweep is complete; 1 sweep = `N_loop` × `N_hist` elementary
    updates; increase this if the measurement of the histograms takes too much
    memory bandwidth (at the the expense of error analysis)
  * `max_order`: maximum diagram order to consider; can be choosen high enough
    to not matter at all; imposed only for technical reasons
  * `p_*`: proposal probabilities for the individual update types; these consist
    of two sets:
      - for fake diagrams: `p_from_fake`, `p_change_time`
      - for real diagrams: `p_insert`, `p_remove`, `p_swap`, `p_extend`, `p_to_fake`

    These probabilities are _not_ cumulative, but actual probabilities, and
    should add up to unity within each set. If the latter is violated, the
    probabilities are renormalized accordingly and the simulation will still run.

#### Simulation control 
  * `Tmin`, `Tmax` (MPI only): control the schedule of the checks of whether the
    simulation has finished yet
  * `timelimit`: seconds after which simulation will shutdown upon next check,
    even if `sweeps` have not been completed; 0 means no time limit imposed
  * `checkpointtime`: seconds between periodic checkpoints; 0 means no periodic
    checkpoints, but final checkpoint will still be written before simulation
    terminates.

### Output files
  * `job.out.h5`: HDF5 file containing ALPSCore observables, binning data, and
    Jackknife analyses of normalized quantities
  * `job.green`: full Green function data; column format:
    - τ: imaginary time
    - ⟨-G(τ)⟩: mean value of negative full Green function
    - σ(-G(τ)): error on the mean of the above
    - ⟨ln(-G(τ))⟩: mean value of the logarithm of the negative Green function
    - σ(ln(-G(τ))): error on the mean of the above
  * `job.green0` / `job.green1`: bare / first-order Green function data; column
    format:
    - τ: imaginary time
    - ⟨-G_{0/1}(τ)⟩: mean value of negative bare / first-order Green function
    - σ(-G_{0/1}(τ)): error on the mean of the above
    - -G_{0/1}(τ): analytical exact value of the negative bare / first-order
      Green function
  * `job.updates`: Monte Carlo update statistics

### Example parameter files

  * `fig9a.ini`: parameters to reproduce Fig. 9a of the main text, i.e. _α_=5,
    _μ_=-6, `max_time`=15
  * `fig9b.ini`: parameters to reproduce Fig. 9b of the main text, i.e. _α_=5,
    _μ_=-5.6, `max_time`=40
