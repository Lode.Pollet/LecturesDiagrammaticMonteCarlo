// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "froh_green.hpp"

#include <string>
#include <sstream>

#include <alps/utilities/fs/remove_extensions.hpp>


void diagmc::print_params(std::ostream& os) const {
    os << "Simulation parameters:\n";
    for (auto kv : parameters) {
        os << kv.first << " = ";
        print(os, kv.second, true);
        os << '\n';
    }
    os << "\n\n\n";
}


void diagmc::print_update_statistics(std::ostream& os) const {
    std::vector<std::string> name;
    os << std::setprecision(10);
    name.push_back("INSERT           ");
    name.push_back("REMOVE           ");
    name.push_back("SWAP             ");
    name.push_back("EXTEND           ");
    name.push_back("TO_FAKE          ");
    name.push_back("FROM_FAKE        ");
    name.push_back("CHANGE_TIME      ");
    os << "\n\n# UPDATE STATISTICS";
    os << "\n" << "# col 1 : all updates"
       << "\n" << "# col 2 : impossible updates"
       << "\n" << "# col 3 : rejected updates"
       << "\n" << "# col 4 : accepted updates"
       << "\n" << "# col 5 : acceptance factor with respect to Metropolis ratio only"
       << "\n" << "# col 6 : acceptance factor with respect to all attempts (esp rejections due to p < p_F are taken into account here).\n";
    for (int i = 0; i < update_count; i++) {
        os << "\n" << name[i]
           << "\t" << update_statistics[total_attempted][i]
           << "\t" << update_statistics[impossible][i]
           << "\t" << update_statistics[rejected][i]
           << "\t" << update_statistics[accepted][i]
           << "\t" << update_statistics[accepted][i] / (update_statistics[total_attempted][i] - update_statistics[impossible][i])
           << "\t" << update_statistics[accepted][i] / update_statistics[total_attempted][i];
    }
    os << std::endl;
}

void diagmc::save(alps::hdf5::archive & ar) const {
    /* Test configuration before saving:
     * if something went wrong since the last checkpoint, we don't
     * want to corrupt the last healthy checkpoint, too.
     */
    test_conf();

    // Most of the save logic is already implemented in the base class
    alps::mcbase::save(ar);

    ar["checkpoint/sweeps"] << sweeps;

    ar["checkpoint/update_statistics"] << update_statistics;
#ifdef DEBUGMODE
    ar["checkpoint/diagram_weight"] << diagram_weight;
#endif

    ar["checkpoint/order"] << order;
    ar["checkpoint/diagram_time"] << diagram_time;
    ar["checkpoint/diagram_p"] << diagram_p;

    // random number engine
    std::ostringstream engine_ss;
    engine_ss << rng;
    ar["checkpoint/random"] << engine_ss.str();

    // saving current diagram
    ar["checkpoint/diagram"] << diagram;
}

void diagmc::load(alps::hdf5::archive & ar) {
    alps::mcbase::load(ar);
    // parameters object gets restored in mcbase::save

    // Do not restore the total_sweeps parameter -- we may want to
    // override it when starting from the checkpoint by supplying
    // the --sweeps=N flag.

    // these params cannot change mid-simulation and should be restored
    // from the checkpoint
    thermalization_sweeps = (unsigned long)(parameters["thermalization"]);
    mass = double(parameters["mass"]);
    omega_p = double(parameters["omega_p"]);
    alpha = double(parameters["alpha"]);
    mu_zero = double(parameters["mu_zero"]);
    p_init = double(parameters["p_init"]);
    max_order = double(parameters["max_order"]);
    max_time = double(parameters["max_time"]);
    number_of_times = size_t(parameters["number_of_times"]);
    N_loop = size_t(parameters["N_loop"]);
    N_hist = size_t(parameters["N_hist"]);

    initialize_update_prob();

    ar["checkpoint/sweeps"] >> sweeps;

    ar["checkpoint/update_statistics"] >> update_statistics;

#ifdef DEBUGMODE
    ar["checkpoint/diagram_weight"] >> diagram_weight;
#endif

    ar["checkpoint/order"] >> order;
    ar["checkpoint/diagram_time"] >> diagram_time;
    ar["checkpoint/diagram_p"] >> diagram_p;
    ar["checkpoint/diagram"] >> diagram;

    // random number engine
    std::string engine_str;
    ar["checkpoint/random"] >> engine_str;
    std::istringstream engine_ss(engine_str);
    engine_ss >> rng;

#ifdef DEBUGMODE
    print_diagram();
#endif
    test_conf();
}


void diagmc::output_final(const alps::results_type<diagmc>::type & results,
                          alps::hdf5::archive& ar)
{
    std::string basename = alps::fs::remove_extensions(parameters["outputfile"]);

    double Z_fake = ((1 - propagator(diagram_p, mu, max_time))
                     / (dispersion(diagram_p, mu)));
    // Jackknife analyses
    using alps::accumulators::result_wrapper;
    const result_wrapper& normed_0 = Z_fake*results["Greenfun_0"]/results["MCfake"];
    const result_wrapper& normed_1 = Z_fake*results["Greenfun_1"]/results["MCfake"];
    const result_wrapper& normed   = Z_fake*results["Greenfun"]  /results["MCfake"];
    const result_wrapper& logged   = log(normed);

    // save to archive
    ar["/simulation/normed/Greenfun_0"] << normed_0;
    ar["/simulation/normed/Greenfun_1"] << normed_1;
    ar["/simulation/normed/Greenfun"] << normed;
    ar["/simulation/normed/log(Greenfun)"] << logged;

    // write ASCII files for convenience
    std::ofstream f_updatestat(basename + ".updates");
    print_update_statistics(f_updatestat);
    f_updatestat.close();
    std::ofstream fmom0(basename + ".green0");
    print_times_hist_green(fmom0, 0, normed_0);
    fmom0.close();
    std::ofstream fmom1(basename + ".green1");
    print_times_hist_green(fmom1, 1, normed_1);
    fmom1.close();
    std::ofstream fmom(basename + ".green");
    print_times(fmom, normed, logged);
    fmom.close();
}

void diagmc::print_times_hist_green(std::ostream& os, const int nr,
                                    const alps::accumulators::result_wrapper& normed) const
{
    if (nr == 0) {
        for (int i=0; i < number_of_times; i++) {
            os << bin_coord_time[i] << "\t"
               << normed.mean<std::vector<double> >()[i] << "\t"
               << normed.error<std::vector<double> >()[i] << "\t"
               << propagator(diagram_p, mu, bin_coord_time[i]) << "\n";
        }
    }
    else if (nr == 1) {
        for (int i=0; i < number_of_times; i++) {
            os << bin_coord_time[i] << "\t"
               << normed.mean<std::vector<double> >()[i] << "\t"
               << normed.error<std::vector<double> >()[i] << "\t"
               << get_green_one(bin_coord_time[i]) << "\n";
        }
    }
    else {
        os << "# Invalid argument " << nr << "to print_times_hist_green.\n";
    }
    return;
}


void diagmc::print_times(std::ostream& os,
                         const alps::accumulators::result_wrapper& normed,
                         const alps::accumulators::result_wrapper& logged) const
{
    for (int i=0; i < number_of_times; i++) {
        os << bin_coord_time[i] << "\t"
           << normed.mean<std::vector<double> >()[i] << "\t"
           << normed.error<std::vector<double> >()[i] << "\t"
           << logged.mean<std::vector<double> >()[i] << "\t"
           << logged.error<std::vector<double> >()[i] << "\n";
    }
    return;
}


void diagmc::print_diagram() {
    print_diagram(diagram);
}

void diagmc::print_diagram(Diagram_type& d) {
    std::cout << "\n# PRINTING DIAGRAM : "
              << "\n# ------------------ "
              << "\n# Nr elements : " << d.size() << "\t" << order
              << "\n";
    if (order == 0) return;
    for (Diagram_type::iterator it = d.begin(); it != d.end(); ++it)
        std::cout << *it << "\n";
    std::cout << "\n# ASSOCIATIONS :\n";
    for (Diagram_type::iterator it = d.begin(); it!= d.end(); ++it) {
        std::cout << it->time() << "\t" << it->link()->time() << "\t";
        if (it->time() > (it->link())->time())
            std::cout << (it->in() - it->out() + it->v()).r_sq() << "\n";
        if (it->time() < (it->link())->time())
            std::cout << (it->in() - it->out() - it->v()).r_sq() << "\n";
    }
    std::cout << "\n\n\n";
    return;
}
