// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "froh_green.hpp"
#include <utility>
#include <iterator>


void diagmc::update() {
    reset_hists();
    for (size_t j = 0; j < N_loop; ++j) {
        for (size_t i = 0; i < N_hist; ++i) {
            int a = -1;
            double q = random();
            if (order == 0) { // fake diagram updates only
#ifdef DEBUGMODE
                if (q < update_prob_cuml[from_fake]) {
                    std::cout << "\nFROM_FAKE : ";
                    a = FROM_FAKE();
                    std::cout << "\n RESULT OF FROM_FAKE : " << a;
                } else {
                    std::cout << "\nCHANGE_TIME : ";
                    a = CHANGE_TIME();
                    std::cout << "\n RESULT OF CHANGE_TIME : " << a;
                }
#else
                if (q < update_prob_cuml[from_fake]) {
                    a = FROM_FAKE();
                    update_statistics[total_attempted][from_fake] += 1.;
                    update_statistics[a][from_fake] += 1.;
                } else {
                    a = CHANGE_TIME();
                    update_statistics[total_attempted][change_time] += 1.;
                    update_statistics[a][change_time] += 1.;
                }
#endif
            } else { // real diagram updates only
#ifdef DEBUGMODE
                if (q < update_prob_cuml[insert]) {
                    std::cout << "\nINSERT : ";
                    a = INSERT();
                    std::cout << "\n RESULT OF INSERT : "<< a;
                } else if (q < update_prob_cuml[remove]) {
                    std::cout << "\nREMOVE : ";
                    a = REMOVE();
                    std::cout << "\n RESULT OF REMOVE : " << a;
                } else if (q < update_prob_cuml[swap]) {
                    std::cout << "\nSWAP : ";
                    a = SWAP();
                    std::cout << "\n RESULT OF SWAP : " << a;
                } else if (q < update_prob_cuml[extend]) {
                    std::cout << "\nEXTEND : ";
                    a = EXTEND();
                    std::cout << "\n RESULT OF EXTEND : " << a;
                } else {
                    std::cout <<"\nTO_FAKE : ";
                    a = TO_FAKE();
                    std::cout << "\n RESULT OF TO_FAKE : " << a;
                }
#else
                if (q < update_prob_cuml[insert]) {
                    a = INSERT();
                    update_statistics[total_attempted][insert] += 1.;
                    update_statistics[a][insert] += 1.;
                } else if (q < update_prob_cuml[remove]) {
                    a = REMOVE();
                    update_statistics[total_attempted][remove] += 1.;
                    update_statistics[a][remove] += 1.;
                } else if (q < update_prob_cuml[swap]) {
                    a = SWAP();
                    update_statistics[total_attempted][swap] += 1.;
                    update_statistics[a][swap] += 1.;
                } else if (q < update_prob_cuml[extend]) {
                    a = EXTEND();
                    update_statistics[total_attempted][extend] += 1.;
                    update_statistics[a][extend] += 1.;
                } else {
                    a = TO_FAKE();
                    update_statistics[total_attempted][to_fake] += 1.;
                    update_statistics[a][to_fake] += 1.;
                }
#endif
            }
#ifdef DEBUGMODE
            print_diagram(diagram);
            try {
                test_conf(diagram);
            } catch (const char* e) {
                std::cerr << e << std::endl;
                print_diagram(diagram);
                exit(1);
            }
#endif
        }
        fill_hists();
    }
}

int diagmc::INSERT() {
#ifdef DEBUGMODE
    if (order == 0) std::cerr << "\nINSERT in fake diagram?\n";
#endif
    if (order >= max_order-1) return impossible;

    size_t nr_int = 2*order+1;
    size_t nr_arcs = order + 1;
    size_t n_int = static_cast<size_t>(random()*(nr_int));

#ifdef DEBUGMODE
    std::cout << "# INSERT order, n_int, nr_int : "
              << order << "\t" << n_int << "\t" << nr_int << "\n";
#endif
    Momentum_t p_ext;
    Diagram_type::iterator it;
    double tau_intv, t_init, t_fin;
    if (n_int == 2 * order) {
        it = diagram.end();
        p_ext = diagram_p;
        t_init = diagram.back().time();
        t_fin = diagram_time;
    } else {
        it = diagram.at(n_int);
        p_ext = it->in();
        if (it == diagram.begin())
            t_init = 0.;
        else
            t_init = std::prev(it)->time();
        t_fin = it->time();
    }
    tau_intv = t_fin - t_init;

    double t1 = random() * tau_intv + t_init;
    double t2 = t1 - log(random())/omega_p;
    if (t2 > t_fin)
        return impossible;
    double dt = t2 - t1;
    Momentum_t q;
    std::normal_distribution<double> gauss(0, sqrt(mass/dt));
    for (int i = 0; i < dim; i++)
        q(i) = gauss(rng);

    Momentum_t p_prime = p_ext - q;

    double ratio = (pow(sqrt(mass / (2*M_PI*dt)), dim) * vertex_amplitude_sq
                    * nr_int * tau_intv * update_prob[remove]/update_prob[insert]
                    * exp(dt * (p_ext*q)/mass) / (omega_p * nr_arcs * q.r_sq()));

#ifdef DEBUGMODE
    std::cout << "# INSERT t1, t2 : " << t1 << "\t" << t2
              << "\tdiagram_time " << diagram_time <<  "\n";
    std::cout << "# INSERT : " << pow(sqrt(mass / (2*M_PI*dt)), dim)
              << "\tnr_int : " << nr_int << "\ttau : " << tau_intv << "\n";
    std::cout << "# INSERT : propagator p_prime - p : " << exp(dt * (p_ext*q)/mass) << "\n";
    std::cout << "# INSERT : q " << q << "\t |q|^2 : " << q.r_sq()
              << "\tvertex ampl sq : " << vertex_amplitude_sq << "\n";
    std::cout <<  "# INSERT ratio : " << ratio << "\n\n";
#endif

    if (Metropolis(ratio)) {
        vertex_type v2(t2, p_prime, p_ext, q);
        vertex_type v1(t1, p_ext, p_prime, q);
        it = diagram.insert(it, v2);
        Diagram_type::iterator it1 = diagram.insert(it, v1);
        it1->link(it);
        it->link(it1);
        order++;

#ifdef DEBUGMODE
        diagram_weight *= (vertex_amplitude_sq / q.r_sq() * exp(-omega_p*dt)
                           * propagator(q, 0, dt) * exp(dt* (p_ext* q)/mass)
                           / pow(2*M_PI,dim));
#endif
        return accepted;
    }
    else {
        return rejected;
    }
}

int diagmc::REMOVE() {
    if (order == 0)
        std::cerr << "REMOVE in fake diagram?\n";
    if (order == 1)
        return impossible; // self-energy should contain at least one wiggly line
#ifdef DEBUGMODE
    std::cout << "\n# Welcome to REMOVE.\n";
    std::cout << "# REMOVE order: " << order << "\n";
#endif
    int nr_int = 2*order - 1;
    int n_v = static_cast<int>(random() * (2 * order));
    Diagram_type::iterator it1 = diagram.at(n_v);
    // follow the arc
    Diagram_type::iterator it2 = it1->link();
    if (it1->time() > it2->time())
        std::swap(it1, it2);
    Diagram_type::iterator it1_n = std::next(it1);
    // check if arc can be removed (adjacent vertices)
    if (it1_n != it2)
        return impossible;
    Diagram_type::iterator it_fin = std::next(it2);
    //Momentum_t p_prime = it1->out();
    Momentum_t p_ext = it1->in();
    Momentum_t q = it1->v();
    double dt = it2->time() - it1->time();
    double tau_intv, t_init, t_fin;
    if (it1 == diagram.begin()) {
        t_init = 0;
    }
    else {
        Diagram_type::iterator it_init = it1;
        --it_init;
        t_init = it_init->time();
    }
    t_fin =  (it_fin == diagram.end() ? diagram_time : it_fin->time());
    tau_intv = t_fin - t_init;
#ifdef DEBUGMODE
    std::cout << "# REMOVE iterators : " << it1->time() << "\t" << it2->time()
              << "\t" << t_init << "\t" << t_fin << "\n";
#endif
    double ratio = (pow(sqrt(mass / (2*M_PI*dt)), dim) * vertex_amplitude_sq
                    * nr_int * tau_intv * update_prob[remove]/update_prob[insert]
                    * exp(dt* (p_ext* q)/mass) / (omega_p * order * q.r_sq()));

#ifdef DEBUGMODE
    std::cout << "# REMOVE t1, t2 : " << it1->time() << "\t" << it2->time()
              << "\tdiagram_time " << diagram_time << "\n";
    std::cout << "# REMOVE : " << pow(sqrt(mass / (2*M_PI*dt)), dim)
              << "\tnr_int : " << nr_int << "\ttau : " << tau_intv << "\n";
    std::cout << "# REMOVE : propagator p_prime - p : " <<  exp(dt*(p_ext*q)/mass) << "\n";
    std::cout << "# REMOVE : q " << q << "\t |q|^2 : " << q.r_sq()
              << "\tvertex ampl sq : " << vertex_amplitude_sq << "\n";
    std::cout << "# REMOVE 1/ratio : " << ratio << "\n";
#endif
    if (Metropolis(1./ratio)) {
        diagram.erase(it2);
        diagram.erase(it1);
        order--;
#ifdef DEBUGMODE
        diagram_weight /= (vertex_amplitude_sq / q.r_sq() * exp(-omega_p*dt)
                           * propagator(q, 0, dt) * exp(dt* (p_ext* q)/mass)
                           / pow(2*M_PI,dim));
#endif
        return accepted;
    }
    else {
#ifdef DEBUGMODE
        std::cout << "REMOVE rejection\n";
#endif
        return rejected;
    }
}


int diagmc::SWAP() {
#ifdef DEBUGMODE
    if (order == 0) std::cerr << "\nSWAP in fake diagram?\n";
#endif
    if (order < 2) return impossible; // pointless

    //find arbitrary 2 neighbors;
#ifdef DIAG_LIST_STACK
    // for list-stack diagrams, the iterators returned by ::at(size_t)
    // are not in order. Thus, we need to draw a uniform vertex out of
    // all available ones and reroll the update if the last one was picked.
    int n_v;
    Diagram_type::iterator it1, it2;
    do {
        n_v = static_cast<int>(random()*(2*order));
        it1 = diagram.at(n_v);
        it2 = std::next(it1);
    } while (it2 == diagram.end());
#else
    int n_v = static_cast<int>(random()*(2*order-1));
    Diagram_type::iterator it1 = diagram.at(n_v);
    Diagram_type::iterator it2 = std::next(it1);
#endif
    if (it1->link() == it2)  return impossible;
    double dt = it2->time() - it1->time();
    Momentum_t p_old = it1->out();
    Diagram_type::iterator it_link1 = it1->link();
    Diagram_type::iterator it_link2 = it2->link();

#ifdef DEBUGMODE
    std::cout << "# welcome to SWAP : " << it1->time() << "\t" << it2->time()
              << "\t" << it_link1->time() << "\t" << it_link2->time() << "\n";
#endif
    Momentum_t p1 = (it_link2->time() > it2->time()
                     ? it1->in() - it2->v()
                     : it1->in() + it2->v());
    double delta_old = (std::fabs(it1->time() - it_link1->time())
                        + std::fabs(it2->time() - it_link2->time()));
    double delta_new = (std::fabs(it1->time() - it_link2->time())
                        + std::fabs(it2->time() - it_link1->time()));

    double ratio = (propagator(p1, mu, dt) * exp(-omega_p * delta_new)
                    / propagator(p_old, mu, dt) / exp(-omega_p * delta_old));

#ifdef DEBUGMODE
    std::cout << "\n#Ratio in SWAP : " << ratio << "\n";
#endif
    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "\n# Swap accepted.\n";
#endif
        it1->out(p1);
        it2->in(p1);
        it1->v(it_link2->v());
        it2->v(it_link1->v());
        it1->link(it_link2);
        it2->link(it_link1);
        it_link2->link(it1);
        it_link1->link(it2);
#ifdef DEBUGMODE
        diagram_weight *= ratio;
#endif
        return accepted;
    }
    else {
#ifdef DEBUGMODE
        std::cout << "\n# Swap Rejection.\n";
#endif
        return rejected;
    }
}


int diagmc::EXTEND() {
#ifdef DEBUGMODE
    if (order == 0 ) std::cerr << "\nEXTEND in fake diagram?\n";
#endif
    Diagram_type::iterator it = diagram.end();
    --it;
    double t0 = it->time();
    double En = dispersion(diagram_p, mu);
    double tnew = t0 -log(random())/En;
    if (tnew > max_time) return impossible;
#ifdef DEBUGMODE
    diagram_weight *= (propagator(diagram_p, mu, tnew - t0)
                       / propagator(diagram_p, mu, diagram_time - t0));
#endif
    diagram_time = tnew;
    return accepted;
}

int diagmc::TO_FAKE() {
#ifdef DEBUGMODE
    if (order == 0)
        std::cerr << "\nTO_FAKE in fake diagram?\n";
#endif
    if (order > 1)
        return impossible;
    Diagram_type::iterator it = diagram.begin();
    Momentum_t p_ext = diagram_p;
    Momentum_t q = it->v();
    Diagram_type::iterator itn = it; ++itn;
    double dt = itn->time() - it->time();

    double ratio = (pow(sqrt(mass / (2*M_PI*dt)), dim)
                    * vertex_amplitude_sq * diagram_time
                    * update_prob[to_fake]/update_prob[from_fake]
                    * exp(dt*(p_ext*q)/mass) / (omega_p * q.r_sq()));
#ifdef DEBUGMODE
    std::cout << "# TO_FAKE : " << pow(sqrt(2.*mass*omega_p)/M_PI, dim)
              << "\tomega_p : " << omega_p
              << "\tdiagram_time : " << diagram_time << "\n";
    std::cout << "# TO_FAKE : q " << q << "\t |q|^2 : " << q.r_sq()
              << "\tvertex ampl sq : " << vertex_amplitude_sq << "\n";
    std::cout << "# TO_FAKE : ratio " << 1./ratio << "\t" << ratio << "\n";
#endif
    if (Metropolis(1./ratio)) {
#ifdef DEBUGMODE
        std::cout << "# TO_FAKE accepted : " << it->time() << "\t" << itn->time() << "\n";
#endif
        diagram.erase(itn);
        diagram.erase(it);
        order = 0;
#ifdef DEBUGMODE
        diagram_weight /= (exp(-omega_p*dt) / q.r_sq() * vertex_amplitude_sq
                           / pow(2*M_PI, dim) * propagator(q,0,dt)
                           * exp(dt*(p_ext*q)/mass));
#endif
        return accepted;
    }
    else {
        return rejected;
    }
}

int diagmc::FROM_FAKE() {
#ifdef DEBUGMODE
    if (order != 0)
        std::cerr << "\nFROM_FAKE in real diagram?\n";
#endif
    double t1 = random() * diagram_time;
    double dt = -log(random())/omega_p;
    double t2 = t1 + dt;
    if (t2 > diagram_time)
        return impossible;
    Momentum_t q;
    std::normal_distribution<double> gauss(0, sqrt(mass/dt) );
    for (int i = 0; i < dim; i++)
        q(i) = gauss(rng);

    Momentum_t p_ext = diagram_p;
    Momentum_t p_prime = p_ext - q;

    double ratio = (pow(sqrt(mass / (2*M_PI*dt)), dim)
                    * vertex_amplitude_sq * diagram_time
                    * update_prob[to_fake]/update_prob[from_fake]
                    * exp(dt*(p_ext*q)/mass) / (omega_p * q.r_sq()));

#ifdef DEBUGMODE
    std::cout << "# FROM_FAKE : " << pow(sqrt(2.*mass*omega_p)/M_PI, dim)
              << "\tomega_p : " << omega_p << "\tdiagram_time : " << diagram_time << "\n";
    std::cout << "# FROM_FAKE : propagator p_prime - p : "
              << propagator(p_prime, mu, dt) << "\t"
              << propagator(diagram_p, mu, dt) << "\tdt : " << dt << "\n";
    std::cout << "# FROM_FAKE : q " << q << "\t |q|^2 : " << q.r_sq()
              << "\tvertex ampl sq : " << vertex_amplitude_sq << "\n";
    std::cout << "\n# FROM_FAKE ratio : " << ratio << "\n";
#endif

    if (Metropolis(ratio)) {
        vertex_type v1(t1, diagram_p, p_prime, q);
        vertex_type v2(t2, p_prime, diagram_p, q);
        Diagram_type::iterator it;
        Diagram_type::iterator itn;
        itn = diagram.insert(diagram.begin(), v2);
        it = diagram.insert(itn, v1);
        itn->link(it);
        it->link(itn);
        order = 1;
#ifdef DEBUGMODE
        diagram_weight *= (exp(-omega_p*dt) / q.r_sq() * vertex_amplitude_sq
                           / pow(2*M_PI, dim) * propagator(q,0,dt)
                           * exp(dt*(p_ext*q)/mass));
#endif
        return accepted;
    }
    else {
        return rejected;
    }
}


int diagmc::CHANGE_TIME() {
#ifdef DEBUGMODE
    if (order != 0)
        std::cerr << "\nCHANGE_TIME in real diagram?\n";
#endif
    double En = dispersion(diagram_p, mu);
    double tnew = -log(random())/En;
    if (tnew > max_time)
        return impossible;
#ifdef DEBUGMODE
    diagram_weight *= (propagator(diagram_p, mu, tnew)
                       / propagator(diagram_p, mu, diagram_time));
#endif
    diagram_time = tnew;
    return accepted;
}
