// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "momentum.hpp"

#include <iostream>
#include <list>
#include <algorithm>


const int dim = 3;

typedef double TimeType;
typedef Momentum_type<double, dim> Momentum_t;

class vertex_type; // forward declaration

#if defined DIAG_AVL
#include "avl_tree.hpp"

class AVL_diagram : public AVL::tree<vertex_type> {
public:
    void save(alps::hdf5::archive & ar) const;
    void load(alps::hdf5::archive & ar);
    using AVL::tree<vertex_type>::insert;
    inline iterator insert(const_iterator pos, const vertex_type& v) {
        return insert(v);
    }
    inline iterator insert(const_iterator pos, vertex_type&& v) {
        return insert(std::move(v));
    }
};

typedef AVL_diagram Diagram_type;

#elif defined DIAG_LIST
#include <list>

class list_diagram : public std::list<vertex_type> {
public:
    void save(alps::hdf5::archive & ar) const;
    void load(alps::hdf5::archive & ar);
    iterator at(size_t i);
    const_iterator at(size_t i) const;
};

typedef list_diagram Diagram_type;

#elif defined DIAG_LIST_STACK
#include "list_stack.hpp"

class list_stack_diagram : public list_stack<vertex_type> {
public:
    void save(alps::hdf5::archive & ar) const;
    void load(alps::hdf5::archive & ar);
};

typedef list_stack_diagram Diagram_type;

#endif

class vertex_type {
public:
    vertex_type() {};
    vertex_type(const TimeType t, const Momentum_t& in, const Momentum_t& out, const Momentum_t& v, const Diagram_type::iterator link) {
        mTime = t;
        mIn_momentum = in;
        mOut_momentum = out;
        mInteraction_momentum = v;
        mLink = link;
    }
    vertex_type(const TimeType t, const Momentum_t& in, const Momentum_t& out, const Momentum_t& v) {
        mTime = t;
        mIn_momentum = in;
        mOut_momentum = out;
        mInteraction_momentum = v;
    }
    void init(const TimeType t, const Momentum_t& in, const Momentum_t& out, const Momentum_t& v, const Diagram_type::iterator link) {
        mTime = t;
        mIn_momentum = in;
        mOut_momentum = out;
        mInteraction_momentum = v;
        mLink = link;
    }
    void init(const TimeType t, const Momentum_t& in, const Momentum_t& out, const Momentum_t& v) {
        mTime = t;
        mIn_momentum = in;
        mOut_momentum = out;
        mInteraction_momentum = v;
    }
    ~vertex_type() {};
    vertex_type(const vertex_type& rhs) {
        mTime = rhs.mTime;
        mIn_momentum = rhs.mIn_momentum;
        mOut_momentum = rhs.mOut_momentum;
        mInteraction_momentum = rhs.mInteraction_momentum;
        mLink = rhs.mLink;
    }
    vertex_type& operator=(const vertex_type& rhs) {
        mTime = rhs.mTime;
        mIn_momentum = rhs.mIn_momentum;
        mOut_momentum = rhs.mOut_momentum;
        mInteraction_momentum = rhs.mInteraction_momentum;
        mLink = rhs.mLink;
        return *this;
    }
    bool operator<(const vertex_type& rhs) const {
        return time() < rhs.time();
    }
    friend std::ostream &operator <<(std::ostream &os, vertex_type& rhs) {
        os << rhs.time() << "\t"
           << rhs.in() << "\t"
           << rhs.out() << "\t"
           << rhs.v() << "\n";
        return os;
    }
    friend std::istream& operator >>(std::istream& is, vertex_type& vt) {
        is >> vt.mTime >> vt.mIn_momentum >> vt.mOut_momentum >> vt.mInteraction_momentum; // no point in streaming the link
        return is;
    }
    TimeType time() const { return mTime; }
    Momentum_t& in()  { return mIn_momentum; }
    const Momentum_t& in() const  { return mIn_momentum; }
    Momentum_t& out()  { return mOut_momentum; }
    const Momentum_t& out() const  { return mOut_momentum; }
    Momentum_t& v()  { return mInteraction_momentum; }
    const Momentum_t& v() const  { return mInteraction_momentum; }
    Diagram_type::iterator link() const { return mLink; }
    void time(const TimeType t) { mTime = t; }
    void in(const Momentum_t& p) { mIn_momentum = p; }
    void out(const Momentum_t& p) { mOut_momentum = p; }
    void v(const Momentum_t& p) { mInteraction_momentum = p; }
    void link(const Diagram_type::iterator l) { mLink = l; }

    mutable int name;

private:
    TimeType mTime;
    Momentum_t mIn_momentum;
    Momentum_t mOut_momentum;
    Momentum_t mInteraction_momentum;
    Diagram_type::iterator mLink;
};
