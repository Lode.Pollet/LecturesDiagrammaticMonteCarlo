// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "froh.element.hpp"

#include <iterator>
#include <vector>

#include <alps/hdf5/vector.hpp>
#include <alps/hdf5/multi_array.hpp>

#include <boost/multi_array.hpp>


void Diagram_type::save(alps::hdf5::archive & ar) const {
    int n = 0;
    std::vector<double> conf_vector;

    for (const_iterator it = begin(); it != end(); ++it, ++n) {
        it->name = n;
        conf_vector.push_back(it->time());
        for (size_t idim =0; idim < dim; idim++)
            conf_vector.push_back(it->in()(idim));
        for (size_t idim =0; idim < dim; idim++)
            conf_vector.push_back(it->out()(idim));
        for (size_t idim =0; idim < dim; idim++)
            conf_vector.push_back(it->v()(idim));
    }
    ar["conf_vector"] << conf_vector;

    // save current diagram associations
    std::vector<int> linkname(n);
    size_t inc = 0;
    for (const_iterator it = begin(); it != end(); ++it, inc++) {
        linkname[inc] = (it->link())->name;
    }
    ar["linkname"] << linkname;
}

void Diagram_type::load(alps::hdf5::archive & ar) {
    std::vector<double> conf_vector;
    ar["conf_vector"] >> conf_vector;
    clear();
    size_t inc = 0;
    for (size_t i =0; i < conf_vector.size() / (3*dim + 1); i++) {
        double t1 = conf_vector[inc]; inc++;
        Momentum_t mom_in, mom_out, mom_v;
        for (size_t idim=0; idim < dim; idim++, inc++)
            mom_in(idim) = conf_vector[inc];
        for (size_t idim=0; idim < dim; idim++, inc++)
            mom_out(idim) = conf_vector[inc];
        for (size_t idim=0; idim < dim; idim++, inc++)
            mom_v(idim) = conf_vector[inc];
        vertex_type elem(t1, mom_in, mom_out, mom_v);
        insert(end(), elem);
    }

    std::vector<int> linkname;
    ar["linkname"] >> linkname;
    inc = 0;
    std::vector<iterator> itlist(linkname.size());
    iterator it = begin();
    for (int i = 0; i < linkname.size(); i++, ++it) {
        itlist[i] = it;
    }
    for (iterator it = begin();  it != end(); ++it, ++inc) {
        it->link(itlist[linkname[inc]]);
    }
}
