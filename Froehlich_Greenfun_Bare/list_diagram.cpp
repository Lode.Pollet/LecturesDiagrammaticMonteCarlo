// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "froh.element.hpp"

list_diagram::iterator list_diagram::at(size_t i) {
    if (i > size())
        return end();
    iterator it = begin();
    for (; i > 0; --i)
        ++it;
    return it;
}

list_diagram::const_iterator list_diagram::at(size_t i) const {
    if (i > size())
        return end();
    const_iterator it = begin();
    for (; i > 0; --i)
        ++it;
    return it;
}
