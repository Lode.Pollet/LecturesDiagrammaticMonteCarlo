// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "froh.element.hpp"

#include <fstream>
#include <vector>
#include <string>
#include <valarray>
#include <stdexcept>
#include <cmath>
#include <random>

#include <alps/hdf5/archive.hpp>
#include <alps/hdf5/vector.hpp>
#include <alps/hdf5/multi_array.hpp>
#include <alps/params.hpp>
#include <alps/accumulators.hpp>
#include <alps/mc/mcbase.hpp>
#include <alps/mc/api.hpp>
#include <alps/mc/stop_callback.hpp>

#include <boost/function.hpp>
#include <boost/multi_array.hpp>


typedef std::mt19937 base_generator_type;

enum statistics_tag {
    impossible,
    rejected,
    accepted,
    total_attempted,
    statistics_count
};
namespace {
    const std::string statistics_names[] = {
        "impossible",
        "rejected",
        "accepted",
        "total_attempted"
    };
    const std::string update_names[] = {
        "insert",
        "remove",
        "swap",
        "extend",
        "to_fake",
        "from_fake",
        "change_time"
    };
}

class diagmc : public alps::mcbase {
public:
    class invalid_configuration : public std::logic_error {
        using std::logic_error::logic_error;
    };

    typedef boost::multi_array<double, 2> Matrix;
    typedef boost::multi_array<double, 2>::index Matrix_index;

    diagmc(parameters_type const & params, size_t seed_offset = 0);

    void update();
    void measure();
    double fraction_completed() const;

    static std::string code_name();
    static void define_parameters(parameters_type&);

    using alps::mcbase::save;
    void save(alps::hdf5::archive & ar) const;
    using alps::mcbase::load;
    void load(alps::hdf5::archive & ar);

    double dispersion(const double p_sq, const double) const;
    double dispersion(const Momentum_t& p, const double) const;
    double propagator(const double p_sq, const double mu, const double dt) const;
    double propagator(const Momentum_t& , const double mu, const double dt) const;
    void print_params(std::ostream& os) const;
    void initialize();
    void init_diagram();
    int INSERT();
    int REMOVE();
    int SWAP();
    int EXTEND();
    int TO_FAKE();
    int FROM_FAKE();
    int CHANGE_TIME();
    void print_update_statistics(std::ostream& os) const;
    void reset_update_statistics();
    void fill_hists();
    void reset_hists();
    void output_final(const alps::results_type<diagmc>::type & results,
                      alps::hdf5::archive& ar);

    bool Metropolis(const double& x) {
        return x > 1 || random() < x;
    }
    int32_t index_time(const TimeType t) const {
        return ( static_cast<int32_t> ( t / dbin_radial_time));
    }


    size_t get_order() const {return order;}
    size_t get_max_order() const {return max_order;}
    void print_times_hist_green(std::ostream&, const int,
                                const alps::accumulators::result_wrapper&) const;
    void print_times(std::ostream& os,
                     const alps::accumulators::result_wrapper& normed,
                     const alps::accumulators::result_wrapper& logged) const;
    void test_conf(const Diagram_type& d) const;
    void test_conf() const {
        test_conf(diagram);
    }
    void print_diagram();
    void print_diagram(Diagram_type&);
    void initialize_coordinates();
    double get_green_one(const double) const;

    void update_measurements();

protected:
    base_generator_type rng;
    std::uniform_real_distribution<double> uni_dist;
    inline double random () { return uni_dist(rng); }

protected:
    unsigned long sweeps;
    unsigned long thermalization_sweeps;
    unsigned long total_sweeps;
    size_t N_hist, N_loop;

    enum update_tag {
        insert,
        remove,
        swap,
        extend,
        to_fake,
        from_fake,
        change_time,
        update_count
    };
    double update_prob_cuml[update_count];
    double update_prob[update_count];
private:
    void initialize_update_prob(update_tag begin, update_tag end);
    void initialize_update_prob();

protected:
    size_t number_of_times;

    // Hamiltonian parameters
    double max_time;
    double mu, mu_zero;
    double alpha, vertex_amplitude_sq;
    double omega_p;
    double p_init;
    double mass;
    size_t max_order;

private:
    std::vector<double> bin_coord_time, bin_size_time;
    double dbin_radial_time;

    size_t order;
    Diagram_type diagram;
    double diagram_time;
    Momentum_t diagram_p;
#ifdef DEBUGMODE
    double diagram_weight;
#endif

    //measurement variables
    Matrix update_statistics;
    double MCfake;
    double hist_order;
    std::vector<double> order_frequencies;
    std::vector<double> hist_green;
    std::vector<double> hist_green_zero;
    std::vector<double> hist_green_one;
};

inline double diagmc::dispersion(const double p_sq, const double mu) const {
    return (0.5 * p_sq/mass - mu);
}

inline double diagmc::dispersion(const Momentum_t& p, const double mu) const {
    return(p.r_sq()/2./mass - mu);
}
inline double diagmc::propagator(const Momentum_t& p, const double mu, const double dt) const {
    return(exp(-(dispersion(p, mu)*dt)));
}

inline double diagmc::propagator(const double p_sq, const double mu, const double dt) const {
    return (exp(-(dispersion(p_sq, mu))*dt ));
}
