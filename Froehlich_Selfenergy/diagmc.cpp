// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "diagmc.hpp"
#include "faddeeva.hpp"

#include <alps/params/convenience_params.hpp>

#include <boost/lambda/lambda.hpp>


std::string diagmc::code_name() {
    return "DiagMC simulation of the Froehlich polaron: "
        "Self-energy function";
}


void diagmc::define_specific_parameters(parameters_type& params) {
    params
        .description(diagmc::code_name())
        .define<double>("p_insert", 0.2, "probability for insert update")
        .define<double>("p_remove", 0.2, "probability for remove update")
        .define<double>("p_dress", 0.0,  "probability for dress update")
        .define<double>("p_undress", 0.0,  "probability for undress update")
        .define<double>("p_swap", 0.2, "probability for swap update")
        .define<double>("p_extend", 0.2, "probability for extend update")
        .define<double>("p_to_fake", 0.2, "probability for to_fake update")
        .define<double>("p_from_fake", 0.5, "probability for from_fake update")
        .define<double>("p_change_p", 0.0, "probability for change_p update")
        .define<double>("p_change_time", 0.5, "probability for change_time update")
        .define<size_t>("number_of_momenta", 0, "number of momentum grid points")
    ;
}


void diagmc::define_parameters(parameters_type& params) {
    alps::mcbase::define_parameters(params);
    alps::define_convenience_parameters(params)
        .define<unsigned long>("thermalization", "Number of thermalization sweeps")
        .define<unsigned long>("sweeps", "Total sweeps")
        .define<double>("mass", 1.0, "electron mass")
        .define<double>("omega_p", 1.0, "phonon frequency")
        .define<double>("alpha", 5.0, "coupling constant")
        .define<double>("mu_zero", "chemical potential")
        .define<size_t>("p_init", 0, "index of external momentum")
        .define<size_t>("max_order", 100, "maximum expansion order")
        .define<double>("max_momentum", 20.0,
                        "maximum momentum (if number_of_momenta != 0)")
        .define<double>("max_time", 20.0, "maximum time")
        .define<size_t>("number_of_times", 100, "number of time grid points")
        .define<size_t>("number_of_frequencies", 8192,
                        "number of Matsubara frequencies used in Dyson eq.")
        .define<unsigned int>("number_of_energies", 20,
                              "number of energies measured according to Eq. (16)")
        .define<double>("energy_guess", 5.,
                        "center point of energies used for Eq. (16), "
                        "i.e. guess for polaron energy")
        .define<double>("energy_spread", 0.2,
                        "percentage of energy guess to consider as width of "
                        "energy histograms")
        .define<size_t>("N_loop", 1,
                        "number of histogram samples gathered before measuring")
        .define<size_t>("N_hist", 1000,
                        "number of updates until counting configuration "
                        "towards histograms")
    ;
}


diagmc::diagmc(parameters_type const & params, size_t seed_offset)
    : alps::mcbase(params, seed_offset)
    , mu_zero(double(parameters["mu_zero"]))
    , alpha(double(parameters["alpha"]))
    , omega_p(double(parameters["omega_p"]))
    , p_init(double(parameters["p_init"]))
    , mass(double(parameters["mass"]))
    , number_of_momenta(size_t(parameters["number_of_momenta"]))
    , number_of_times(size_t(parameters["number_of_times"]))
    , number_of_frequencies(size_t(parameters["number_of_frequencies"]))
    , N_hist(size_t(parameters["N_hist"]))
    , N_loop(size_t(parameters["N_loop"]))
    , max_order(double(parameters["max_order"]))
    , max_time(double(parameters["max_time"]))
    , max_momentum(double(parameters["max_momentum"]))
    , update_statistics(statistics_count, update_count)
    , Time_self_grid(0, max_time, number_of_times+1)
    , Momentum_self_grid(0, max_momentum, number_of_momenta+1)
    , FFTgrid(0, max_time, number_of_frequencies+1)
    , rng(long(parameters["SEED"]) + seed_offset)
    , uni_dist(0,1)
    , sweeps(0)
    , thermalization_sweeps((unsigned long)(parameters["thermalization"]))
    , total_sweeps((unsigned long)(parameters["sweeps"]))
    , energy_guess(double(parameters["energy_guess"]))
    , energy_spread(double(parameters["energy_spread"]))
    , hist_green_zero(number_of_momenta+1, number_of_times)
    , hist_SGo_first(number_of_momenta+1, number_of_times)
    , hist_SGo_other(number_of_momenta+1, number_of_times)
    , hist_selfenergy_first(number_of_momenta+1, number_of_times)
    , hist_selfenergy_other(number_of_momenta+1, number_of_times)
    , Nenergies_MC((unsigned int)(parameters["number_of_energies"]))
{
    vertex_amplitude_sq = 4 * M_PI * alpha * pow(omega_p, 1.5) / sqrt(2 * mass);
    mu = mu_zero;
    max_momentum_sq = max_momentum * max_momentum;
    measurements
        << alps::accumulators::FullBinningAccumulator<double>("MCfake")
        << alps::accumulators::FullBinningAccumulator<std::vector<double> >("Greenfun_0")
        << alps::accumulators::FullBinningAccumulator<std::vector<double> >("Selfenergy_1")
        << alps::accumulators::FullBinningAccumulator<std::vector<double> >("Selfenergy_other")
        << alps::accumulators::FullBinningAccumulator<std::vector<double> >("SGo_1")
        << alps::accumulators::FullBinningAccumulator<std::vector<double> >("SGo_other")
        << alps::accumulators::MeanAccumulator<std::vector<double> >("order_frequencies")
        << alps::accumulators::FullBinningAccumulator<double>("order")
        ;
    if (Nenergies_MC > 0) {
        measurements
            << alps::accumulators::FullBinningAccumulator<std::vector<double> >("Selfenergy_energy")
            << alps::accumulators::FullBinningAccumulator<std::vector<double> >("Selfenergy_energy_other")
            ;
    }
    std::cout << std::setprecision(12);
    order_frequencies.resize(max_order);

    initialize_update_prob();
    if (number_of_momenta > 0 && update_prob[change_p] == 0)
        throw std::runtime_error("Non-zero momenta but no way to get there!");

    Energies_MC.resize(Nenergies_MC);
    Hist_Energies_MC.resize(Nenergies_MC);
    Hist_Energies_other_MC.resize(Nenergies_MC);
    double a = 8. * energy_spread * std::fabs(energy_guess) / std::pow(Nenergies_MC, 3);
    for (long j = 0; j < Nenergies_MC; j++) {
        long i = j - Nenergies_MC / 2;
        Energies_MC[j] = a * std::pow(i, 3) + energy_guess;
    }

    init_diagram();
    reset_hists();
}


void diagmc::initialize_update_prob(update_tag begin, update_tag end) {
    double norm = 0.;
    for (size_t upd = begin; upd < end; ++upd) {
        if (parameters.defined("p_" + update_names[upd])) {
            update_prob[upd] = double(parameters["p_" + update_names[upd]]);
            if (update_prob[upd] < 0) {
                throw std::runtime_error("Negative update probability: "
                                         + update_names[upd]);
            }
        } else {
            update_prob[upd] = 0.;
        }
        norm += update_prob[upd];
    }
    if (abs(norm - 1.) > 1e-10) {
        std::cerr << "Update probabilities don't add up to one.\n"
                  << "Renormalizing..." << std::endl;
        for (size_t upd = begin; upd < end; ++upd) {
            update_prob[upd] /= norm;
            parameters["p_" + update_names[upd]] = update_prob[upd];
        }
    }
    norm = 0.;
    for (size_t upd = begin; upd < end; ++upd) {
        norm += update_prob[upd];
        update_prob_cuml[upd] = norm;
    }
}


void diagmc::initialize_update_prob() {
    initialize_update_prob(insert, from_fake);
    initialize_update_prob(from_fake, update_count);
}


void diagmc::init_diagram() {
    diagram.clear();
    double tau_init = 1.0;
    diagram_p_index = p_init;
    diagram_p(0) = Momentum_self_grid(p_init);
    for (int i = 1; i < dim; i++) diagram_p(i) = 0.;
    diagram_time = tau_init;
    order = 0;
#ifdef DEBUGMODE
    diagram_weight = bare_propagator(diagram_p, mu, diagram_time);
    std::cout << "\t# Initial time     of the fake diagram : "
              << diagram_time << "\n";
    std::cout << "\t# Initial momentum of the fake diagram : "
              << diagram_p    << "\n";
    std::cout << "# Initial diagram weight               : "
              <<  diagram_weight << "\n";
#endif
}

void diagmc::reset_update_statistics() {
    for (int i= 0; i < statistics_count; i++) {
        for (int j = 0; j < update_count; j++) {
            update_statistics(i,j) = 0;
        }
    }
}

void diagmc::reset_hists() {
    hist_green_zero = 0.;
    hist_SGo_other = 0.;
    hist_SGo_first = 0.;
    hist_selfenergy_first = 0.;
    hist_selfenergy_other = 0.;
    for (size_t j=0; j < Nenergies_MC; j++) {
        Hist_Energies_MC[j] = 0.;
        Hist_Energies_other_MC[j] = 0.;
    }
    MCfake = 0.0;
    hist_order = 0.;
    std::fill(order_frequencies.begin(), order_frequencies.end(), 0.);
}

void diagmc::reset_sweeps() {
    sweeps = 0;
}

void diagmc::fill_hists() {
    if (diagram_time < max_time) {
        size_t itime = Time_self_grid.get_x_index(diagram_time);
        if (order == 0) {
            hist_green_zero(diagram_p_index, itime) += 1.;
        } else {
            if (order == 1) {
                hist_SGo_first(diagram_p_index, itime) += sqrt(diagram_time);
            } else {
                hist_SGo_other(diagram_p_index, itime) += 1.;
            }
        }
    }
    Diagram_type::iterator itend = diagram.end(); --itend;
    double r = dispersion(diagram_p, mu);
    if (itend->time() < max_time) {
        size_t itime = Time_self_grid.get_x_index(itend->time());
        if (order == 1) {
            hist_selfenergy_first(diagram_p_index, itime) += sqrt(itend->time()) * r;
        } else if (order > 1) {
            hist_selfenergy_other(diagram_p_index, itime) += 1. * r;
        }
    }
    hist_order += order;
    order_frequencies[order] += 1.;
    if (order == 0) {
        MCfake += 1.;
    } else {
        if (diagram_p_index == 0) {
            for (size_t k = 0; k < Nenergies_MC; k++) {
                Hist_Energies_MC[k] += exp((Energies_MC[k] - mu)*itend->time()) * r;
                if (order > 1) {
                    Hist_Energies_other_MC[k] += exp((Energies_MC[k] - mu)*itend->time()) * r;
                }
            }
        }
    }
}

void diagmc::measure() {
    sweeps++;
    if (sweeps < thermalization_sweeps)
        return;
    for (size_t k =0; k < number_of_momenta+1; ++k) {
        for (size_t j=0; j < number_of_times; ++j) {
            double tau = Time_self_grid[j];
            double vol = Time_self_grid.get_bin_volume(j,1);
            hist_green_zero(k,j) /= vol;
            hist_selfenergy_first(k,j) /= vol * sqrt(tau);
            hist_selfenergy_other(k,j) /= vol;
            hist_SGo_first(k,j) /= vol * sqrt(tau);
            hist_SGo_other(k,j) /= vol;
        }
    }
    for (size_t j =0; j < Nenergies_MC; j++) {
        Hist_Energies_MC[j] *= -1.;
        Hist_Energies_other_MC[j] *= -1.;
    }
    for (auto it = order_frequencies.begin(); it != order_frequencies.end(); ++it)
        *it /= N_loop;
    measurements["MCfake"] << MCfake;
    measurements["Greenfun_0"] << hist_green_zero.data();
    measurements["Selfenergy_1"] << hist_selfenergy_first.data();
    measurements["Selfenergy_other"] << hist_selfenergy_other.data();
    measurements["SGo_1"] << hist_SGo_first.data();
    measurements["SGo_other"] << hist_SGo_other.data();
    measurements["order"] << hist_order / N_loop;
    measurements["order_frequencies"] << order_frequencies;
    if (Nenergies_MC > 0) {
        measurements["Selfenergy_energy"] << Hist_Energies_MC;
        measurements["Selfenergy_energy_other"] << Hist_Energies_other_MC;
    }
}

double diagmc::fraction_completed() const {
  return ((sweeps > thermalization_sweeps)
          ? ((sweeps - thermalization_sweeps) / double(total_sweeps))
          : 0.);
}

double diagmc::get_self_one(const double p, const double tau) const {
  // computation of first order non-bold selfenergy contribtution Sigma(p, tau);
  // Sigma(p, tau) = V^2 e^{- (omega_ph - mu) tau} / p * (m / (2 pi tau) )^{3/2} \int_0^{\infty} sin (p r) exp(-0.5 m r^2/t) dr
  if (p == 0) {
    return (-exp((mu - omega_p)*tau) * vertex_amplitude_sq * sqrt(M_PI *2 * mass / tau) / (4* M_PI*M_PI));
  }
  double tau_ = sqrt(tau / (mass));
  return (-vertex_amplitude_sq * exp(-(omega_p - mu)*tau) / p * pow(2* M_PI, -1.5) * pow(tau_, -2.) * sqrt(2.) * Faddeeva::Dawson( tau_ * p / sqrt(2.)));
}


void diagmc::test_conf(const Diagram_type& d) const {
    if (is_not_close(diagram_p.r(), Momentum_self_grid(diagram_p_index) , 0.))
        throw invalid_configuration("external momentum value -- index broken");
    if (order > 0) {
        Diagram_type::const_iterator it = d.begin();
        Diagram_type::const_iterator itend = d.end(); --itend;
        if ((it->in() - diagram_p).r_sq() > 1e-8)
            throw invalid_configuration("external momentum different from "
                                        "incoming momentum at first vertex.");
        if ((it->in() - itend->out()).r_sq() > 1e-8 )
            throw invalid_configuration("In and out momenta not equal.");
        if (d.size()/2 != order)
            throw invalid_configuration("Problem with size/order");
        Diagram_type::const_iterator itn = it; ++itn;
        int n_occ = 0;
        for (; it!=itend; ++it, ++itn) {
            if (it->time() > diagram_time)
                throw invalid_configuration("Time too large on vertex.");
            if (!(it->v() == (it->link())->v() ))
                throw invalid_configuration("Different interaction lines on "
                                            "linked vertices.");
            if ( (it->out()-itn->in()).r_sq() > 1e-8)
                throw invalid_configuration("Change of momentum on a "
                                            "propagator line?");
            if (itn->time() <= it->time())
                throw invalid_configuration("Chronological order broken in diagram.");
            if ( (it->link())->time() > it->time()) {
                if (is_not_close((it->in()-it->out() - it->v()).r_sq(), 0., 1e-8))
                    throw invalid_configuration("Momentum conservation not ok "
                                                "at vertex with outgoing "
                                                "interaction line");
            } else {
                if (is_not_close((it->in() - it->out() + it->v() ).r_sq(), 0., 1e-8))
                    throw invalid_configuration("Momentum conservation not ok "
                                                "at vertex with incoming "
                                                "interaction line");
            }
            n_occ += (it->link()->time() > it->time() ? 1 : -1);
            if (n_occ <= 0)
                throw invalid_configuration("1p reducible diagram");
        }
    }
#ifdef DEBUGMODE
    double weight = compute_configuration_energy();
    if (is_not_close(weight, diagram_weight, 1e-8)) {
        std::cout << "# Weight : " << weight << "\t diagram_weight : "
                  << diagram_weight << "\t time " << diagram_time
                  << "\t momentum " << diagram_p << "\n";
        throw invalid_configuration("Weight of diagram not OK.\n");
    }
#endif
}

double diagmc::compute_configuration_energy() const {
    double weight = 1.;
    if (order == 0)
        return bare_propagator(diagram_p, mu, diagram_time);
    Diagram_type::const_iterator it = diagram.begin();
    Diagram_type::const_iterator itend = diagram.end(); --itend;
    Diagram_type::const_iterator itn = it; ++itn;
    for (; it!=itend; ++it, ++itn) {
        weight *= bare_propagator(it->out(), mu, itn->time() - it->time()) ;
        std::cout << "# Weight : " << it->time() << "\t" << weight << "\n";
        if (it->link()->time() > it->time())
            weight *= exp(-omega_p * (it->link()->time() - it->time()))
                / (pow(2*M_PI, dim)) * vertex_amplitude_sq  / it->v().r_sq();
        std::cout << "# Weight : " << it->time() << "\t" << weight << "\n";
    }
    weight *= bare_propagator(itend->out(), mu, diagram_time - itend->time()); // G0 part
    std::cout << "# Weight (end) : " << diagram_time << "\t"
              << diagram_time - itend->time() << "\t"
              << bare_propagator(itend->out(), mu, diagram_time - itend->time())
              << "\t" << weight << "\n";
    return weight;
}
