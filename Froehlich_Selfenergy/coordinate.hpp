// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>
#include <cmath>
#include <algorithm>
#include <iostream>


class Grid_1d_t {
public:
    Grid_1d_t(const double xmin, const double xmax)
        : xmin_(xmin), xmax_(xmax) {}

    Grid_1d_t(const double xmin, const double xmax, size_t Nx)
        : xmin_(xmin), xmax_(xmax)
    {
        populate_uniform(Nx);
    }

    void populate_uniform(size_t Nx) {
        X.resize(Nx);
        if (Nx == 1) {
            X[0] = xmin_;
        } else {
            double dx = (xmax_ - xmin_)/(Nx-1);
            for (size_t i =0; i < Nx; i++) {
                X[i] = xmin_ + dx * i;
            }
        }
        // should be sorted at all times
        // should be inclusive for boundaries: xmin_ = X[0] and xmax_ =  X[Nx-1].
    }

    double get_xmin() const { return xmin_;}
    double get_xmax() const { return xmax_;}
    std::size_t get_N() const { return X.size()-1;}


    void add_point(const double v) {
        if (v > xmax_) xmax_ = v;
        if (v < xmin_) xmin_ = v;
        std::vector<double>::iterator it = std::lower_bound(X.begin(), X.end(), v);
        std::vector<double>::iterator it2 = std::find(X.begin(), X.end(), v);
        if (!(it2 == X.end() ) ) {
            std::cerr << "Value " << v << " already exists in add_point!\n";
        }
        X.insert(it,v);
    }

    void remove_point(const double v) {
        std::vector<double>::iterator it = std::find(X.begin(), X.end(), v);
        // static_assert(!(it == X.end(), "not found" )) ;
        if (it == X.end()) {
            std::cerr << "Value " << v << " not found in remove_point!\n";
        }
        X.erase(it);
    }

    void print_grid(std::ostream& os) {
        os << "# Grid min, max : " << xmin_ << " " << xmax_
           << "\t N : " << X.size() << "\n";
        size_t inc = 0;
        for (std::vector<double>::iterator it = X.begin(); it != X.end(); ++it, inc++)
            os << inc << " " << *it << "\n";
    }

    double get_bin_volume(const size_t n, const size_t d) const {
        if (d == 1) {
            return X[n+1] - X[n];
        } else if (d == 2) {
            return M_PI * (X[n+1] * X[n+1] - X[n] * X[n]);
        } else if (d == 3) {
            return 4 * M_PI / 3. * (X[n+1]*X[n+1]*X[n+1] - X[n]*X[n]*X[n]);
        }
        return 0.;
    }

    std::vector<double>::iterator get_x_iterator(const double val) {
        return --(std::upper_bound(X.begin(), X.end(), val));
    }

    size_t get_x_index(const double val) const {
        return --(std::upper_bound(X.begin(), X.end(), val)) - X.begin();
    }

    double mid(const size_t n) const {
        return (X[n] + X[n+1])/2.;
    }
    double operator[](const size_t n) const {
        return (X[n] + X[n+1])/2.;
    }
    double operator()(const size_t n) const {
        return X[n];
    }

    template <class Container>
    double linear_interpolation(const double x, Container const& f) const {
        // in this case the container values are on the grid points
        //
        //    |__________|__________|__________|__________|__________|        (X.size() = 6)
        //   x(0)       x(1)       x(2)       x(3)       x(4)       x(5)
        //       x[0]        x[1]        x[2]       x[3]       x[4]
        //   f[0]       f[1]       f[2]       f[3]       f[4]       f[5]
        //
        size_t n = get_x_index(x);
        if (x > xmax_ || x < xmin_) {
            std::cerr << "index too large in linear_interpolation : " << x
                      << " " << n << "\n";
        }
        if (n >= X.size() - 1)
            return f[X.size()-1];
        return f[n] + (f[n+1] - f[n])/(X[n+1] - X[n]) * (x - X[n]);
    }

    template <class Container>
    double linear_interpolation_fewer(const double x, Container const& f) const {
        // in this case the container values are on the grid points
        //
        //    |__________|__________|__________|__________|__________|        (X.size() = 6)
        //   x(0)       x(1)       x(2)       x(3)       x(4)       x(5)
        //       x[0]        x[1]        x[2]       x[3]       x[4]
        //   f[0]       f[1]       f[2]       f[3]       f[4]                   f[5] does not exist
        //
        size_t n = get_x_index(x);
        if (x > xmax_ || x < xmin_) {
            std::cerr << "index too large in linear_interpolation : " << x
                      << " " << n << "\n";
        }
        if (n >= X.size() - 2)
            n = X.size() - 3;
        return f[n] + (f[n+1] - f[n])/(X[n+1] - X[n]) * (x - X[n]);
    }

    template <class Container>
    double linear_interpolation_mid(const double x, Container const& f) const {
        // in this case the container values are in the middle of the grid points
        //
        //    |__________|__________|__________|__________|__________|
        //   x(0)       x(1)       x(2)       x(3)       x(4)       x(5)      --> operator() return values
        //       x[0]        x[1]        x[2]       x[3]       x[4]           --> operator[] return values
        //       f[0]        f[1]        f[2]       f[3]       f[4]           --> container calls
        //
        size_t n = get_x_index(x);
        if (x > xmax_ || x < xmin_ ) {
            std::cerr << "index too large in linear_interpolation : " << x
                      << " " << n << "\n";
        }
        double xmid = (X[n] + X[n+1])/2;
        // to the left or right of the middle of the interval?
        if (xmid - x > 0) { // left
            //
            //    |__________|__________|___$________|__________|__________|
            //   x(0)       x(1)       x(2) $       x(3)       x(4)       x(5)     --> operator() return values
            //       x[0]        x[1]       $ x[2]       x[3]       x[4]           --> operator[] return values
            //       f[0]        f[1]       $ f[2]       f[3]       f[4]           --> container calls
            //
            if (n == 0) {
                double x0 = (X[1] + X[0])/2;
                double x1 = (X[2] + X[1])/2;
                return f[0] + (f[1] - f[0])/(x1-x0) * (x - x0);     // extrapolation
            } else {
                double x1 = (X[n] + X[n-1])/2;
                double x2 = (X[n+1] + X[n])/2;                          // all values should always exist
                return f[n-1] + (f[n] - f[n-1])/(x2 - x1) * (x - x1);
            }
        } else { // right
            //
            //    |__________|__________|_________$__|__________|__________|
            //   x(0)       x(1)       x(2)       $ x(3)       x(4)       x(5)     --> operator() return values
            //       x[0]        x[1]        x[2] $      x[3]       x[4]           --> operator[] return values
            //       f[0]        f[1]        f[2] $      f[3]       f[4]           --> container calls
            //
            if (n >= X.size()-2) {
                size_t m = X.size() - 2;
                double x1 = (X[m] + X[m-1])/2;
                double x2 = (X[m+1] + X[m])/2;                          // all values should always exist
                return f[m] + (f[m] - f[m-1])/(x2 - x1) * (x - x2);
            } else {
                double x1 = (X[n+1] + X[n])/2;
                double x2 = (X[n+2] + X[n+1])/2;                          // all values should always exist
                return f[n+1] + (f[n+1] - f[n])/(x2 - x1) * (x - x2);
            }
        }
    }

    template <class Matrix>
    friend double bilinear_interpolation(const double x, const double y, Grid_1d_t const& Xgrid, Grid_1d_t const& Ygrid, Matrix const& M) {
        if (x > Xgrid.get_xmax() || x < Xgrid.get_xmin() ) {
            std::cerr << "x out of range in bilinear_interpolation : " << x
                      << " " <<  Xgrid.get_xmin() << " " << Xgrid.get_xmax() << "\n";
        }
        if (y > Ygrid.get_xmax() || y < Ygrid.get_xmin() ) {
            std::cerr << "y out of range in bilinear_interpolation : " << y
                      << " " <<  Ygrid.get_xmin() << " " << Ygrid.get_xmax() << "\n";
        }
        size_t n = Xgrid.get_x_index(x);
        size_t m = Ygrid.get_x_index(y);

        size_t n1 = ( n > Xgrid.get_N()-2 ? Xgrid.get_N()-2 : n);
        size_t m1 = ( m > Ygrid.get_N()-2 ? Ygrid.get_N()-2 : m);

        double dx = (x - Xgrid(n1))/(Xgrid(n1+1) - Xgrid(n1));
        double dy = (y - Ygrid(m1))/(Ygrid(m1+1) - Ygrid(m1));
        double a00 = M(n1,   m1);
        double a10 = M(n1+1, m1)   - M(n1,m1);
        double a01 = M(n1,   m1+1) - M(n1,m1);
        double a11 = M(n1+1, m1+1) + M(n1,m1) - M(n1+1, m1) - M(n1, m1+1);
        return a00 + a10*dx + a01*dy + a11*dx*dy;
    }

    template <class Matrix>
    friend double bilinear_interpolation_mid(double x, double y, Grid_1d_t const& Xgrid, Grid_1d_t const& Ygrid, Matrix const& M) {
        if (x > Xgrid.get_xmax() || x < Xgrid.get_xmin() ) {
            std::cerr << "x out of range in bilinear_interpolation_mid : " << x
                      << " " <<  Xgrid.get_xmin() << " " << Xgrid.get_xmax() << "\n";
        }
        if (y > Ygrid.get_xmax() || y < Ygrid.get_xmin() ) {
            std::cerr << "y out of range in bilinear_interpolation_mid : " << y
                      << " " <<  Ygrid.get_xmin() << " " << Ygrid.get_xmax() << "\n";
        }
        size_t n = Xgrid.get_x_index(x);
        size_t m = Ygrid.get_x_index(y);

        size_t n1, m1;
        if (n >= Xgrid.get_N()-1) {
            n1 = Xgrid.get_N()-2;
        } else {
            n1 = ( (x < Xgrid[n] && n) ? n-1 : n);
        }
        if (m >= Ygrid.get_N()-1) {
            m1 = Ygrid.get_N()-2;
        } else {
            m1 = ( (y < Ygrid[m] && m) ? m-1 : m);
        }

        double dx = (x - Xgrid[n1])/(Xgrid[n1+1] - Xgrid[n1]);
        double dy = (y - Ygrid[m1])/(Ygrid[m1+1] - Ygrid[m1]);
        double a00 = M(n1,   m1);
        double a10 = M(n1+1, m1)   - M(n1,m1);
        double a01 = M(n1,   m1+1) - M(n1,m1);
        double a11 = M(n1+1, m1+1) + M(n1,m1) - M(n1+1, m1) - M(n1, m1+1);
        return a00 + a10*dx + a01*dy + a11*dx*dy;
    }


    template <class Container>
    double quadratic_interpolation(const double x, Container const& f) const {   // in this case the container values are on the grid points
        //
        //    |__________|__________|__________|__________|__________|        (X.size() = 6)
        //   x(0)       x(1)       x(2)       x(3)       x(4)       x(5)
        //       x[0]        x[1]        x[2]       x[3]       x[4]
        //   f[0]       f[1]       f[2]       f[3]       f[4]       f[5]
        //
        size_t n = get_x_index(x);
        if (x > xmax_ || x < xmin_) {
            std::cerr << "index too large in quadratic_interpolation : " << x
                      << " " << n << "\n";
        }
        if (n >= X.size() - 1)
            return f[X.size()-1];
        size_t n0, n1, n2;
        if (n == 0) {
            n0 = 0; n1 = 1; n2 = 2;
        } else if (n == X.size()-2) {
            n0 = X.size()-3; n1 = X.size() - 2; n2 = X.size() -1;
        } else {
            if (x < mid(n)) { // left
                n0 = n-1; n1 = n; n2 = n+1;
            } else { // right
                n0 = n; n1 = n+1; n2 = n+2;
            }
        }
        double L0 = (x - X[n1]) * (x - X[n2]) / (X[n0] - X[n1]) / (X[n0] - X[n2]);
        double L1 = (x - X[n0]) * (x - X[n2]) / (X[n1] - X[n0]) / (X[n1] - X[n2]);
        double L2 = (x - X[n0]) * (x - X[n1]) / (X[n2] - X[n0]) / (X[n2] - X[n1]);
        return f[n0] * L0 + f[n1] * L1 + f[n2] * L2;
    }

    template <class Container>
    double quadratic_interpolation_mid(const double x, Container const& f) const { // in this case the container values are in the middle of the grid points
        //
        //    |__________|__________|__________|__________|__________|
        //   x(0)       x(1)       x(2)       x(3)       x(4)       x(5)      --> operator() return values
        //       x[0]        x[1]        x[2]       x[3]       x[4]           --> operator[] return values
        //       f[0]        f[1]        f[2]       f[3]       f[4]           --> container calls
        //
        size_t n = get_x_index(x);
        if (x > xmax_ || x < xmin_ ) {
            std::cerr << "index too large in quadratic_interpolation_mid : "
                      << x << " " << n << "\n";
        }
        double xmid = mid(n);
        size_t n0, n1, n2;
        if (x <= mid(0)) {
            n0 = 0; n1 = 1; n2 = 2;
        } else if (x >= mid(X.size() -3)) {
            n0 = X.size() -4; n1 = X.size() -3; n2 = X.size() -2;
        } else {
            if (x < xmid) n--;
            n0 = n; n1 = n+1; n2 = n+2;
        }

        double L0 = (x - mid(n1)) * (x - mid(n2)) / (mid(n0) - mid(n1)) / (mid(n0) - mid(n2));
        double L1 = (x - mid(n0)) * (x - mid(n2)) / (mid(n1) - mid(n0)) / (mid(n1) - mid(n2));
        double L2 = (x - mid(n0)) * (x - mid(n1)) / (mid(n2) - mid(n0)) / (mid(n2) - mid(n1));
        return f[n0] * L0 + f[n1] * L1 + f[n2] * L2;

    }

    template <class Matrix>
    friend double biquadratic_interpolation(const double x, const double y, Grid_1d_t const& Xgrid, Grid_1d_t const& Ygrid, Matrix const& M) {
        if (x > Xgrid.get_xmax() || x < Xgrid.get_xmin() ) {
            std::cerr << "x out of range in biquadratic_interpolation : " << x
                      << " " <<  Xgrid.get_xmin() << " " << Xgrid.get_xmax() << "\n";
        }
        if (y > Ygrid.get_xmax() || y < Ygrid.get_xmin() ) {
            std::cerr << "y out of range in biquadratic_interpolation : " << y
                      << " " <<  Ygrid.get_xmin() << " " << Ygrid.get_xmax() << "\n";
        }
        size_t n = Xgrid.get_x_index(x);
        size_t m = Ygrid.get_x_index(y);

        size_t n0, n1, n2;
        size_t m0, m1, m2;
        if (n == 0) {
            n0 = 0; n1 = 1; n2 = 2;
        } else if (n == Xgrid.get_N() - 1) {
            n0 = Xgrid.get_N()-2; n1 = Xgrid.get_N() - 1; n2 = Xgrid.get_N();
        } else {
            if (x < Xgrid[n]) { // left
                n0 = n-1; n1 = n; n2 = n+1;
            } else { // right
                n0 = n; n1 = n+1; n2 = n+2;
            }
        }

        if (m == 0) {
            m0 = 0; m1 = 1; m2 = 2;
        } else if (m == Ygrid.get_N()-1) {
            m0 = Ygrid.get_N()-2; m1 = Ygrid.get_N() - 1; m2 = Ygrid.get_N();
        } else {
            if (y < Ygrid[m]) { // left
                m0 = m-1; m1 = m; m2 = m+1;
            } else { // right
                m0 = m; m1 = m+1; m2 = m+2;
            }
        }

        // quadratic interpolation at constant m0
        double L0x = (x - Xgrid(n1)) * (x - Xgrid(n2)) / (Xgrid(n0) - Xgrid(n1)) / (Xgrid(n0) - Xgrid(n2));
        double L1x = (x - Xgrid(n0)) * (x - Xgrid(n2)) / (Xgrid(n1) - Xgrid(n0)) / (Xgrid(n1) - Xgrid(n2));
        double L2x = (x - Xgrid(n0)) * (x - Xgrid(n1)) / (Xgrid(n2) - Xgrid(n0)) / (Xgrid(n2) - Xgrid(n1));

        double a0 = M(n0, m0) * L0x + M(n1, m0) * L1x + M(n2, m0)*L2x;
        double a1 = M(n0, m1) * L0x + M(n1, m1) * L1x + M(n2, m1)*L2x;
        double a2 = M(n0, m2) * L0x + M(n1, m2) * L1x + M(n2, m2)*L2x;

        double L0y = (y - Ygrid(m1)) * (y - Ygrid(m2)) / (Ygrid(m0) - Ygrid(m1)) / (Ygrid(m0) - Ygrid(m2));
        double L1y = (y - Ygrid(m0)) * (y - Ygrid(m2)) / (Ygrid(m1) - Ygrid(m0)) / (Ygrid(m1) - Ygrid(m2));
        double L2y = (y - Ygrid(m0)) * (y - Ygrid(m1)) / (Ygrid(m2) - Ygrid(m0)) / (Ygrid(m2) - Ygrid(m1));
        return a0 * L0y + a1 * L1y + a2 * L2y;

    }



    template <class Matrix>
    friend double biquadratic_interpolation_semi1(const double x, const double y, Grid_1d_t const& Xgrid, Grid_1d_t const& Ygrid, Matrix const& M) {
        // middle of the bin for x, on the grid points for y
        if (x > Xgrid.get_xmax() || x < Xgrid.get_xmin() ) {
            std::cerr << "x out of range in biquadratic_interpolation_semi1 : "
                      << x << " " <<  Xgrid.get_xmin() << " " << Xgrid.get_xmax() << "\n";
        }
        if (y > Ygrid.get_xmax() || y < Ygrid.get_xmin() ) {
            std::cerr << "y out of range in biquadratic_interpolation_semi1 : "
                      << y << " " <<  Ygrid.get_xmin() << " " << Ygrid.get_xmax() << "\n";
        }
        size_t n = Xgrid.get_x_index(x);
        size_t m = Ygrid.get_x_index(y);

        size_t n0, n1, n2;
        if (x <= Xgrid[0]) {
            n0 = 0; n1 = 1; n2 = 2;
        } else if (x >= Xgrid[Xgrid.get_N() -2]) {
            n0 = Xgrid.get_N() -3; n1 = Xgrid.get_N() -2; n2 = Xgrid.get_N() -1;
        } else {
            if (x < Xgrid[n])
                n--;
            n0 = n; n1 = n+1; n2 = n+2;
        }

        size_t m0, m1, m2;
        if (m == 0) {
            m0 = 0; m1 = 1; m2 = 2;
        } else if (m == Ygrid.get_N()-1) {
            m0 = Ygrid.get_N()-2; m1 = Ygrid.get_N() - 1; m2 = Ygrid.get_N();
        } else {
            if (y < Ygrid[m]) { // left
                m0 = m-1; m1 = m; m2 = m+1;
            } else { // right
                m0 = m; m1 = m+1; m2 = m+2;
            }
        }

        // quadratic interpolation at constant m0
        double L0x = (x - Xgrid[n1]) * (x - Xgrid[n2]) / (Xgrid[n0] - Xgrid[n1]) / (Xgrid[n0] - Xgrid[n2]);
        double L1x = (x - Xgrid[n0]) * (x - Xgrid[n2]) / (Xgrid[n1] - Xgrid[n0]) / (Xgrid[n1] - Xgrid[n2]);
        double L2x = (x - Xgrid[n0]) * (x - Xgrid[n1]) / (Xgrid[n2] - Xgrid[n0]) / (Xgrid[n2] - Xgrid[n1]);

        double a0 = M(n0, m0) * L0x + M(n1, m0) * L1x + M(n2, m0)*L2x;
        double a1 = M(n0, m1) * L0x + M(n1, m1) * L1x + M(n2, m1)*L2x;
        double a2 = M(n0, m2) * L0x + M(n1, m2) * L1x + M(n2, m2)*L2x;
        //std::cout << "# biquad_semi a0, a1, a2 : " << a0 << " " << a1 << " " << a2 << "\n";

        double L0y = (y - Ygrid(m1)) * (y - Ygrid(m2)) / (Ygrid(m0) - Ygrid(m1)) / (Ygrid(m0) - Ygrid(m2));
        double L1y = (y - Ygrid(m0)) * (y - Ygrid(m2)) / (Ygrid(m1) - Ygrid(m0)) / (Ygrid(m1) - Ygrid(m2));
        double L2y = (y - Ygrid(m0)) * (y - Ygrid(m1)) / (Ygrid(m2) - Ygrid(m0)) / (Ygrid(m2) - Ygrid(m1));
        return a0 * L0y + a1 * L1y + a2 * L2y;
    }

    template <class Matrix>
    friend double biquadratic_interpolation_semi2(const double x, const double y, Grid_1d_t const& Xgrid, Grid_1d_t const& Ygrid, Matrix const& M) {
        //  on the grid points for x, middle of the bin for y
        if (x > Xgrid.get_xmax() || x < Xgrid.get_xmin() ) {
            std::cerr << "x out of range in biquadratic_interpolation_semi2 : "
                      << x << " " <<  Xgrid.get_xmin() << " " << Xgrid.get_xmax() << "\n";
        }
        if (y > Ygrid.get_xmax() || y < Ygrid.get_xmin() ) {
            std::cerr << "y out of range in biquadratic_interpolation_semi2 : "
                      << y << " " <<  Ygrid.get_xmin() << " " << Ygrid.get_xmax() << "\n";
        }
        size_t n = Xgrid.get_x_index(x);
        size_t m = Ygrid.get_x_index(y);

        size_t n0, n1, n2;
        size_t m0, m1, m2;
        if (n == 0) {
            n0 = 0; n1 = 1; n2 = 2;
        } else if (n == Xgrid.get_N() - 1) {
            n0 = Xgrid.get_N()-2; n1 = Xgrid.get_N() - 1; n2 = Xgrid.get_N();
        } else {
            if (x < Xgrid[n]) { // left
                n0 = n-1; n1 = n; n2 = n+1;
            } else { // right
                n0 = n; n1 = n+1; n2 = n+2;
            }
        }
        if (y <= Ygrid[0]) {
            m0 = 0; m1 = 1; m2 = 2;
        } else if (y >= Ygrid[Ygrid.get_N() -2]) {
            m0 = Ygrid.get_N() -3; m1 = Ygrid.get_N() -2; m2 = Ygrid.get_N() -1;
        } else {
            if (y < Ygrid[m]) m--;
            m0 = m; m1 = m+1; m2 = m+2;
        }

        // quadratic interpolation at constant m0
        double L0x = (x - Xgrid(n1)) * (x - Xgrid(n2)) / (Xgrid(n0) - Xgrid(n1)) / (Xgrid(n0) - Xgrid(n2));
        double L1x = (x - Xgrid(n0)) * (x - Xgrid(n2)) / (Xgrid(n1) - Xgrid(n0)) / (Xgrid(n1) - Xgrid(n2));
        double L2x = (x - Xgrid(n0)) * (x - Xgrid(n1)) / (Xgrid(n2) - Xgrid(n0)) / (Xgrid(n2) - Xgrid(n1));

        double a0 = M(n0, m0) * L0x + M(n1, m0) * L1x + M(n2, m0)*L2x;
        double a1 = M(n0, m1) * L0x + M(n1, m1) * L1x + M(n2, m1)*L2x;
        double a2 = M(n0, m2) * L0x + M(n1, m2) * L1x + M(n2, m2)*L2x;

        double L0y = (y - Ygrid[m1]) * (y - Ygrid[m2]) / (Ygrid[m0] - Ygrid[m1]) / (Ygrid[m0] - Ygrid[m2]);
        double L1y = (y - Ygrid[m0]) * (y - Ygrid[m2]) / (Ygrid[m1] - Ygrid[m0]) / (Ygrid[m1] - Ygrid[m2]);
        double L2y = (y - Ygrid[m0]) * (y - Ygrid[m1]) / (Ygrid[m2] - Ygrid[m0]) / (Ygrid[m2] - Ygrid[m1]);
        return a0 * L0y + a1 * L1y + a2 * L2y;
    }


    template <class Matrix>
    friend double biquadratic_interpolation_mid(const double x, const double y, Grid_1d_t const& Xgrid, Grid_1d_t const& Ygrid, Matrix const& M) {
        if (x > Xgrid.get_xmax() || x < Xgrid.get_xmin() ) {
            std::cerr << "x out of range in biquadratic_interpolation_mid : "
                      << x << " " <<  Xgrid.get_xmin() << " " << Xgrid.get_xmax() << "\n";
        }
        if (y > Ygrid.get_xmax() || y < Ygrid.get_xmin() ) {
            std::cerr << "y out of range in biquadratic_interpolation_mid : "
                      << y << " " <<  Ygrid.get_xmin() << " " << Ygrid.get_xmax() << "\n";
        }
        size_t n = Xgrid.get_x_index(x);
        size_t m = Ygrid.get_x_index(y);

        size_t n0, n1, n2;
        if (x <= Xgrid[0]) {
            n0 = 0; n1 = 1; n2 = 2;
        } else if (x >= Xgrid[Xgrid.get_N() -2]) {
            n0 = Xgrid.get_N() -3; n1 = Xgrid.get_N() -2; n2 = Xgrid.get_N() -1;
        } else {
            if (x < Xgrid[n]) n--;
            n0 = n; n1 = n+1; n2 = n+2;
        }

        size_t m0, m1, m2;
        if (y <= Ygrid[0]) {
            m0 = 0; m1 = 1; m2 = 2;
        } else if (y >= Ygrid[Ygrid.get_N() -2]) {
            m0 = Ygrid.get_N() -3; m1 = Ygrid.get_N() -2; m2 = Ygrid.get_N() -1;
        } else {
            if (y < Ygrid[m]) m--;
            m0 = m; m1 = m+1; m2 = m+2;
        }

        // quadratic interpolation at constant m0
        double L0x = (x - Xgrid[n1]) * (x - Xgrid[n2]) / (Xgrid[n0] - Xgrid[n1]) / (Xgrid[n0] - Xgrid[n2]);
        double L1x = (x - Xgrid[n0]) * (x - Xgrid[n2]) / (Xgrid[n1] - Xgrid[n0]) / (Xgrid[n1] - Xgrid[n2]);
        double L2x = (x - Xgrid[n0]) * (x - Xgrid[n1]) / (Xgrid[n2] - Xgrid[n0]) / (Xgrid[n2] - Xgrid[n1]);

        double a0 = M(n0, m0) * L0x + M(n1, m0) * L1x + M(n2, m0)*L2x;
        double a1 = M(n0, m1) * L0x + M(n1, m1) * L1x + M(n2, m1)*L2x;
        double a2 = M(n0, m2) * L0x + M(n1, m2) * L1x + M(n2, m2)*L2x;

        double L0y = (y - Ygrid[m1]) * (y - Ygrid[m2]) / (Ygrid[m0] - Ygrid[m1]) / (Ygrid[m0] - Ygrid[m2]);
        double L1y = (y - Ygrid[m0]) * (y - Ygrid[m2]) / (Ygrid[m1] - Ygrid[m0]) / (Ygrid[m1] - Ygrid[m2]);
        double L2y = (y - Ygrid[m0]) * (y - Ygrid[m1]) / (Ygrid[m2] - Ygrid[m0]) / (Ygrid[m2] - Ygrid[m1]);
        return a0 * L0y + a1 * L1y + a2 * L2y;

    }

    template <class Container>
    double cubic_interpolation(const double x, Container const& f) const {   // in this case the container values are on the grid points
        //
        //    |__________|__________|__________|__________|__________|        (X.size() = 6)
        //   x(0)       x(1)       x(2)       x(3)       x(4)       x(5)
        //       x[0]        x[1]        x[2]       x[3]       x[4]
        //   f[0]       f[1]       f[2]       f[3]       f[4]       f[5]
        //
        size_t n = get_x_index(x);
        if (x > xmax_ || x < xmin_) {
            std::cerr << "index too large in cubic_interpolation : " << x
                      << " " << n << "\n";
        }
        if (n >= X.size() - 1) return f[X.size()-1];
        // reduced coordinate
        double t = (x - X[n])/(X[n+1] - X[n]);
        // tangents
        double m0 = (n == 0
                     ? (f[1]-f[0])/(X[1] - X[0])
                     : 0.5 * ((f[n+1]-f[n])/(X[n+1]-X[n]) + (f[n]-f[n-1])/(X[n]-X[n-1])));
        double m1 = (n >= X.size()-2
                     ? (f[X.size()-1]-f[X.size()-2])/(X[X.size()-1]-X[X.size()-2])
                     : 0.5 * ((f[n+2]-f[n+1])/(X[n+2]-X[n+1]) + (f[n+1]-f[n])/(X[n+1]-X[n])));
        // Hermite basis functions
        double h00 = (1+ 2 *t) * (1-t)*(1-t);
        double h10 = t * (1-t)*(1-t);
        double h01 = t * t* (3 - 2*t);
        double h11 = t * t* (t-1);

        return h00 * f[n] + h10 * (X[n+1] - X[n]) * m0 + h01 * f[n+1] + h11 * (X[n+1] - X[n]) * m1;
    }

    template <class Container>
    double cubic_interpolation_mid(const double x, Container const& f) const { // in this case the container values are in the middle of the grid points
        //
        //    |__________|__________|__________|__________|__________|
        //   x(0)       x(1)       x(2)       x(3)       x(4)       x(5)      --> operator() return values
        //       x[0]        x[1]        x[2]       x[3]       x[4]           --> operator[] return values
        //       f[0]        f[1]        f[2]       f[3]       f[4]           --> container calls
        //
        size_t n0 = get_x_index(x);
        if (x > xmax_ || x < xmin_ ) {
            std::cerr << "index too large in cubic_interpolation_mid : " << x
                      << " " << n0 << "\n";
        }
        double xmid = mid(n0);

        size_t n;
        if (n0 >= X.size()-2) {
            n = X.size()-3;
        } else {
            n = ( (x < xmid && n0) ? n0-1 : n0);
        }

        // reduced coordinate
        double t = (x - mid(n))/(mid(n+1) - mid(n));

        double m0, m1;
        if (n <= 1) {
            m0 = (f[1]-f[0])/(mid(1) - mid(0));
            //m0 = 0;
            m1 = 0.5 * ( (f[n+2] - f[n+1]) / (mid(n+2)-mid(n+1))  + (f[n+1] - f[n])/(mid(n+1)-mid(n)) );
            if (x < mid(0)) {
                return f[0] + (f[1] - f[0])/(mid(1)-mid(0)) * (x - mid(0));     // extrapolation
            }
        } else if (n >= X.size()-3) {
            m0 = 0.5 * ( (f[n+1] - f[n])   / (mid(n+1)-mid(n))    + (f[n] - f[n-1]) / (mid(n)-mid(n-1)) );
            m1 = (f[X.size()-2]-f[X.size()-3])/(mid(X.size()-2) - mid(X.size()-3));
            if (x > mid(X.size()-2)) {
                return f[X.size()-2] + (f[X.size()-2] - f[X.size()-3])/(mid(X.size()-2) - mid(X.size()-3)) * (x - mid(X.size()-2));
            }
        } else {
            m0 = 0.5 * ( (f[n+1] - f[n])   / (mid(n+1)-mid(n))    + (f[n] - f[n-1]) / (mid(n)-mid(n-1)) );
            m1 = 0.5 * ( (f[n+2] - f[n+1]) / (mid(n+2)-mid(n+1))  + (f[n+1] - f[n]) / (mid(n+1)-mid(n)) );
        }

        // Hermite basis functions
        double h00 = (1+ 2 *t) * (1-t)*(1-t);
        double h10 = t * (1-t)*(1-t);
        double h01 = t * t* (3 - 2*t);
        double h11 = t * t* (t-1);

        return (h00 * f[n] + h10 * (mid(n+1) - mid(n)) * m0 + h01 * f[n+1]
                + h11 * (mid(n+1) - mid(n)) * m1);
    }



private:
    double xmin_;
    double xmax_;
    std::vector<double> X;
};
