// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "diagmc.hpp"

#include <alps/mc/mpiadapter.hpp>


class bold_diagmc : public alps::mcmpiadapter<diagmc> {
public:
    using Base = alps::mcmpiadapter<diagmc>;

    static std::string code_name();
    static void define_specific_parameters(parameters_type&);

    template <typename... Args>
    bold_diagmc (const parameters_type& parameters, Args... rest)
        : Base(parameters, rest...)
        , iteration_nr(0)
        , bold_max_nr_iteration(size_t(parameters["bold_max_nr_iteration"]))
        , convergence_threshold(double(parameters["convergence_threshold"]))
        , Greenfun_t(number_of_momenta+1, number_of_times)
        , Selfenergy_dg1(number_of_momenta+1, number_of_times)
    {
        reinitialize_time_grid(double(parameters["min_time"]),
                               double(parameters["max_time"]));
        init_Greenfun();
    }

    bool run(boost::function<bool ()> const & stop_callback);

    void reinitialize_time_grid(double tmin, double tmax);

    virtual void update() override;

    virtual double compute_configuration_energy() const override;
    virtual void test_conf(const Diagram_type& d) const override;

    // These updates shadow (hide) their counterparts from diagmc,
    // they do not need to be virtual and don't incur runtime overhead.
    // Only update is called polymorphically (and relatively seldom)
    statistics_tag DRESS();
    statistics_tag UNDRESS();
    statistics_tag SWAP();
    statistics_tag EXTEND();
    statistics_tag TO_FAKE();
    statistics_tag FROM_FAKE();

    double full_propagator(const double p_sq, const double mu, const double dt) const;
    double full_propagator(const Momentum_t&, const double mu, const double dt) const;

    virtual void fourier_analysis(std::ostream&,
                                  const size_t,
                                  const alps::accumulators::result_wrapper&,
                                  const alps::accumulators::result_wrapper&) override;
    bool boldify(std::ostream&, const alps::results_type<diagmc>::type & results);
    void broadcast_Greenfun();

    using alps::mcbase::save;
    using alps::mcbase::load;
    virtual void save(alps::hdf5::archive & ar) const override;
    virtual void load(alps::hdf5::archive & ar) override;

private:
    void wrap_up();
    void init_Greenfun();
    void compute_self_dg1();
    void NCA();
    size_t get_Nelem_green() const {
        return number_of_times * (number_of_momenta + 1);
    }

private:
    size_t iteration_nr;
    size_t bold_max_nr_iteration;

    double convergence_threshold;

    Matrix<double> Greenfun_t;
    Matrix<double> Selfenergy_dg1;
    
};
