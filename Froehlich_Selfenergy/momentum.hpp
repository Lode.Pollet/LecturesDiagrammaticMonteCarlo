// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <cmath>
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

#include <alps/hdf5/archive.hpp>
#include <alps/hdf5/vector.hpp>

#include <boost/lexical_cast.hpp>

template <typename T, int DIM=3>
class Momentum_type {
public:
    Momentum_type() {};
    Momentum_type(T x, T y, T z) {
        mx[0] = x;
        mx[1] = y;
        mx[2] = z;
    }
    Momentum_type(const T x[DIM]) {
        for (int i = 0; i < DIM; i++)
            mx[i] = x[i];
    }

    void init_r(const T& r) {
        mx[0] = r;
        for (int i = 1; i < DIM; i++)
            mx[i] = 0;
    }

    void spherical_to_cartesian(T r, T theta, T phi) {
        mx[0] = r * sin(theta) * cos(phi);
        mx[1] = r * sin(theta) * sin(phi);
        mx[2] = r * cos(theta);
    }

    const T& operator()(const int i) const {
        return mx[i];
    }
    T& operator()(const int i) {
        return mx[i];
    }

    friend std::ostream& operator<<(std::ostream& os,
                                    const Momentum_type<T, DIM>& p) {
        for (int i = 0; i < DIM; i++)
            os << p.mx[i] << "\t";
        return os;
    }

    friend std::istream& operator>>(std::istream& is, Momentum_type<T, DIM>& p) {
        for (int i = 0; i < DIM; i ++)
            is >> p.mx[i];
        return is;
    }


    // save to HDF5
    void save(alps::hdf5::archive& ar) const {
        std::vector<T> data(mx, mx+DIM);
        ar[""] << data;
    }

    // load from HDF5
    void load(alps::hdf5::archive& ar) {
        std::vector<T> data;
        ar[""] >> data;
        std::copy(data.begin(), data.end(), mx);
    }

    friend Momentum_type operator+(const Momentum_type<T, DIM>& m1,
                                   const Momentum_type<T, DIM>& m2) {
        Momentum_type<T, DIM> v;
        for (int i = 0; i < DIM; i++)
            v.mx[i] = m1(i) + m2(i);
        return v;
    }

    friend Momentum_type operator-(const Momentum_type<T, DIM>& m1,
                                   const Momentum_type<T, DIM>& m2) {
        Momentum_type<T, DIM> v;
        for (int i = 0; i < DIM; i++)
            v.mx[i] = m1(i) - m2(i);
        return v;
    }

    friend T operator*(const Momentum_type<T, DIM>& m1,
                       const Momentum_type<T, DIM>& m2) {
        T d = 0;
        for (int i = 0; i < DIM; i++)
            d += m1(i)* m2(i);
        return d;
    }

    friend bool operator==(const Momentum_type<T, DIM>& m1, const Momentum_type<T, DIM>& m2) {
        for (int i =0; i < DIM; i++)
            if (m1(i) != m2(i))
                return false;
        return true;
    }

    template <typename E>
    friend Momentum_type operator*(const E alpha, const Momentum_type<T, DIM>& m) {
        Momentum_type<T, DIM> v;
        for (int i=0; i < DIM; i++)
            v.mx[i] = alpha * m(i);
        return v;
    }


    // spherical coordinate transformations, only for real types.
    // Needs speciliazation for complex types
    T r_sq() const {
        T norm = 0;
        for (int i = 0; i < DIM; i++)
            norm += mx[i]*mx[i];
        return norm;
    }
    T r() const {
        T norm = 0;
        for (int i = 0; i < DIM; i++)
            norm += mx[i]*mx[i];
        return sqrt(norm);
    }

private:
    T mx[DIM];
};
