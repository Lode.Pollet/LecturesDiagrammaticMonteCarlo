// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "bold_diagmc.hpp"


void bold_diagmc::update() {
    reset_hists();
    for (size_t j = 0; j < N_loop; ++j) {
        for (size_t i = 0; i < N_hist; ++i) {
            double q = random();
            if (order == 0) { // fake diagram updates only
                try_log_update<from_fake, bold_diagmc, &bold_diagmc::FROM_FAKE>(q)
                    || try_log_update<change_p, diagmc, &diagmc::CHANGE_P>(q)
                    || try_log_update<change_time, diagmc, &diagmc::CHANGE_TIME>(q);
            } else { // real diagram updates only
                try_log_update<dress, bold_diagmc, &bold_diagmc::DRESS>(q)
                    || try_log_update<undress, bold_diagmc, &bold_diagmc::UNDRESS>(q)
                    || try_log_update<swap, bold_diagmc, &bold_diagmc::SWAP>(q)
                    || try_log_update<extend, diagmc, &diagmc::EXTEND>(q)
                    || try_log_update<to_fake, bold_diagmc, &bold_diagmc::TO_FAKE>(q);
            }
#ifdef DEBUGMODE
            print_diagram(diagram);
            try {
                test_conf(diagram);
            }
            catch (const char* e) {
                std::cerr << e << std::endl;
                print_diagram(diagram);
                exit(1);
            }
#endif
        }
        fill_hists();
    }
}


statistics_tag bold_diagmc::DRESS() { // insert new arc dressing one existing vertex
#ifdef DEBUGMODE
    if (order == 0)
        std::cerr << "\nDRESS in fake diagram?\n";
#endif
    if (order >= max_order-1)
        return impossible;

    size_t n_v = static_cast<size_t>(random()*(2*order - 1)); // twice as many vertices as arcs, excluding first one
    Diagram_type::iterator itm = diagram.begin(); ++itm; // vertex in the middle
    for (size_t j=0 ; j < n_v; j++) ++itm;

    Diagram_type::iterator it_left  = itm; --it_left;
    Diagram_type::iterator it_right = itm; ++it_right; // it is possible that it_right is diagram.end()  !!!
    double dt1 = itm->time() - it_left->time();
    double t1 = it_left->time() + random() * dt1; // time of vertex on the left of the new arc

    double dt2 = (it_right == diagram.end()
                  ? diagram_time - itm->time()
                  : it_right->time() - itm->time());
    double t2 = itm->time() + random() * dt2; // time of vertex on the right of new arc

    double dt = t2 - t1; // duration of new arc
#ifdef DEBUGMODE
    std::cout << "# DRESS diagram_time : " << diagram_time << "\n";
    std::cout << "# DRESS order , n_v " << order << " " << n_v << " itm, it_left, it_right, t1, t2 : " << itm->time() << " " << it_left->time() << " " << (it_right == diagram.end() ? diagram_time : it_right->time()) << " "  << t1 << " " << t2 << "\n";
#endif
    // generate new momentum
    Momentum_t q;
    std::normal_distribution<double> gauss(0, sqrt(mass/dt));
    for (int i = 0; i < dim; i++)
        q(i) = gauss(rng);


    Momentum_t p_a = itm->in() - q;
    Momentum_t p_b = itm->out() - q;

    if (p_a.r_sq() > max_momentum_sq) return impossible;             // hard cutoff
    if (p_b.r_sq() > max_momentum_sq) return impossible;             // hard cutoff
    double wx = full_propagator(itm->in(), mu, dt1)
        * (it_right == diagram.end()
           ? bare_propagator(itm->out() , mu, dt2)
           : full_propagator(itm->out(), mu, dt2));
    double wy = exp(-omega_p * dt) * vertex_amplitude_sq / q.r_sq()
        * pow (2 *M_PI, -dim*1.);
    if (itm->time() - t1 > max_time)
        return impossible;
    if (t2 - itm->time() > max_time)
        return impossible;
    if (t1 - it_left->time() > max_time)
        return impossible;
    if (!(it_right == diagram.end())) {
        if (it_right->time() - t2 > max_time)
            return impossible;
    }
    wy *= full_propagator(p_a, mu, itm->time() - t1)
        * full_propagator(p_b, mu, t2 - itm->time())
        * full_propagator(itm->in(), mu, t1 - it_left->time());
    wy *= (it_right == diagram.end()
           ? bare_propagator(itm->out(), mu, diagram_time - t2)
           : full_propagator(itm->out(), mu, it_right->time() - t2));
    double pxy = update_prob[dress] / pow(sqrt( 2 * M_PI * mass/dt), dim)
        * exp(- q.r_sq() * dt / (2*mass)) / dt1 / dt2 / (2*order - 1.);
    double pyx = update_prob[undress] / (2. * order );
    double ratio = wy*pyx/wx/pxy;

#ifdef DEBUGMODE
    std::cout << "# DRESS wx = " << wx << "\t"
              << bare_propagator(itm->in(), mu, dt2) << "\t"
              << bare_propagator(itm->out(), mu, t2 - itm->time()) << "\n";
    std::cout << "# DRESS wy = " << wy << "\t" << exp(-omega_p * dt) << "\t"
              << full_propagator(p_a, mu, itm->time() - t1) << "\t"
              << full_propagator(p_b, mu, t2 - itm->time()) << "\t"
              << vertex_amplitude_sq / q.r_sq() * pow (2 *M_PI, -dim*1.) << "\n";
    std::cout << "# DRESS pxy = " << pxy << "\n";
    std::cout << "# DRESS pyx = " << pyx << "\t" << order << "\t"
              << dt1 << "\t" << dt2 <<  "\n";
    std::cout << "# DRESS ratio = " << ratio << "\n";
#endif


    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "# DRESS accepted, ratio = " << ratio << "\n";
        std::cout << "# DRESS old diagram weight " << diagram_weight
                  << "\t wy,wx, wy/wx : " << wy << "\t" << wx << "\t"
                  << wy/wx << "\n";
#endif
        vertex_type v2(t2, p_b, itm->out(), q);
        vertex_type v1(t1, itm->in(), p_a, q);
        Diagram_type::iterator it2 = diagram.insert(it_right, v2);
        Diagram_type::iterator it1 = diagram.insert(itm, v1);
        itm->out(p_b);
        itm->in(p_a);
        it1->link(it2);
        it2->link(it1);
#ifdef DEBUGMODE
        diagram_weight *= wy / wx;
#endif
        order++;
        return accepted;
    } else {
        return rejected;
    }
}

statistics_tag bold_diagmc::UNDRESS() {

    if (order == 0) std::cerr << "UNDRESS in fake diagram?\n";
    if (order == 1) return impossible; // self-energy should contain at least one wiggly line

    size_t n_v = static_cast<size_t>(random()*(2*order - 2)); // twice as many orders (# vertices) as arcs (order), excluding first and last one
    Diagram_type::iterator itm = diagram.begin(); ++itm; // vertex in the "middle"
    for (size_t j=0 ; j < n_v; j++)
        ++itm;
    Diagram_type::iterator ite = diagram.end();
    --ite;
    if (itm == ite) {
        std::cerr << "itm == ite in UNDRESS???\n";
    }
    Diagram_type::iterator it1 = itm; --it1; // left vertex
    Diagram_type::iterator it2 = itm; ++it2; // right vertex

    if (it2 != it1->link())
        return impossible; // not removable
    if (it1 == diagram.begin())
        return impossible; // not removable

    Diagram_type::iterator it_left = it1; --it_left;
    Diagram_type::iterator it_right = it2; ++it_right;
    double dt1 = itm->time() - it_left->time();
    double dt2 = (it_right == diagram.end()
                  ? diagram_time - itm->time()
                  : it_right->time() - itm->time());

    Momentum_t p_left = it1->in();
    Momentum_t p_right = it2->out();
    Momentum_t p_a = it1->out();
    Momentum_t p_b = it2->in();
    Momentum_t q = it1->v();

    double dt = it2->time() - it1->time();
    double wx = exp(-omega_p * dt) * vertex_amplitude_sq / q.r_sq()
        * pow (2*M_PI, -dim*1.); // weight of arc and 2 propagators
    wx *= full_propagator(p_a, mu, itm->time() - it1->time())
        * full_propagator(p_b, mu, it2->time() - itm->time())
        * full_propagator(it_left->out(), mu, it1->time() - it_left->time());
    wx *= (it2 == ite
           ? bare_propagator(it2->out(), mu, diagram_time - it2->time())
           : full_propagator(it_right->in(), mu, it_right->time() - it2->time()));
    if (itm->time() - it_left->time() > max_time)
        return impossible;
    if (!(it2 == ite)) {
        if (it_right->time() - itm->time() > max_time)
            return impossible;
    }
    double wy = full_propagator(p_left, mu, itm->time() - it_left->time())
        * (it2 == ite
           ? bare_propagator(p_right, mu, diagram_time - itm->time())
           : full_propagator(p_right, mu, it_right->time() - itm->time()));
    double pxy = update_prob[undress]/ (2*order - 2.);
    double pyx = update_prob[dress] / pow(sqrt( 2 * M_PI * mass/dt), dim)
        * exp(- q.r_sq() * dt / (2*mass)) / dt1 / dt2 / (2*order - 3.);
    double ratio = wy * pyx/wx/pxy;

#ifdef DEBUGMODE
    std::cout << "# UNDRESS itm, it1, it2 : " << itm->time() << "\t"
              << it1->time() << "\t" << it2->time() << "\n";
    std::cout << "# UNDRESS wx = " << wx << "\n";
    std::cout << "# UNDRESS wy = " << wy << "\n";
    std::cout << "# UNDRESS pxy = " << pxy << "\n";
    std::cout << "# UNDRESS pyx = " << pyx << "\t" << order << "\t"
              << dt1 << "\t" << dt2 << "\n";
    std::cout << "# UNDRESS ratio = " << ratio << "\t" << 1./ratio << "\n";
#endif

    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "# UNDRESS accepted, ratio = " << ratio << "\n";
#endif
        itm->in(p_left);
        itm->out(p_right);
        diagram.erase(it2);
        diagram.erase(it1);
        order--;
#ifdef DEBUGMODE
        diagram_weight *= wy/wx;
#endif
        return accepted;
    } else {
#ifdef DEBUGMODE
        std::cout << "UNDRESS rejection\n";
#endif
        return rejected;
    }
}


statistics_tag bold_diagmc::SWAP() {
#ifdef DEBUGMODE
    if (order == 0) std::cerr << "\nSWAP in fake diagram?\n";
#endif
    if (order < 2)
        return impossible; // pointless

    // find arbitrary 2 neighbors;
    int n_v = static_cast<int>(random()*(2*order-1));
    Diagram_type::iterator it1 = diagram.begin();
    for (int i = 0; i < n_v; i++) ++it1;
    Diagram_type::iterator it2 = it1;
    ++it2;
    if (it1->link() == it2)  return impossible;
    double dt = it2->time() - it1->time();
    Momentum_t p_old = it1->out();
    Diagram_type::iterator it_link1 = it1->link();
    Diagram_type::iterator it_link2 = it2->link();

#ifdef DEBUGMODE
    std::cout << "# welcome to SWAP : " << it1->time() << "\t" << it2->time()
              << "\t" << it_link1->time() << "\t" << it_link2->time() << "\n";
    //print_diagram(diagram);
#endif
    Momentum_t p1 = (it_link2->time() > it2->time()
                     ? it1->in() - it2->v()
                     : it1->in() + it2->v());

    if ((p1 - diagram_p).r_sq() < 1e-10) {
        // reducible diagram in SWAP
        return impossible;
    }

    double delta_old = (std::fabs(it1->time() - it_link1->time())
                        + std::fabs(it2->time() - it_link2->time()));
    double delta_new = (std::fabs(it1->time() - it_link2->time())
                        + std::fabs(it2->time() - it_link1->time()));

    if (p1.r_sq() > max_momentum_sq) return impossible;          // hard cutoff
    for (Diagram_type::iterator it_irr_test = diagram.begin();
         it_irr_test != diagram.end();
         ++ it_irr_test)
    {
        if (!is_not_close((p1 - it_irr_test->out()).r_sq(), 0., 1e-8))
            return impossible; // bold reducible
    }
    double ratio = full_propagator(p1, mu, dt) * exp(-omega_p * delta_new )
        / full_propagator(p_old, mu, dt) / exp(-omega_p * delta_old);

#ifdef DEBUGMODE
    std::cout << "\n#Ratio in SWAP : " << ratio << "\n";
#endif
    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "\n# Swap accepted.\n";
#endif
        it1->out(p1);
        it2->in(p1);
        it1->v(it_link2->v());
        it2->v(it_link1->v());
        it1->link(it_link2);
        it2->link(it_link1);
        it_link2->link(it1);
        it_link1->link(it2);
#ifdef DEBUGMODE
        diagram_weight *= ratio;
#endif
        return accepted;
    } else {
#ifdef DEBUGMODE
        std::cout << "\n# Swap Rejection.\n";
#endif
        return rejected;
    }

}


statistics_tag bold_diagmc::TO_FAKE() {
#ifdef DEBUGMODE
    if (order == 0) std::cerr << "\nTO_FAKE in fake diagram?\n";
#endif
    if (order > 1) return impossible;
    Diagram_type::iterator it = diagram.begin();
    Momentum_t p_prime = it->out();
    Momentum_t q = it->v();
    Diagram_type::iterator itn = it; ++itn;
    double dt = itn->time() - it->time();

    double wy = bare_propagator(diagram_p, mu, diagram_time);
    double wx = vertex_amplitude_sq / q.r_sq() * exp(-omega_p * dt)
        * pow(2*M_PI, -dim*1.) * full_propagator(p_prime, mu, dt)
        * bare_propagator(diagram_p, mu, diagram_time - itn->time());
    double pyx = update_prob[from_fake] * exp( - q.r_sq() * dt / (2*mass))
        * pow(dt / (2*mass*M_PI), dim*0.5) * omega_p * exp(-omega_p * dt);
    double pxy = update_prob[to_fake];
    double ratio = wy * pyx / wx / pxy;

#ifdef DEBUGMODE
    std::cout << "# TO_FAKE : " << pow(sqrt(2.*mass*omega_p)/M_PI, dim)
              << "\tomega_p : " << omega_p << "\tdiagram_time : "
              << diagram_time << "\n";
    std::cout << "# TO_FAKE : propagator p_prime - p : "
              << full_propagator(p_prime, mu, dt) << "\t"
              << bare_propagator(diagram_p, mu, dt) << "\n";
    std::cout << "# TO_FAKE : q " << q << "\t |q|^2 : " << q.r_sq()
              << "\tvertex ampl sq : " << vertex_amplitude_sq << "\n";
    std::cout << "# TO_FAKE : ratio " << 1./ratio << "\t" << ratio << "\n";
#endif
    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "# TO_FAKE accepted : " << it->time() << "\t"
                  << itn->time() << "\n";
#endif
        diagram.erase(itn);
        diagram.erase(it);
        order = 0;
#ifdef DEBUGMODE
        diagram_weight *= wy/wx;
#endif
        return accepted;
    } else {
        return rejected;
    }
}

statistics_tag bold_diagmc::FROM_FAKE() {
#ifdef DEBUGMODE
    if (order != 0)
        std::cerr << "\nFROM_FAKE in real diagram?\n";
#endif
    double dt = -log(random())/omega_p;
    double t2 = dt;
    if (t2 > diagram_time)
        return impossible;
    double t1 = 0.; // selfenergy
    Momentum_t q;
    std::normal_distribution<double> gauss(0, sqrt(mass/dt) );
    for (int i = 0; i < dim; i++)
        q(i) = gauss(rng);

    Momentum_t p_ext = diagram_p;
    Momentum_t p_prime = p_ext - q;

    double wx = bare_propagator(diagram_p, mu, diagram_time);
    if (p_prime.r_sq() > max_momentum_sq) return impossible;      // hard cutoff
    if (dt > max_time)
        return impossible;
    if (diagram_time - t2 > max_time)
        return impossible;
    double wy = vertex_amplitude_sq / q.r_sq() * exp(-omega_p * dt)
        * pow(2*M_PI, -dim*1.) * full_propagator(p_prime, mu, dt)
        * bare_propagator(diagram_p, mu, diagram_time - t2);
    double pxy = update_prob[from_fake] * exp( - q.r_sq() * dt / (2*mass))
        * pow(dt / (2*mass*M_PI), dim*0.5) * omega_p * exp(-omega_p * dt);
    double pyx = update_prob[to_fake];
    double ratio = wy * pyx / wx / pxy;
#ifdef DEBUGMODE
    std::cout << "# FROM_FAKE : " << pow(sqrt(2.*mass*omega_p)/M_PI, dim)
              << "\tomega_p : " << omega_p << "\tdiagram_time : "
              << diagram_time << "\n";
    std::cout << "# FROM_FAKE : propagator p_prime - p : "
              << full_propagator(p_prime, mu, dt) << "\t"
              << full_propagator(diagram_p, mu, dt) << "\tdt : " << dt << "\n";
    std::cout << "# FROM_FAKE : q " << q << "\t |q|^2 : " << q.r_sq()
              << "\tvertex ampl sq : " << vertex_amplitude_sq << "\n";

    std::cout << "\n# FROM_FAKE ratio : " << ratio << "\n";
#endif

    if (Metropolis(ratio)) {
        vertex_type v1(t1, diagram_p, p_prime, q);
        vertex_type v2(t2, p_prime, diagram_p, q);
        Diagram_type::iterator it;
        Diagram_type::iterator itn;
        itn = diagram.insert(diagram.end(), v2);
        it = diagram.insert(itn, v1);
        itn->link(it);
        it->link(itn);
        order = 1;
#ifdef DEBUGMODE
        diagram_weight *= wy/wx;
#endif
        return accepted;
    } else {
        return rejected;
    }
}
