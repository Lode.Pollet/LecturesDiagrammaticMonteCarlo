// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "diagmc.hpp"

#include <fftw3.h>


void diagmc::fourier_analysis(std::ostream& os, const size_t mom_indx,
                              const alps::accumulators::result_wrapper& sgo_1,
                              const alps::accumulators::result_wrapper& sgo_other)
{
    std::vector<double> self_t_one(number_of_times);
    std::vector<double> self_t_other(number_of_times);
    std::vector<double> self_t_dense(number_of_frequencies);
    std::vector<double> G0_t(number_of_frequencies);
    std::vector<double> G_t_dense(number_of_frequencies);
    std::vector<double> G_t(number_of_times);
    std::vector<std::complex<double> > self_w(number_of_frequencies);
    std::vector<std::complex<double> > G0_w(number_of_frequencies);
    std::vector<std::complex<double> > G_w(number_of_frequencies);
    std::vector<double> omega_n(number_of_frequencies);
    std::vector<double> conv_t(number_of_frequencies);
    std::vector<std::complex<double> > conv_w(number_of_frequencies);

    // statistics of self energy
    double p_sq = Momentum_self_grid(mom_indx) *Momentum_self_grid(mom_indx);

    for (size_t i=0; i < number_of_times; i++) {
        double tau = Time_self_grid[i];
        self_t_one[i] = sgo_1.mean<std::vector<double> >()[mom_indx * number_of_times + i] * sqrt(tau);       // measure() has already divided by tau, but we need this division on self_t_one_dense
        self_t_other[i] = sgo_other.mean<std::vector<double> >()[mom_indx * number_of_times + i];
    }

    // initialization of bare green functions
    for (size_t j =0; j < number_of_frequencies; j++) {
        omega_n[j] = (j < number_of_frequencies/2
                      ? (2 * j ) * M_PI / max_time
                      : -(2. * (number_of_frequencies -j)) * M_PI / max_time); // bosonic
        G0_w[j] = 1./(std::complex<double>(- dispersion(p_sq, mu),omega_n[j]) );
    }

    for (size_t i =0; i < number_of_frequencies; i++) {
        double tau = FFTgrid[i];
        G0_t[i] = -bare_propagator(p_sq, mu, tau);
        // make selfenergy dense by interpolation
        // this is Selfenergy convoluted with G0
        self_t_dense[i] = (Time_self_grid.quadratic_interpolation_mid(tau, self_t_one)
                           / sqrt(tau)
                           + Time_self_grid.quadratic_interpolation_mid(tau, self_t_other));
    }

    // forward fourier of the selfenergy
    do_FFT_forward_selfenergy(self_t_dense, self_w, 0., G0_t, G0_w); // this is Selfenergy convolved with G0

    // Dyson
    for (size_t i =0; i < number_of_frequencies; i++) {
        G_w[i] = G0_w[i] * (self_w[i]) / (1. - self_w[i]);
    }

    // backward fourier of the full green function
    do_FFT_backward_green(G_w, G_t_dense, G0_t);

    // coarse grain
    for (size_t i=0; i < number_of_times; i++) {
        G_t[i] = FFTgrid.quadratic_interpolation_mid(Time_self_grid[i], G_t_dense);
    }

    // output
    for (size_t i =0; i < number_of_times; i++) {
        double tau = Time_self_grid[i] ;
        double sb = (exp(mu * tau) * vertex_amplitude_sq * sqrt(M_PI *2 * mass)
                     / (4*M_PI*M_PI) * sqrt(M_PI / omega_p) * erf(sqrt(omega_p * tau)));
        if (mom_indx == 0)
            os << tau << "\t" << bare_propagator(p_sq, mu, tau)
               << "\t" << self_t_one[i]/sqrt(tau)  << "\t" << sb
               << "\t" << -G_t[i] << "\t" << log(std::fabs(G_t[i]) + 1e-16) << "\n";
    }
    os << "\n\n";
}

void diagmc::do_FFT_forward_selfenergy(const std::vector<double>& Self_t,
                                       std::vector<std::complex<double> >& Self_w,
                                       const double jump,
                                       const std::vector<double>& Gzero_t,
                                       const std::vector<std::complex<double> >& Gzero_w) const
{

    size_t N = number_of_frequencies;
    double dt = max_time / number_of_frequencies;

    fftw_complex *in, *out;
    fftw_plan p;
    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    p = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE); // +1 in exponent


    for (size_t j = 0; j < N; j++) {
        in[j][0] = (Self_t[j] - jump * Gzero_t[j]) * dt;
        in[j][1] = 0.;
    }


    fftw_execute(p);

    for (size_t j = 0; j < N/2; j++) {
        Self_w[j] = std::complex<double>(out[j][0], out[j][1])
            * std::complex<double>( cos(j * M_PI / N), sin(j*M_PI/N))
            + jump * Gzero_w[j];
    }
    for (size_t j = N/2; j < N; j++) {
        Self_w[j] = std::complex<double>(out[j][0], out[j][1])
            * std::complex<double>( -cos(j * M_PI / N), -sin(j*M_PI/N))
            + jump * Gzero_w[j];
    }

    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);
}


void diagmc::do_FFT_backward_green(const std::vector<std::complex<double> >& Green_w,
                                   std::vector<double>& Green_t,
                                   const std::vector<double>& Gzero_t) const
{
    size_t N = number_of_frequencies;

    fftw_complex *in, *out;
    fftw_plan p;
    in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N );
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N );
    p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE); // -1 in exponent

    for (size_t j = 0; j < N/2; j++) {
        in[j][0] = real(Green_w[j] * std::complex<double>(cos(j * M_PI / N),
                                                          -sin(j*M_PI/N))) / max_time;
        in[j][1] = imag(Green_w[j] * std::complex<double>(cos(j * M_PI / N),
                                                          -sin(j*M_PI/N))) / max_time;
    }
    for (size_t j = N/2; j < N; j++) {
        in[j][0] = real(Green_w[j] * std::complex<double>(-cos(j * M_PI / N),
                                                          sin(j*M_PI/N))) / max_time;
        in[j][1] = imag(Green_w[j] * std::complex<double>(-cos(j * M_PI / N),
                                                          sin(j*M_PI/N))) / max_time;
    }

    fftw_execute(p);

    for (size_t j = 0; j < N; j++) {
        Green_t[j] = out[j][0] + Gzero_t[j];
    }

    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);
}

/*
void diagmc::forward_fourier_mid(const std::vector<double>& G_in,
                                 std::vector<std::complex<double> >& G_out,
                                 const std::vector<double>& vec_tau_in,
                                 const std::vector<double>& vec_omega_in) const
{
    // forward fourier transform when the tau grid is in the middle of the interval
    // use simpson integration without end points:
    // coeff = 4-2-4-2-... , weight = h/3
    if (G_in.size() != vec_tau_in.size())
    {
        std::cerr << "# Error in forward fourier with time sizes : "
                  << G_in.size() << "\t" << vec_tau_in.size() << "\n";
        throw std::exception();
    }
    if (G_out.size() != vec_omega_in.size())
    {
        std::cerr << "# Error in forward fourier with frequency sizes : "
                  << G_out.size() << "\t" << vec_omega_in.size() << "\n";
        throw std::exception();
    }
    // distinguishable particle.
    double dt = vec_tau_in.at(1) - vec_tau_in.at(0);
    size_t Nt = vec_tau_in.size();

    // zero frequency
    G_out[0] = std::complex<double>(0., 0.);
    for (unsigned int it = 0; it < Nt; it+=2)
    {
        G_out[0] +=  4 * G_in[it];
    }
    for (unsigned int it=1; it< Nt; it+=2)
    {
        G_out[0] +=  2 * G_in[it];
    }

    G_out[0] *= dt/3;


    // other frequencies
    for (unsigned int iw = 1; iw < vec_omega_in.size(); iw++)
    {
        double omega = vec_omega_in[iw];
        G_out[iw] = std::complex<double>(0.,0.);
        for (unsigned int it = 0; it < Nt; it+=2)
        {
            double tau = vec_tau_in.at(it);
            G_out[iw] += 4 * G_in[it] * std::complex<double>(cos(omega*tau),
                                                             sin(omega*tau));
        }
        for (unsigned int it=1; it< Nt; it+=2)
        {
            double tau = vec_tau_in.at(it);
            G_out[iw] += 2 * G_in[it] * std::complex<double>(cos(omega*tau),
                                                             sin(omega*tau));
        }
        G_out[iw] *= dt/3;
    }
    // ...distinguishable
}

void diagmc::backward_fourier(const std::vector<std::complex<double> >& G_in,
                              std::vector<double>& G_out,
                              const std::vector<double>& vec_tau_in,
                              const std::vector<double>& vec_omega_in) const
{
    if (G_out.size() != vec_tau_in.size()) {
        std::cerr << "# Error in backward fourier with time sizes : "
                  << G_out.size() << "\t" << vec_tau_in.size() << "\n";
        throw std::exception();
    }
    if (G_in.size() != vec_omega_in.size())
    {
        std::cerr << "# Error in backward fourier with frequency sizes : "
                  << G_in.size() << "\t" << vec_omega_in.size() << "\n";
        throw std::exception();
    }
    // distinguishable particle
    // omega grid is equidistant at integer points
    double domega_loc = vec_omega_in[1] - vec_omega_in[0];
    size_t Nw = vec_omega_in.size();
    for (unsigned int it = 0; it < vec_tau_in.size(); it++)
    {
        double tau = vec_tau_in.at(it);

        G_out[it] = real(G_in[0])*4./3.;
        for (unsigned int iw = 1; iw < Nw; iw+=2)
            G_out[it] += (real(G_in[iw])*cos(vec_omega_in[iw]*tau)
                          + imag(G_in[iw])*sin(vec_omega_in[iw]*tau)) * 2 * 2/3.;
        for (unsigned int iw = 2; iw < Nw; iw+=2)
            G_out[it] += (real(G_in[iw])*cos(vec_omega_in[iw]*tau)
                          + imag(G_in[iw])*sin(vec_omega_in[iw]*tau)) * 2 * 4/3.;
        G_out[it] *= domega_loc / (2*M_PI);
    }
}
*/
