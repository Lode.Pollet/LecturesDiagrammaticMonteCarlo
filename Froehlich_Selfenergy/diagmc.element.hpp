// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "momentum.hpp"

#include <iostream>
#include <list>
#include <vector>
#include <algorithm>

#include <alps/hdf5/archive.hpp>
#include <alps/hdf5/vector.hpp>


const int dim = 3;

typedef double TimeType;
typedef Momentum_type<double, dim> Momentum_t;

class vertex_type {
public:
    vertex_type() {};
    vertex_type(const TimeType t, const Momentum_t& in, const Momentum_t& out, const Momentum_t& v, const std::list<vertex_type>::iterator link) {
        mTime = t;
        mIn_momentum = in;
        mOut_momentum = out;
        mInteraction_momentum = v;
        mLink = link;
    }
    vertex_type(const TimeType t, const Momentum_t& in, const Momentum_t& out, const Momentum_t& v) {
        mTime = t;
        mIn_momentum = in;
        mOut_momentum = out;
        mInteraction_momentum = v;
    }
    void init(const TimeType t, const Momentum_t& in, const Momentum_t& out, const Momentum_t& v, const std::list<vertex_type>::iterator link) {
        mTime = t;
        mIn_momentum = in;
        mOut_momentum = out;
        mInteraction_momentum = v;
        mLink = link;
    }
    void init(const TimeType t, const Momentum_t& in, const Momentum_t& out, const Momentum_t& v) {
        mTime = t;
        mIn_momentum = in;
        mOut_momentum = out;
        mInteraction_momentum = v;
    }
    ~vertex_type() {};
    vertex_type(const vertex_type& rhs) {
        mTime = rhs.mTime;
        mIn_momentum = rhs.mIn_momentum;
        mOut_momentum = rhs.mOut_momentum;
        mInteraction_momentum = rhs.mInteraction_momentum;
        mLink = rhs.mLink;
    };
    vertex_type& operator=(const vertex_type& rhs) {
        mTime = rhs.mTime;
        mIn_momentum = rhs.mIn_momentum;
        mOut_momentum = rhs.mOut_momentum;
        mInteraction_momentum = rhs.mInteraction_momentum;
        mLink = rhs.mLink;
        return *this;
    };
    friend std::ostream &operator<<(std::ostream &os, vertex_type& rhs) {
        os << rhs.time() << "\t"
           << rhs.in() << "\t"
           << rhs.out() << "\t"
           << rhs.v() << "\n";
        return os;
    };
    friend std::istream& operator>>(std::istream& is, vertex_type& vt) {
        is >> vt.mTime >> vt.mIn_momentum >> vt.mOut_momentum >> vt.mInteraction_momentum; // no point in streaming the link
        return is;
    };
    TimeType time() const { return mTime; }
    Momentum_t& in()  { return mIn_momentum; }
    const Momentum_t& in() const  { return mIn_momentum; }
    Momentum_t& out()  { return mOut_momentum; }
    const Momentum_t& out() const  { return mOut_momentum; }
    Momentum_t& v()  { return mInteraction_momentum; }
    const Momentum_t& v() const  { return mInteraction_momentum; }
    std::list<vertex_type>::iterator link() { return mLink; }
    const std::list<vertex_type>::const_iterator link() const { return mLink; }
    void time(const TimeType t) { mTime = t; }
    void in(const Momentum_t& p) { mIn_momentum = p; }
    void out(const Momentum_t& p) { mOut_momentum = p; }
    void v(const Momentum_t& p) { mInteraction_momentum = p; }
    void link(const std::list<vertex_type>::iterator l) { mLink = l; }

    mutable int name;

private:
    TimeType mTime;
    Momentum_t mIn_momentum;
    Momentum_t mOut_momentum;
    Momentum_t mInteraction_momentum;
    std::list<vertex_type>::iterator mLink;
};
