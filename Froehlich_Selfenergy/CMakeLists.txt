cmake_minimum_required(VERSION 2.8.12)
cmake_policy(SET CMP0060 NEW)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
project(froehlich_selfenergy CXX)

SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

if(DEFINED DEBUGMODE)
  if(DEBUGMODE EQUAL 1)
    add_definitions(-DDEBUGMODE)
  endif()
endif()

add_executable(${PROJECT_NAME}_single
  diagmc.cpp
  diagmc.update.cpp
  diagmc.output.cpp
  diagmc.fourier.cpp
  faddeeva.cpp
  checkpointing_stop_callback.cpp
  main.cpp
  )
add_executable(${PROJECT_NAME}
  diagmc.cpp
  diagmc.update.cpp
  diagmc.output.cpp
  diagmc.fourier.cpp
  faddeeva.cpp
  checkpointing_stop_callback.cpp
  main_mpi.cpp
  )
add_executable(${PROJECT_NAME}_bold
  bold_diagmc.cpp
  bold_diagmc.update.cpp
  diagmc.cpp
  diagmc.update.cpp
  diagmc.output.cpp
  diagmc.fourier.cpp
  faddeeva.cpp
  checkpointing_stop_callback.cpp
  main_mpi.cpp
  )
target_compile_definitions(${PROJECT_NAME}_bold PRIVATE -DBOLD)
find_package(ALPSCore REQUIRED mc accumulators params)

# Use ALPSCore_LIBRARIES variable to link to ALPSCore 
target_link_libraries(${PROJECT_NAME}_single ${ALPSCore_LIBRARIES} fftw3)
target_link_libraries(${PROJECT_NAME} ${ALPSCore_LIBRARIES} fftw3)
target_link_libraries(${PROJECT_NAME}_bold ${ALPSCore_LIBRARIES} fftw3)
