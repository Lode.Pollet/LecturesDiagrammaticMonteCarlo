// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "diagmc.element.hpp"
#include "coordinate.hpp"
#include "matrix.hpp"

#include <fstream>
#include <vector>
#include <string>
#include <stdexcept>
#include <cmath>
#include <random>

#include <alps/params.hpp>
#include <alps/accumulators.hpp>
#include <alps/hdf5/archive.hpp>
#include <alps/hdf5/vector.hpp>
#include <alps/mc/mcbase.hpp>
#include <alps/mc/api.hpp>
#include <alps/mc/stop_callback.hpp>

#include <boost/function.hpp>


typedef std::mt19937 base_generator_type;

enum statistics_tag {
    impossible,
    rejected,
    accepted,
    total_attempted,
    statistics_count
};
namespace {
    const std::string statistics_names[] = {
        "impossible",
        "rejected",
        "accepted",
        "total_attempted"
    };
    const std::string update_names[] = {
        "insert",
        "remove",
        "dress",
        "undress",
        "swap",
        "extend",
        "to_fake",
        "from_fake",
        "change_p",
        "change_time"
    };
}

class diagmc : public alps::mcbase {
public:
    class invalid_configuration : public std::logic_error {
        using std::logic_error::logic_error;
    };

    typedef std::list<vertex_type> Diagram_type;

    diagmc(parameters_type const & params, size_t seed_offset = 0);

    void update();
    void measure();
    void measure_bin();
    virtual double fraction_completed() const;

    static std::string code_name();
    static void define_parameters(parameters_type&);
    static void define_specific_parameters(parameters_type&);

    using alps::mcbase::save;
    using alps::mcbase::load;
    virtual void save(alps::hdf5::archive & ar) const;
    virtual void load(alps::hdf5::archive & ar);

    double dispersion(const double p_sq, const double) const;
    double dispersion(const Momentum_t& p, const double) const;
    double bare_propagator(const double p_sq,
                           const double mu,
                           const double dt) const;
    double bare_propagator(const Momentum_t&,
                           const double mu,
                           const double dt) const;
    std::complex<double> bare_propagator(const std::complex<double>,
                                         const double,
                                         const double) const;
    std::complex<double> bare_propagator(const std::complex<double>,
                                         const Momentum_t&,
                                         const double) const;

    void print_params(std::ostream& os) const;
    void initialize();
    void init_diagram();
    statistics_tag INSERT();
    statistics_tag REMOVE();
    statistics_tag DRESS();
    statistics_tag UNDRESS();
    statistics_tag SWAP();
    statistics_tag EXTEND();
    statistics_tag TO_FAKE();
    statistics_tag FROM_FAKE();
    statistics_tag CHANGE_P();
    statistics_tag CHANGE_TIME();

    void print_update_statistics(std::ostream& os) const;
    void reset_update_statistics();
    void fill_hists();
    void reset_hists();
    void reset_sweeps();
    void output_final(const alps::results_type<diagmc>::type &,
                      alps::hdf5::archive& ar);

    inline bool Metropolis(const double& x) {
        return x > 1 || random() < x;
    }

    size_t get_order() const {return order;}
    size_t get_max_order() const {return max_order;}
    double get_max_momentum() const {return max_momentum;}

    void print_times_green(std::ostream&,
                           const size_t,
                           const alps::accumulators::result_wrapper&) const;
    void print_times_sgo(std::ostream&,
                         const size_t,
                         const alps::accumulators::result_wrapper&,
                         const alps::accumulators::result_wrapper&) const;
    void print_times_self(std::ostream&,
                          const size_t,
                          const alps::accumulators::result_wrapper&,
                          const alps::accumulators::result_wrapper&) const;
    void compute_energy_self(std::ostream&,
                             const alps::accumulators::result_wrapper&,
                             const alps::accumulators::result_wrapper&) const;
    virtual void fourier_analysis(std::ostream&,
                                  const size_t,
                                  const alps::accumulators::result_wrapper&,
                                  const alps::accumulators::result_wrapper&);

    virtual double compute_configuration_energy() const;

    virtual void test_conf(const Diagram_type& d) const;
    void test_conf() const {
        test_conf(diagram);
    }
    void print_diagram();
    void print_diagram(Diagram_type&);

    void do_FFT_forward_selfenergy(const std::vector<double>&,
                                   std::vector<std::complex<double> >&,
                                   const double,
                                   const std::vector<double>&,
                                   const std::vector<std::complex<double> >&) const;
    void do_FFT_backward_green(const std::vector<std::complex<double> >&,
                               std::vector<double>&,
                               const std::vector<double>&) const;

    bool is_not_close(const double val1, const double val2, const double tol) const {
        bool answ = false;
        if (fabs(val1) > 1.) {
            if (fabs(1. - val1/val2) > tol) return true;
        }
        else {
            if (fabs(val1 - val2) > tol) return true;
        }
        return answ;
    }

    double get_self_one(const double p, const double tau) const;

protected:
    // Hamiltonian parameters
    double mu, mu_zero; // chemical potential
    double alpha, vertex_amplitude_sq; // disorder amplitude, gaussian noise is assumed
    double omega_p;
    size_t p_init;
    double mass; // mass of the particle

    size_t number_of_momenta, number_of_times;
    size_t number_of_frequencies;

    size_t N_hist, N_loop;
    size_t max_order; // maximum expansion order
    double max_time;
    double max_momentum, max_momentum_sq;

    size_t order;
    Matrix<double> update_statistics;

    Diagram_type diagram;
    double diagram_time;
    Momentum_t diagram_p;
    size_t diagram_p_index;
#ifdef DEBUGMODE
    double diagram_weight;
#endif


    Grid_1d_t Time_self_grid;
    Grid_1d_t Momentum_self_grid;
    Grid_1d_t FFTgrid;

    base_generator_type rng;
    std::uniform_real_distribution<double> uni_dist;
    inline double random () { return uni_dist(rng); }

    enum update_tag {
        insert,
        remove,
        dress,
        undress,
        swap,
        extend,
        to_fake,
        from_fake,
        change_p,
        change_time,
        update_count
    };
    double update_prob_cuml[update_count];
    double update_prob[update_count];

    template <typename Callee, statistics_tag (Callee::* f)()>
    struct caller {
        typedef Callee & Callee_ref;
        caller (Callee &c) : c(c) {}
        statistics_tag operator() () { return (c.*f)(); }
    private:
        Callee_ref c;
    };

    template <update_tag tag, typename Callee, statistics_tag (Callee::* update_func)()>
    inline bool try_log_update(double q) {
        if (q >= update_prob_cuml[tag]) {
            return false;
        }
#ifdef DEBUGMODE
        std::cout << "\nBegin " << update_names[tag] << ": ";
#endif
        using Functor = caller<Callee, update_func>;
        statistics_tag s = Functor(static_cast<typename Functor::Callee_ref>(*this))();
#ifdef DEBUGMODE
        std::cout << "\nResult of " << update_names[tag] << ": " << statistics_names[s];
#else
        update_statistics(total_attempted, tag) += 1.;
        update_statistics(s, tag) += 1.;
#endif
        return true;
    }

private:
    void initialize_update_prob(update_tag begin, update_tag end);
    void initialize_update_prob();

private:
    unsigned long sweeps;
    unsigned long thermalization_sweeps;
    unsigned long total_sweeps;

    double energy_guess;
    double energy_spread;

    //measurement variables
    double MCfake;

    Matrix<double> hist_green_zero;
    Matrix<double> hist_SGo_first;
    Matrix<double> hist_SGo_other;
    Matrix<double> hist_selfenergy_first;
    Matrix<double> hist_selfenergy_other;

    std::vector<double> Energies_MC;
    std::vector<double> Hist_Energies_MC;
    std::vector<double> Hist_Energies_other_MC;
    long Nenergies_MC;

    double hist_order;
    std::vector<double> order_frequencies;
};


inline double diagmc::dispersion(const double p_sq, const double mu) const {
    return 0.5 * p_sq/mass - mu;
}

inline double diagmc::dispersion(const Momentum_t& p, const double mu) const {
    return p.r_sq()/2./mass - mu;
}

inline double diagmc::bare_propagator(const Momentum_t& p,
                                      const double mu,
                                      const double dt) const
{
    return exp(-(dispersion(p, mu)*dt));
}

inline double diagmc::bare_propagator(const double p_sq,
                                      const double mu,
                                      const double dt) const
{
    return exp(-(dispersion(p_sq, mu))*dt );
}


inline std::complex<double> diagmc::bare_propagator(const std::complex<double> omega_n,
                                                    const double p_sq,
                                                    const double mu) const
{
    return 1./(omega_n - dispersion(p_sq, mu));
}


inline std::complex<double> diagmc::bare_propagator(const std::complex<double> omega_n,
                                                    const Momentum_t& p,
                                                    const double mu) const
{
    return bare_propagator(omega_n, p.r_sq(), mu);
}
