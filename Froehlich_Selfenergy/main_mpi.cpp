// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef BOLD
#include "bold_diagmc.hpp"
#else
#include "diagmc.hpp"
#include <alps/mc/mpiadapter.hpp>
#endif

#include "checkpointing_stop_callback.hpp"
#include "comment.hpp"
#include "copyright.hpp"

#include <string>
#include <iostream>
#include <cstring>
#include <stdexcept>

#include <alps/accumulators.hpp>
#include <alps/mc/api.hpp>
#include <alps/utilities/fs/remove_extensions.hpp>

#ifdef BOLD
typedef bold_diagmc simulation_t;
#else
typedef alps::mcmpiadapter<diagmc> simulation_t;
#endif

int main(int argc, const char *argv[]) {
    try {
        comment_stream cs(std::cout, "# ");

        // communicator needs a NULL-terminated argv for some reason
        char **argv2 = new char*[argc+1];
        for (size_t i = 0; i < argc; ++i) {
            argv2[i] = new char[strlen(argv[i])+1];
            strcpy(argv2[i], argv[i]);
        }
        argv2[argc] = NULL;

        alps::mpi::environment env(argc, argv2, false);
        alps::mpi::communicator c;

        typedef alps::parameters_type<diagmc>::type params_type;
        params_type parameters(argc, argv, "/parameters"); // reads from HDF5 if need be

        if (c.rank() == 0) {
            if (!parameters.is_restored()) {
                simulation_t::define_parameters(parameters);
                simulation_t::define_specific_parameters(parameters);
                parameters.define<size_t>("checkpointtime", 0,
                                          "time between checkpoints");
            }
            if (parameters.help_requested(std::cerr))
                return 1; // Stop if help requested.

            // legal
            cs << simulation_t::code_name() << std::endl;
            print_copyright(cs);
        }
        parameters.broadcast(c, 0);

        if (parameters["outputfile"].as<std::string>().empty()) {
            parameters["outputfile"] = alps::fs::remove_extensions(alps::origin_name(parameters)) + ".out.h5";
        }

        simulation_t sim(parameters, c);

        // If needed, restore the last checkpoint
        std::string checkpoint_file = [&] {
            std::stringstream ss;
            ss << alps::fs::remove_extensions(parameters["checkpoint"])
               << ".clone"
               << c.rank()
               << ".h5";
            return ss.str();
        } ();
        if (parameters.is_restored()) {
            cs << "Restoring checkpoint from " << checkpoint_file
               << std::endl;
            alps::hdf5::archive ar(checkpoint_file, "r");
            sim.load(ar);
            ar.close();
        }

        if (c.rank() == 0) {
            sim.print_params(cs);
        }

        auto make_checkpoint = [&] {
            // Checkpoint the simulation
            cs << "Checkpointing simulation: "
               << checkpoint_file
               << std::endl;
            sim.save(checkpoint_file);
        };
        sim.run(checkpointing_stop_callback(c,
                                            size_t(parameters["timelimit"]),
                                            size_t(parameters["checkpointtime"]),
                                            make_checkpoint));

        // Final checkpoint
        make_checkpoint();

        alps::results_type<diagmc>::type results;
        results = alps::collect_results(sim);

        if (c.rank() == 0) {
            cs << std::endl << "Results:" << std::endl
               << results << std::endl;

            // Saving to the output file
            std::string output_file = parameters["outputfile"];
            alps::hdf5::archive ar(output_file, "w");
            ar["/parameters"] << parameters;
            ar["/simulation/results"] << results;
            sim.output_final(results, ar);
        }
    } catch (std::exception const & e) {
        std::cerr << "Caught exception: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
