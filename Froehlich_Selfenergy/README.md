Fröhlich Polaron: Self-energy Formalism
=======================================

Please refer to the [README in the parent directory][1] for general information
on our codes. Refer to parts IV and V of the main text for a discussion of the
update scheme, Fourier transform, and results.

The calculation of the Green function from the self-energy is achieved by
carrying out the Fourier transform to Matsubara frequency space where the Dyson
equation takes on an algebraic form. To this end, we employ the FFTW3 library.

This code implements both the bare expansion as discussed in part IV of the main
text, as well as the iterative bold expansion, discussed in part V therein. The
latter introduces a self-consistency loop where the Green function is
periodically calculated from the self-energy and used as the bold propagator in
the updates to follow, a step we refer to as _boldification_. Crucially, we do
not reset the observables for the self-energy after boldification. This allows
to improve the result further by decreasing the statistical error bars after the
iteration has qualitatively converged.

The logic for the bare expansion of the self-energy can be found in the `diagmc`
class. Where necessary, its functions have been overridden (or hidden) by the
`bold_diagmc` class which is used to build the `froehlich_selfenergy_bold`
executable. The latter also provides a custom `::run` function which invokes
boldification and broadcasts the new bold propagator among the MPI processes.
For this reason, the bold version requires the use of MPI.

  [1]: ../README.md#diagrammatic-monte-carlo-for-the-fr%C3%B6hlich-polaron

Executables
-----------

  * `froehlich_selfenergy_single`: bare expansion of self-energy, serial
    executable, supports checkpointing
  * `froehlich_selfenergy`: bare expansion of self-energy, parallelized with
    MPI, supports checkpointing
  * `froehlich_selfenergy_bold`: bold expansion of the self-energy, parallelized
    with MPI, supports checkpointing

Configuration Options
---------------------

  * `-DDEBUGMODE=1`: enable verbose debug mode; prints diagnostics for each and
    every update.
  * `-DCMAKE_BUILD_TYPE=(Release | Debug)`: `Debug` compiles unoptimized code
    with debug symbols (`-g`); `Release` compiles optimized code and should be
    used in a production setting.

Parameters
----------

Consult `./froehlich_selfenergy --help` and `./froehlich_selfenergy_bold` for
complete lists of supported parameters and their default values.

#### Model parameters
  * `alpha`: electron-phonon coupling constant _α_
  * `mass`: electron mass _m_
  * `omega_p`: phonon frequency (Einstein dispersion) ω\__ph_
  * `mu_zero`: chemical potential _μ_
  * `p_init`: external momentum _index_, [0, `number_of_momenta`]

#### Discretization parameters
  * `min_time` (**bold** only): minimum non-zero imaginary time in logarithmic grid
  * `max_time`: maximum imaginary time _τ_ of the diagram
  * `number_of_times`: number of grid points in imaginary time
  * `max_momentum`: maximum external momentum value
  * `number_of_momenta`: number of equidistant momentum grid points from 0 to
    `max_momentum`
  * `number_of_frequencies`: number of Matsubara frequencies at which the Dyson
    equation is evaluated; refer to Sec. IV A in the main text for a thorough
    discussion 

The time grid is chosen differently for the bare and bold versions of the
code:
  * in the bare expansion, the time grid is equidistant between 0 and
    `max_time`;
  * in the **bold** expansion, 0 is a grid point, the rest are chosen from a
    logarithmic grid between `min_time` and `max_time`:

        tau_k = min_time * (max_time / min_time)^(k / (number_of_times-1))

    with `k` = 0, 1, ..., `number_of_times-1`.

Note that during sampling the imaginary time is continuous but the Green
function is measured according to a histogram in imaginary time. In contrast,
the external momentum can only assume grid points and the `CHANGE_P` update is
used to switch among them in fake diagrams. Thus, if `number_of_momenta` > 0,
the `CHANGE_P` update needs to be turned on, i.e. `p_change_p` > 0.

As for defaults, the `number_of_momenta` defaults to 0 in the bare expansion
(thus the external momentum is fixed at 0); in the **bold** expansion, it
defaults to a non-zero value.

#### Energy estimation

In order to estimate the polaron energy directly from the self-energy function
using Eq. (16) of the main text, we sample the RHS, the quantity designated ϕ(E)
at a number of different energies, centered around a guess for the polaron
energy. The density of energies under consideration is highest close to the
guess. This is controlled by these parameters:
  * `number_of_energies`: number of energies to calculate and measure ϕ(E) at;
    large values may incur long (and unnecessary) measurements
  * `energy_guess`: guess for the polaron energy to center search around; e.g.
    from asymptotic decay of Green function or previous simulation
  * `energy_spread`: percentage of the guess that the energies are scattered
    around

`number_of_energies` = 0 is a perfectly valid choice to forego the definition
and measurement of the energy ϕ(E) observables completely. Keep in mind that
these are sensible only if the external momentum remains fixed, i.e.
`p_change_p` = 0 and only in the bare expansion, in particular.

#### Monte Carlo parameters
  * `thermalization`: number of update sweeps before starting to measure; this
    has to be done for each MPI process separately
  * `sweeps`: number of update sweeps (and interwoven measurements) to be
    carried out after thermalization
    
    In the **bold** expansion, `sweeps` refers to the number of sweeps in each
    bold iteration. Also, `thermalization` sweeps are carried out after each
    boldification and for each MPI process separately.
    
  * `N_hist`: number of elementary updates to be carried out before histogram
    counters are increased; increase this if autocorrelations are strong
  * `N_loop`: number of histogram samples to be taken before the histograms are
    measured and the sweep is complete; 1 sweep = `N_loop` × `N_hist` elementary
    updates; increase this if the measurement of the histograms takes too much
    memory bandwidth (at the the expense of error analysis)
  * `max_order`: maximum diagram order to consider; can be choosen high enough
    to not matter at all; imposed only for technical reasons
  * `p_*`: proposal probabilities for the individual update types; these consist
    of two sets:
      - for fake diagrams: `p_from_fake`, `p_change_time`, `p_change_p`
      - for real diagrams: `p_insert`, `p_remove`, `p_dress`, `p_undress`,
        `p_swap`, `p_extend`, `p_to_fake`

    These probabilities are _not_ cumulative, but actual probabilities, and
    should add up to unity within each set. If the latter is violated, the
    probabilities are renormalized accordingly and the simulation will still run.
    
    For the **bold** expansion, the `INSERT` and `REMOVE` updates are disabled and
    `DRESS` / `UNDRESS` are used exclusively to change orders. Consequently, the
    `p_insert` and `p_remove` parameters are removed.
    
    In the bare expansion, the `CHANGE_P`, `DRESS`, and `UNDRESS` updates are
    disabled by default, but can be turned on. In the **bold** expansion, they are
    all mandatory.

#### Simulation control 
  * `Tmin`, `Tmax` (MPI only): control the schedule of the checks of whether the
    simulation has finished yet
  * `timelimit`: seconds after which simulation will shutdown upon next check,
    even if `sweeps` have not been completed; 0 means no time limit imposed
  * `checkpointtime`: seconds between periodic checkpoints; 0 means no periodic
    checkpoints, but final checkpoint will still be written before simulation
    terminates.
  
For the **bold** expansion _only_, we additionally have these parameters:
  * `bold_max_nr_iteration`: number of bold iterations of `sweeps` upate sweeps
    that will be carried out at most before the program terminates
  * `convergence_threshold`: if the norm of the change of the Green function due
    to boldification drops below `convergence_threshold`, the simulation is
    considered converged and terminates even before `bold_max_nr_iteration` is
    reached.

### Output files
  * `job.out.h5`: HDF5 file containing ALPSCore observables, binning data, and
    Jackknife analyses of normalized quantities
  * `job.green`: full Green function data; column format:
    - τ: imaginary time
    - -G_0(τ): analytical exact value of the negative bare Green function
    - ⟨-(Σ_1*G_0)(τ)⟩: mean value of the convolution of first-order self-energy
      with bare Green function
    - -(Σ_1*G_0)(τ): analytical exact value of the convolution of first-order
      self-energy with bare Green function
    - -G(τ): negative full Green function as obtained from the Fourier transform
      based on measured data
    - ln(-G(τ)): logarithm of the negative full Green function as obtained from
      the Fourier transform based on measured data
  * `job.green0`: bare Green function data; column format:
    - τ: imaginary time
    - ⟨-G_0(τ)⟩: mean value of negative bare Green function
    - σ(-G_0(τ)): error on the mean of the above
    - -G_0(τ): analytical exact value of the negative bare Green function
  * `job.sigma`: self-energy data; column format:
    - τ: imaginary time
    - ⟨-Σ_1(τ)⟩: mean value of negative first-order self-energy
    - σ(-Σ_1(τ)): error on the mean of the above
    - -Σ_1(τ): analytical exact value of the negative first-order self-energy
    - ⟨-Σ_{>1}(τ)⟩: mean value of negative higher-order self-energy
    - σ(-Σ_{>1}(τ)): error on the mean of the above
    - ⟨-Σ(τ)⟩: mean value of negative full self-energy
    - σ(-Σ(τ)): error on the mean of the above
  * `job.SGo`: data for the convolution -(Σ*G_0)(τ); column format is analogous
    to that of `job.sigma`
  * `job.updates`: Monte Carlo update statistics

For the **bold** expansion _only_, there is a file `job.bold_<n>` written
after each boldification iteration step where `<n>` is the step index. The
column format is identical to that of `job.green`.

All these quantities are understood to be evaluated at zero momentum.

### Example parameter files

  * `fig15_mu60.ini`: parameters to reproduce Fig. 15 of the main text with _μ_=-6
  * `fig15_mu56.ini`: parameters to reproduce Fig. 15 of the main text with _μ_=-5.6
  
The above parameter files are intended to be used with the bare expansion
executables. They also measure the ϕ(E) observables for energy estimation to
reproduce Fig. 16.

The last one is intended to be used with the bold expansion executable:
  * `fig24.ini`: parameters to reproduce Fig. 24 of the main text, i.e. _α_=5,
    _μ_=-6
