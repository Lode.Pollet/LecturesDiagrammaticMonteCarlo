// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "diagmc.hpp"

#include <string>
#include <sstream>

#include <alps/utilities/fs/remove_extensions.hpp>


void diagmc::print_params(std::ostream& os) const {
    os << "Simulation parameters:\n";
    for (auto kv : parameters) {
        os << kv.first << " = ";
        print(os, kv.second, true);
        os << '\n';
    }
    os << "\n\n\n";
}

void diagmc::output_final(const alps::results_type<diagmc>::type & results,
                          alps::hdf5::archive& ar)
{
    // get basename
    std::string basename = alps::fs::remove_extensions(parameters["outputfile"]);

    double Z_fake = 0.;
    size_t kmin = (update_prob[change_p] < 1e-10 ? p_init : 0);
    size_t kmax = (update_prob[change_p] < 1e-10 ? p_init+1 : number_of_momenta+1);
    for (size_t k=kmin; k < kmax; k++) {
        Momentum_t p;
        p(0) = Momentum_self_grid(k);
        for (size_t idim = 1; idim < dim; idim++)
            p(idim) = 0.;
        Z_fake += 1./ (dispersion(p, mu));
    }

    // Jackknife analyses
    using alps::accumulators::result_wrapper;
    const result_wrapper& normed_G0
        = Z_fake * results["Greenfun_0"] / results["MCfake"];
    const result_wrapper& normed_S1
        = Z_fake * results["Selfenergy_1"] / results["MCfake"];
    const result_wrapper& normed_So
        = Z_fake * results["Selfenergy_other"] / results["MCfake"];
    const result_wrapper& normed_SGo_1
        = Z_fake * results["SGo_1"] / results["MCfake"];
    const result_wrapper& normed_SGo_other
        = Z_fake * results["SGo_other"] / results["MCfake"];

    // save to archive
    ar["/simulation/normed/Greenfun_0"] << normed_G0;
    ar["/simulation/normed/Selfenergy_1"] << normed_S1;
    ar["/simulation/normed/Selfenergy_other"] << normed_So;
    ar["/simulation/normed/SGo_1"] << normed_SGo_1;
    ar["/simulation/normed/SGo_other"] << normed_SGo_other;

    std::ofstream f_updatestat(basename + ".updates");
    print_update_statistics(f_updatestat);
    f_updatestat.close();
    std::ofstream fmom0(basename + ".green0");
    print_times_green(fmom0, 0, normed_G0);
    fmom0.close();
    std::ofstream fmom1(basename + ".sigma");
    print_times_self(fmom1, 0, normed_S1, normed_So);
    fmom1.close();
    std::ofstream fmom2(basename + ".SGo");
    print_times_sgo(fmom2, 0, normed_SGo_1, normed_SGo_other);
    fmom2.close();
    std::ofstream f_fourier(basename + ".green");
    fourier_analysis(f_fourier, 0, normed_SGo_1, normed_SGo_other);
    f_fourier.close();

    if (Nenergies_MC > 0) {
        const result_wrapper& normed_energy
            = Z_fake * results["Selfenergy_energy"] / results["MCfake"];
        const result_wrapper& normed_energy_other
            = Z_fake * results["Selfenergy_energy_other"] / results["MCfake"];

        ar["/simulation/normed/Selfenergy_energy"] << normed_energy;
        ar["/simulation/normed/Selfenergy_energy_other"] << normed_energy_other;

        std::ofstream f_en(basename + ".energy_self");
        compute_energy_self(f_en, normed_energy, normed_energy_other);
        f_en.close();
    }
}

void diagmc::print_times_green(std::ostream& os,
                               const size_t k_index,
                               const alps::accumulators::result_wrapper& normed) const
{
    os << "# Momentum index : " << k_index << "\t"
       << Momentum_self_grid(k_index) << "\n";
    for (size_t i = 0; i < number_of_times; i++) {
        double tau = Time_self_grid[i] ;
        os << tau << "\t"
           << normed.mean<std::vector<double> >()[k_index * number_of_times + i] << "\t"
           << normed.error<std::vector<double> >()[k_index * number_of_times + i] << "\t" << bare_propagator(Momentum_self_grid(k_index) ,mu, tau) << "\n";
    }
}

void diagmc::print_times_sgo(std::ostream& os,
                             const size_t k_index,
                             const alps::accumulators::result_wrapper& s_1,
                             const alps::accumulators::result_wrapper& s_other) const
{
    os << "# Momentum index : " << k_index << "\t"
       << Momentum_self_grid(k_index) << "\n";
    // Derived result:
    const alps::accumulators::result_wrapper& s_tot = s_1 + s_other;

    for (size_t i = 0; i < number_of_times; i++) {
        double tau = Time_self_grid[i] ;
        double sb = exp(mu * tau) * vertex_amplitude_sq * sqrt(M_PI *2 * mass) / (4*M_PI*M_PI) * sqrt(M_PI / omega_p) * erf(sqrt(omega_p * tau)); // only valid for zero momentum
        os  << tau << "\t"
            << s_1.mean<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << s_1.error<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << sb << "\t"
            << s_other.mean<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << s_other.error<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << s_tot.mean<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << s_tot.error<std::vector<double> >()[k_index * number_of_times + i] << "\n"
            ;
    }
}

void diagmc::print_times_self(std::ostream& os,
                              const size_t k_index,
                              const alps::accumulators::result_wrapper& s_1,
                              const alps::accumulators::result_wrapper& s_other) const
{
    os << "# Momentum index : " << k_index << "\t" << Momentum_self_grid(k_index) << "\n";
    // Derived result:
    const alps::accumulators::result_wrapper& s_tot = s_1 + s_other;

    for (size_t i = 0; i < number_of_times; i++) {
        double tau = Time_self_grid[i] ;
        double val = (exp((mu - omega_p)*tau) * vertex_amplitude_sq
                      * sqrt(M_PI * 2 * mass / tau) / (4*M_PI*M_PI)); // only valid for zero momentum
        os  << tau << "\t"
            << s_1.mean<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << s_1.error<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << val << "\t"
            << s_other.mean<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << s_other.error<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << s_tot.mean<std::vector<double> >()[k_index * number_of_times + i] << "\t"
            << s_tot.error<std::vector<double> >()[k_index * number_of_times + i] << "\n"
            ;
    }
}

void diagmc::compute_energy_self(std::ostream& os,
                                 const alps::accumulators::result_wrapper& s_energy,
                                 const alps::accumulators::result_wrapper& s_energy_other) const
{
    for (size_t j = 0; j < Nenergies_MC; j++) {
        std::cout << Energies_MC[j] << "\t"
                  << s_energy.mean<std::vector<double> >()[j] << "\t"
                  << s_energy.error<std::vector<double> >()[j] << "\t"
                  << s_energy_other.mean<std::vector<double> >()[j] << "\t"
                  << s_energy_other.error<std::vector<double> >()[j] << "\n";
        os << Energies_MC[j] << "\t"
           << s_energy.mean<std::vector<double> >()[j] << "\t"
           << s_energy.error<std::vector<double> >()[j] << "\t"
           << s_energy_other.mean<std::vector<double> >()[j] << "\t"
           << s_energy_other.error<std::vector<double> >()[j] << "\n";
    }
}


void diagmc::save(alps::hdf5::archive & ar) const {
    /* Test configuration before saving:
     * if something went wrong since the last checkpoint, we don't
     * want to corrupt the last healthy checkpoint, too.
     */
    test_conf();

    // Most of the save logic is already implemented in the base class
    alps::mcbase::save(ar);
    
    ar["checkpoint/sweeps"] << sweeps;

    ar["checkpoint/order"] << order;
    ar["checkpoint/diagram_time"] << diagram_time;
    ar["checkpoint/diagram_p"] << diagram_p;
    ar["checkpoint/diagram_p_index"] << diagram_p_index;

    ar["checkpoint/update_statistics"] << update_statistics;
    ar["checkpoint/Energies_MC"] << Energies_MC;

    // random number engine
    std::ostringstream engine_ss;
    engine_ss << rng;
    ar["checkpoint/random"] << engine_ss.str();

    // saving current diagram
    int n = 0;
    std::vector<double> conf_vector;

    for (Diagram_type::const_iterator it = diagram.begin();
         it!=diagram.end(); ++it)
    {
        it->name = n;      // because of all the constness of the overloaded save functions this does not work
        n++;
        conf_vector.push_back(it->time());
        for (size_t idim =0; idim < dim; idim++)
            conf_vector.push_back(it->in()(idim));
        for (size_t idim =0; idim < dim; idim++)
            conf_vector.push_back(it->out()(idim));
        for (size_t idim =0; idim < dim; idim++)
            conf_vector.push_back(it->v()(idim));
    }
    ar["checkpoint/conf_vector"] << conf_vector;
    // save current diagram associations
    std::vector<int> linkname(2*order);
    size_t inc = 0;
    for ( Diagram_type::const_iterator it = diagram.begin();  it!=diagram.end(); ++it, inc++) {
        linkname[inc] = (it->link())->name;
    }
    ar["checkpoint/linkname"] << linkname;
}

void diagmc::load(alps::hdf5::archive & ar) {
    alps::mcbase::load(ar);
    // parameters object gets restored in mcbase::save

    // Do not restore the total_sweeps parameter -- we may want to
    // override it when starting from the checkpoint by supplying
    // the --sweeps=N flag.

    // these params cannot change mid-simulation and should be restored
    // from the checkpoint
    thermalization_sweeps = (unsigned long)(parameters["thermalization"]);
    mass = double(parameters["mass"]);
    omega_p = double(parameters["omega_p"]);
    alpha = double(parameters["alpha"]);
    mu_zero = double(parameters["mu_zero"]);
    p_init = double(parameters["p_init"]);
    max_order = double(parameters["max_order"]);
    max_time = double(parameters["max_time"]);
    max_momentum = double(parameters["max_momentum"]);
    number_of_momenta = size_t(parameters["number_of_momenta"]);
    number_of_times = size_t(parameters["number_of_times"]);
    number_of_frequencies = size_t(parameters["number_of_frequencies"]);
    Nenergies_MC = (unsigned int)(parameters["number_of_energies"]);
    energy_guess = double(parameters["energy_guess"]);
    energy_spread = double(parameters["energy_spread"]);
    N_loop = size_t(parameters["N_loop"]);
    N_hist = size_t(parameters["N_hist"]);

    initialize_update_prob();

    ar["measurements"] >> measurements;
    ar["checkpoint/sweeps"] >> sweeps;

    ar["checkpoint/order"] >> order;
    ar["checkpoint/diagram_time"] >> diagram_time;

    ar["checkpoint/diagram_p_index"] >> diagram_p_index;
    ar["checkpoint/diagram_p"] >> diagram_p;
    ar["checkpoint/update_statistics"] >> update_statistics;

    ar["checkpoint/Energies_MC"] >> Energies_MC;

    // random number engine
    std::string engine_str;
    ar["checkpoint/random"] >> engine_str;
    std::istringstream engine_ss(engine_str);
    engine_ss >> rng;

    std::vector<double> conf_vector(2*order * (3*dim + 1));
    ar["checkpoint/conf_vector"] >> conf_vector;
    //std::cout << "# conf_vector :" << conf_vector.size() << "\n";
    diagram.clear();
    size_t inc = 0;
    for (size_t i =0; i < 2*order; i++) {
        double t1 = conf_vector[inc]; inc++;
        Momentum_t mom_in;
        for (size_t idim=0; idim < dim; idim++) {
            mom_in(idim) = conf_vector[inc];
            inc++;
        }
        Momentum_t mom_out;
        for (size_t idim=0; idim < dim; idim++) {
            mom_out(idim) = conf_vector[inc];
            inc++;
        }
        Momentum_t mom_v;
        for (size_t idim=0; idim < dim; idim++) {
            mom_v(idim) = conf_vector[inc];
            inc++;
        }
        vertex_type elem(t1, mom_in, mom_out, mom_v);
        diagram.push_back(elem);
    }

    std::vector<int> linkname(2*order);
    ar["checkpoint/linkname"] >> linkname;
    inc = 0;
    std::vector<Diagram_type::iterator> itlist(2*order);
    Diagram_type::iterator it=diagram.begin();
    for (int i = 0; i <2*order; i++, ++it) {
        itlist[i]=it;
    }
    for (Diagram_type::iterator it = diagram.begin();
         it!=diagram.end();
         ++it, ++inc)
    {
        it->link(itlist[linkname[inc]]);
    }
#ifdef DEBUGMODE
    print_diagram();
#endif
    test_conf();
}

void diagmc::print_update_statistics(std::ostream& os) const {
    std::vector<std::string> name;
    os << std::setprecision(10);
    name.push_back("INSERT           ");
    name.push_back("REMOVE           ");
    name.push_back("DRESS            ");
    name.push_back("UNDRESS          ");
    name.push_back("SWAP             ");
    name.push_back("EXTEND           ");
    name.push_back("TO_FAKE          ");
    name.push_back("FROM_FAKE        ");
    name.push_back("CHANGE_P         ");
    name.push_back("CHANGE_TIME      ");
    os << std::setprecision(10)
       << "\n\n# UPDATE STATISTICS"
       << "\n" << "# col 1 : all updates"
       << "\n" << "# col 2 : impossible updates"
       << "\n" << "# col 3 : rejected updates"
       << "\n" << "# col 4 : accepted updates"
       << "\n" << "# col 5 : acceptance factor with respect to Metropolis ratio only"
       << "\n" << "# col 6 : acceptance factor with respect to all attempts (esp rejections due to p < p_F are taken into account here).\n";
    for (int i = 0; i < update_count; i++) {
        os << "\n" << name[i] << "\t"
           << update_statistics(total_attempted,i) << "\t"
           << update_statistics(impossible,i) << "\t"
           << update_statistics(rejected,i) << "\t"
           << update_statistics(accepted,i) << "\t"
           << update_statistics(accepted,i) / (update_statistics(total_attempted,i) - update_statistics(impossible,i))
           << "\t" << update_statistics(accepted,i) / update_statistics(total_attempted,i);
    }
    os << "\n";
}


void diagmc::print_diagram() {
    print_diagram(diagram);
}

void diagmc::print_diagram(Diagram_type& d) {
    std::cout << "\n# PRINTING DIAGRAM : "
              << "\n# ------------------ "
              << "\n# Nr elements : " << d.size() << "\t" << order
              << "\n";
    if (order == 0) return;
    for (Diagram_type::iterator it = d.begin(); it != d.end(); ++it)
        std::cout << *it << "\n";
    std::cout << "\n# ASSOCIATIONS :\n";
    for (Diagram_type::iterator it = d.begin(); it!= d.end(); ++it) {
        std::cout << it->time() << "\t" << it->link()->time() << "\t";
        if (it->time() > (it->link())->time())
            std::cout << (it->in() - it->out() + it->v()).r_sq() << "\n";
        if (it->time() < (it->link())->time())
            std::cout << (it->in() - it->out() - it->v()).r_sq() << "\n";
    }
    std::cout << "\n\n\n";
    return;
}
