// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "bold_diagmc.hpp"
#include "faddeeva.hpp"

#include <alps/utilities/fs/remove_extensions.hpp>


std::string bold_diagmc::code_name() {
    return "DiagMC simulation of the Froehlich polaron: "
        "Self-energy function via bold expansion";
}


void bold_diagmc::define_specific_parameters(parameters_type& params) {
    params
        .description(bold_diagmc::code_name())
        .define<double>("p_dress", 0.2, "probability for dress update")
        .define<double>("p_undress", 0.2, "probability for undress update")
        .define<double>("p_swap", 0.2, "probability for swap update")
        .define<double>("p_extend", 0.2, "probability for extend update")
        .define<double>("p_to_fake", 0.2, "probability for to_fake update")
        .define<double>("p_from_fake", 0.5, "probability for from_fake update")
        .define<double>("p_change_p", 0.3, "probability for change_p update")
        .define<double>("p_change_time", 0.2, "probability for change_time update")
        .define<size_t>("number_of_momenta", 100, "number of momentum grid points")
        .define<double>("min_time", 1e-6, "minimum time in logarithmic grid")
        .define<double>("convergence_threshold", 1e-7,
                        "bold iteration convergence threshold")
        .define<size_t>("bold_max_nr_iteration", 20,
                        "maximum number of bold iteration steps")
    ;
}


bool bold_diagmc::run(boost::function<bool ()> const & stop_callback) {
    // loop over bold iterations
    for (; iteration_nr < bold_max_nr_iteration; iteration_nr++) {
        if (communicator.rank() == 0)
            std::cout << "# Starting bold iteration nr "
                      << iteration_nr << "/" << bold_max_nr_iteration
                      << std::endl;

        // run simulation until either
        // (i) fraction_completed reaches 1
        // (ii) the time limit is exceeded (i.e. the stop_callback triggered)
        // ::run() returns bool indicating if (i) is the case
        if (!Base::run(stop_callback)) {
            return false;
        }

        if (communicator.rank() == 0)
            std::cout << "# fraction completed: "
                      << Base::fraction_completed()
                      << std::endl;

        // Boldification
        auto results = alps::collect_results(*this);

        bool has_converged;
        if (communicator.rank() == 0) {
            std::cout << "# Boldifying..." << std::endl;
            std::string basename = alps::fs::remove_extensions(parameters["outputfile"]);
            std::stringstream boldname;
            boldname << basename << ".bold_" << iteration_nr;
            std::ofstream boldout(boldname.str());
            has_converged = boldify(boldout, results);
            boldout.close();
        } else {
            // reset measurements on slave processes as these are now contained
            // in master process's measurements. This may not be necessary any
            // more in the future if ALPSCore issue #295 is addressed.
            measurements.reset();
        }

        // broadcast new Green function...
        broadcast_Greenfun();
        broadcast(communicator, has_converged, 0);
        reset_sweeps();

        if (has_converged) {
            if (communicator.rank() == 0)
                std::cout << "# Simulation has converged. Finishing."
                          << std::endl;

            wrap_up();
            return true;
        }
    }
    wrap_up();
    return true;
}


void bold_diagmc::wrap_up() {
    // workaround: ALPS throws exception when collecting results from
    // empty accumulators. Thus, sample a little more before returning.
    bool once = false;
    auto one_time_callback = [&once] {
        if (once)
            return true;
        once = true;
        return false;
    };
    Base::run(one_time_callback);
}


void bold_diagmc::reinitialize_time_grid(double tmin, double tmax) {
    Time_self_grid = Grid_1d_t(0, tmax);
    Time_self_grid.add_point(0);
    for (size_t j=0; j < number_of_times; j++)
        Time_self_grid.add_point(tmin * pow(tmax / tmin,
                                            1. * j / (number_of_times - 1)));
}


void bold_diagmc::init_Greenfun() {
    for (size_t i=0; i < number_of_momenta+1; i++) {
        for (size_t j=0; j < number_of_times; j++)
            Greenfun_t(i,j) = (-dispersion(Momentum_self_grid(i) * Momentum_self_grid(i), mu)
                               * Time_self_grid[j]);
    }
}


void bold_diagmc::fourier_analysis(std::ostream& os, const size_t mom_indx,
                                   const alps::accumulators::result_wrapper& sgo_1,
                                   const alps::accumulators::result_wrapper& sgo_other)
{
    std::vector<double> self_t_one(number_of_times);
    std::vector<double> self_t_other(number_of_times);
    std::vector<double> self_t_dense(number_of_frequencies);
    std::vector<double> G0_t(number_of_frequencies);
    std::vector<double> G_t_dense(number_of_frequencies);
    std::vector<double> G_t(number_of_times);
    std::vector<std::complex<double> > self_w(number_of_frequencies);
    std::vector<std::complex<double> > G0_w(number_of_frequencies);
    std::vector<std::complex<double> > G_w(number_of_frequencies);
    std::vector<double> omega_n(number_of_frequencies);
    std::vector<double> conv_t(number_of_frequencies);
    std::vector<std::complex<double> > conv_w(number_of_frequencies);
    std::vector<double> self_t_one_dense(number_of_frequencies);
    std::vector<std::complex<double> > self_w_one_dense(number_of_frequencies);

    // statistics of self energy
    double p_sq = Momentum_self_grid(mom_indx) * Momentum_self_grid(mom_indx);

    for (size_t i = 0; i < number_of_times; i++) {
        double tau = Time_self_grid[i];
        self_t_one[i] = (get_self_one(Momentum_self_grid(mom_indx), tau)
                         - get_self_one(0, tau) + Selfenergy_dg1(mom_indx, i));
        self_t_other[i] = sgo_other.mean<std::vector<double> >()[mom_indx * number_of_times + i];
    }

    // initialization of bare green functions
    for (size_t j = 0; j < number_of_frequencies; j++) {
        omega_n[j] = (j < number_of_frequencies/2
                      ? (2 * j ) * M_PI / max_time
                      : -(2. * (number_of_frequencies -j)) * M_PI / max_time); // bosonic
        G0_w[j] = 1./(std::complex<double>(-dispersion(p_sq, mu), omega_n[j]));
    }

    for (size_t i = 0; i < number_of_frequencies; i++) {
        double tau = FFTgrid[i];
        G0_t[i] = -bare_propagator(p_sq, mu, tau);
        // make selfenergy dense by interpolation
        self_t_one_dense[i] = Time_self_grid.quadratic_interpolation_mid(tau, self_t_one);
        self_t_dense[i] = Time_self_grid.quadratic_interpolation_mid(tau, self_t_other);
        if (p_sq < 2 * mass * omega_p) {
            double wp = sqrt(omega_p - p_sq/(2*mass));
            conv_t[i] = (vertex_amplitude_sq * sqrt(mass * M_PI)
                         * exp(-dispersion(p_sq, mu) * tau) * pow(2*M_PI, -1.5)
                         / wp * erf(wp * sqrt(tau)));
        } else {
            double wp = sqrt(p_sq/(2*mass) - omega_p);
            conv_t[i] = (vertex_amplitude_sq * sqrt(mass) * pow(2*M_PI, -1.5)
                         / wp * 2 * exp(-(omega_p - mu)*tau)
                         * Faddeeva::Dawson(wp * sqrt(tau)));
        }
    }

    // forward fourier of the selfenergy
    do_FFT_forward_selfenergy(self_t_one_dense, self_w_one_dense, 0., G0_t, G0_w); // NOT convolved with G0
    do_FFT_forward_selfenergy(conv_t, conv_w, 0., G0_t, G0_w);
    do_FFT_forward_selfenergy(self_t_dense, self_w, 0., G0_t, G0_w); // this is Selfenergy convolved with G0

    // Dyson
    for (size_t i = 0; i < number_of_frequencies; i++) {
        G_w[i] = (G0_w[i] * (self_w_one_dense[i] * G0_w[i] + self_w[i] + conv_w[i])
                  / (1. - (self_w_one_dense[i] * G0_w[i] + self_w[i] + conv_w[i])));
    }

    // backward fourier of the full green function
    do_FFT_backward_green(G_w, G_t_dense, G0_t);

    // coarse grain
    for (size_t i=0; i < number_of_times; i++) {
        G_t[i] = FFTgrid.quadratic_interpolation_mid(Time_self_grid[i], G_t_dense);
    }

    for (size_t i = 0; i < number_of_times; i++)
        Greenfun_t(mom_indx,i) = log(std::fabs(G_t[i]) + 1e-100); // if G_t changes sign due to error bar of roundof...

    // output
    for (size_t i = 0; i < number_of_times; i++) {
        double tau = Time_self_grid[i] ;
        double sb = (exp(mu * tau) * vertex_amplitude_sq * sqrt(M_PI * 2 * mass)
                     / (4*M_PI*M_PI) * sqrt(M_PI/omega_p) * erf(sqrt(omega_p*tau)));
        if (mom_indx == 0)
            os << tau << "\t" << bare_propagator(p_sq, mu, tau)
               << "\t" << self_t_one[i]/sqrt(tau)  << "\t" << sb
               << "\t" << -G_t[i] << "\t" << log(std::fabs(G_t[i]) + 1e-16)
               << "\t" << Greenfun_t(mom_indx,i) << "\n";
    }
    os << "\n\n";
}


bool bold_diagmc::boldify(std::ostream& os, const alps::results_type<diagmc>::type & results) {
#ifdef DEBUGMODE
    diagmc::test_conf();
#endif

    double Z_fake = 0.;
    size_t kmin = (update_prob[change_p] < 1e-10 ? p_init : 0);
    size_t kmax = (update_prob[change_p] < 1e-10 ? p_init+1 : number_of_momenta+1);
    for (size_t k=kmin; k < kmax; k++) {
        Momentum_t p;
        p(0) = Momentum_self_grid(k);
        for (size_t idim = 1; idim < dim; idim++)
            p(idim) = 0.;
        Z_fake += 1./ (dispersion(p, mu));
    }
      
    // Jackknife analyses
    using alps::accumulators::result_wrapper;
    const result_wrapper& normed_G0 = Z_fake * results["Greenfun_0"] / results["MCfake"];
    const result_wrapper& normed_SGo_1 = Z_fake * results["SGo_1"] / results["MCfake"];
    const result_wrapper& normed_SGo_other = Z_fake * results["SGo_other"] / results["MCfake"];
      
    std::string basename = alps::fs::remove_extensions(parameters["outputfile"]);
      
    std::ofstream fmom0(basename + ".green0");
    print_times_green(fmom0, 0, normed_G0);
    fmom0.close();
    double dG = 0.;
    std::vector<double> G_old(number_of_times);
  
    compute_self_dg1();
  
    for (size_t j=0; j < number_of_momenta+1; j++) {
        for (size_t i=0 ; i < number_of_times; i++)
            G_old[i] = exp(Greenfun_t(j,i));
        fourier_analysis(os, j, normed_SGo_1, normed_SGo_other);
        for (size_t i=0 ; i < number_of_times; i++) {
            dG += ((G_old[i] - exp(Greenfun_t(j,i)))
                   * (G_old[i] - exp(Greenfun_t(j,i))));
        }
    }
  
    print_update_statistics(os);
    dG /= number_of_times * number_of_momenta;
    std::cout << "# dG : "<< dG << "\n";
  
#ifdef DEBUGMODE
    std::cout << "# boldification: diagram_weight " << diagram_weight << "\n";
    diagram_weight = compute_configuration_energy();
    std::cout << "# boldification: diagram_weight " << diagram_weight << "\n";
    diagmc::test_conf();
#endif
    return std::fabs(dG) < convergence_threshold;
}


void bold_diagmc::broadcast_Greenfun() {
    broadcast(communicator, &Greenfun_t(0,0), get_Nelem_green(), 0);
}


void bold_diagmc::load(alps::hdf5::archive & ar) {
    Base::load(ar);

    ar["checkpoint/Greenfun_t"] >> Greenfun_t;

    ar["iteration_nr"] >> iteration_nr;
}


void bold_diagmc::save(alps::hdf5::archive & ar) const {
    Base::save(ar);
    
    ar["checkpoint/Greenfun_t"] << Greenfun_t;

    ar["iteration_nr"] << iteration_nr;
}


double bold_diagmc::full_propagator(const double p_sq,
                                    const double mu,
                                    const double dt) const
{
    // soft cutoff:
    return (p_sq >= max_momentum_sq
            ? bare_propagator(p_sq, mu, dt)
            : exp(biquadratic_interpolation_semi2(sqrt(p_sq), dt,
                                                  Momentum_self_grid,
                                                  Time_self_grid, Greenfun_t)));
    // hard cutoff:
    // return (p_sq >= max_momentum_sq
    //         ? 0.
    //         : exp(biquadratic_interpolation_semi2(sqrt(p_sq), dt,
    //                                               Momentum_self_grid,
    //                                               Time_self_grid, Greenfun_t)));
    // return bare_propagator(p_sq, mu, dt);
}

double bold_diagmc::full_propagator(const Momentum_t& p,
                                    const double mu,
                                    const double dt) const
{
    return full_propagator(p.r_sq(), mu, dt);
}


void bold_diagmc::NCA() {
    std::vector<double> self_t(number_of_times);
    std::vector<double> self_t_dense(number_of_frequencies);
    std::vector<double> G0_t(number_of_frequencies);
    std::vector<double> G_t(number_of_times);
    std::vector<double> G_t_dense(number_of_frequencies);
    std::vector<std::complex<double> > self_w(number_of_frequencies);
    std::vector<std::complex<double> > G0_w(number_of_frequencies);
    std::vector<std::complex<double> > G_w(number_of_frequencies);
    std::vector<double> omega_n(number_of_frequencies);

    std::vector<double> conv_t(number_of_frequencies);
    std::vector<std::complex<double> > conv_w(number_of_frequencies);

    for (size_t iter = 0; iter < 20; iter++) {
        std::cout << "# NCA iteration nr " << iter << "\n";


        compute_self_dg1();
        for (size_t j = 0; j < number_of_momenta+1; j++) {
            double p_sq = Momentum_self_grid(j) * Momentum_self_grid(j);

            for (size_t i=0; i < number_of_times; i++) {
                double tau = Time_self_grid[i];
                self_t[i] = get_self_one(Momentum_self_grid(j), tau) - get_self_one(0, tau) + Selfenergy_dg1(j, i) ;
            }

            for (size_t i = 0; i < number_of_frequencies; i++) {
                double tau = FFTgrid[i];
                G0_t[i] = -bare_propagator(p_sq, mu, tau);
                self_t_dense[i]= Time_self_grid.quadratic_interpolation_mid(tau, self_t);
                if (p_sq < 2 * mass *omega_p) {
                    double wp = sqrt(  omega_p - p_sq/ (2*mass)  );
                    conv_t[i] = vertex_amplitude_sq * sqrt(mass * M_PI) * exp(-dispersion(p_sq, mu) * tau) * pow(2*M_PI, -1.5) / wp * erf( wp * sqrt(tau)) ;
                }
                else {
                    double wp = sqrt(   p_sq/ (2*mass) - omega_p  );
                    conv_t[i] = vertex_amplitude_sq * sqrt(mass ) * pow(2*M_PI, -1.5) / wp * 2 * exp(-(omega_p - mu)*tau) * Faddeeva::Dawson(wp * sqrt(tau));
                }
                omega_n[i] = (i < number_of_frequencies/2 ? ( 2 * i ) * M_PI / max_time : - ( 2. * (number_of_frequencies -i) ) * M_PI / max_time) ;    // bosonic
                G0_w[i] = 1./(std::complex<double>(- dispersion(p_sq, mu),omega_n[i]) );
            }

            do_FFT_forward_selfenergy(self_t_dense, self_w, 0., G0_t, G0_w);
            do_FFT_forward_selfenergy(conv_t, conv_w, 0., G0_t, G0_w);

            // Dyson
            for (size_t i = 0; i < number_of_frequencies; i++) {
                G_w[i] = (G0_w[i] * (self_w[i] * G0_w[i] + conv_w[i])
                          / (1. - (self_w[i] * G0_w[i] + conv_w[i])));
            }

            // backward fourier of the full green function
            do_FFT_backward_green(G_w, G_t_dense, G0_t);

            // coarse grain
            for (size_t i=0; i < number_of_times; i++) {
                G_t[i] = FFTgrid.quadratic_interpolation_mid(Time_self_grid[i], G_t_dense);
            }
            for (size_t i = 0; i < number_of_times; i++)
                Greenfun_t(j,i) = log(fabs(G_t[i]) + 1e-100);
        }

        std::string basename = alps::fs::remove_extensions(parameters["outputfile"]);
        std::stringstream NCAname;
        NCAname << basename << ".NCA_" << iter;
        std::ofstream NCAout(NCAname.str());

        for (size_t i = 0; i < number_of_times; i++) {
            NCAout << Time_self_grid[i] << "\t";
            for (size_t j = 0; j < number_of_momenta+1; j++)
                NCAout << Greenfun_t(j,i) << "\t";
            NCAout << "\n";
        }
        NCAout.close();

        size_t itm1 = Time_self_grid.get_x_index(5.0);
        size_t itm2 = Time_self_grid.get_x_index(18.0);
        double x(0.), y(0.);
        size_t inc = 0;
        for (size_t i = itm1; i <= itm2; i++) {
            x += Time_self_grid[i];
            y += Greenfun_t(0,i);
            inc++;
        }
        double xm = x/inc;
        double ym = y/inc;
        double num(0.), denom(0.);
        for (size_t i = itm1; i <= itm2; i++) {
            num += (Time_self_grid[i] - xm) * (Greenfun_t(0,i) - ym);
            denom += (Time_self_grid[i] - xm) * (Time_self_grid[i] - xm);
        }
        double etmp = num/denom;
        double z = ym - etmp * xm;
        std::cout << "# Energy : " << etmp << "\t" << mu - etmp << "\t"
                  << z << "\t" << exp(z) << "\n";
    }
}


void bold_diagmc::compute_self_dg1() {
    // p = 0
    size_t Nk = (number_of_momenta+1)*10;
    double hk = (max_momentum / Nk);
    for (size_t i = 0; i < number_of_times; i++) {
        double tau = Time_self_grid[i];
        double s = (full_propagator(0, mu, tau) - bare_propagator(0, mu, tau)
                    + full_propagator(max_momentum_sq-0.00000001, mu, tau)
                    - bare_propagator(max_momentum_sq-0.00000001, mu, tau));
        s /= 2;
        for (size_t k =1; k < Nk; k++) {
            double q = k * hk ;
            double q_sq = q*q;
            s += full_propagator(q_sq, mu, tau) - bare_propagator(q_sq, mu, tau);
        }
        Selfenergy_dg1(0, i) = -vertex_amplitude_sq * exp(-omega_p * tau) / (2*M_PI*M_PI) * s * hk;
    }

    // other p
    for (size_t j =1; j < number_of_momenta+1; j++) {
        double p = Momentum_self_grid(j);
        for (size_t i = 0; i < number_of_times; i++) {
            double tau = Time_self_grid[i];
            double s = ((full_propagator(max_momentum_sq, mu, tau)
                         - bare_propagator(max_momentum_sq,mu,tau))
                        * max_momentum
                        * log(std::fabs((p/max_momentum + 1.)
                                        / (p/max_momentum - 1.))));
            s /= 2;
            if (j == number_of_momenta) s = 0.;
            for (size_t k = 1; k < Nk; k++) {
                double q = k * hk ;
                double q_sq = q*q;
                s += (is_not_close(p*p, q_sq, 1e-8)
                      ? ((full_propagator(q_sq, mu, tau) - bare_propagator(q_sq, mu, tau))
                         * q * log(fabs((p+q)/(p-q))))
                      : 0.);
            }
            Selfenergy_dg1(j, i) = (-vertex_amplitude_sq * exp(-omega_p * tau)
                                    / (4*M_PI*M_PI * p) * s * hk);
        }
    }
}


void bold_diagmc::test_conf(const Diagram_type& d) const {
    Base::test_conf(d);
    if (order > 0) {
        Diagram_type::const_iterator it = d.begin();
        Diagram_type::const_iterator itend = d.end(); --itend;
        Diagram_type::const_iterator itn = it; ++itn;
        for (; it!=itend; ++it, ++itn) {
            if (order > 1) {
                if ( (it->link())->time() > it->time()) {
                    if (!is_not_close( (it->in()-it->link()->out()).r_sq(), 0., 1e-8))
                        throw invalid_configuration("Diagram is bold reducible"); // not a complete check
                } else {
                    // check the out-line against any previous moment
                    for (Diagram_type::const_iterator it_test = diagram.begin();
                         it_test != it; ++it_test)
                    {
                        if (!is_not_close((it->out()-it_test->in()).r_sq(), 0., 1e-8))
                            throw invalid_configuration("Diagram is bold "
                                                        "reducible (slow test)");
                        if (!is_not_close((it->out()-it_test->out()).r_sq(), 0., 1e-8))
                            throw invalid_configuration("Diagram is bold "
                                                        "reducible (slow test)");
                    }
                }
            }
        }
    }
}


double bold_diagmc::compute_configuration_energy() const {
    double weight = 1.;
    if (order == 0)
        return bare_propagator(diagram_p, mu, diagram_time);
    Diagram_type::const_iterator it = diagram.begin();
    Diagram_type::const_iterator itend = diagram.end(); --itend;
    Diagram_type::const_iterator itn = it; ++itn;
    for (; it!=itend; ++it, ++itn) {
        weight *= full_propagator(it->out(), mu, itn->time() - it->time()) ;
        std::cout << "# Weight : " << it->time() << "\t" << weight << "\n";
        if (it->link()->time() > it->time())
            weight *= exp(-omega_p * (it->link()->time() - it->time()))
                / (pow(2*M_PI, dim)) * vertex_amplitude_sq  / it->v().r_sq();
        std::cout << "# Weight : " << it->time() << "\t" << weight << "\n";
    }
    weight *= bare_propagator(itend->out(), mu, diagram_time - itend->time()); // G0 part
    std::cout << "# Weight (end) : " << diagram_time << "\t"
              << diagram_time - itend->time() << "\t"
              << bare_propagator(itend->out(), mu, diagram_time - itend->time())
              << "\t" << weight << "\n";
    return weight;
}
