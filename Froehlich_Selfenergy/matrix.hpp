// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <utility>
#include <vector>
#include <stdexcept>

#include <alps/hdf5/archive.hpp>
#include <alps/hdf5/vector.hpp>

namespace detail {
    template <typename T>
    T get_ar(alps::hdf5::archive& ar, const std::string& path) {
        T val;
        ar[path] >> val;
        return val;
    }
}

template <typename T>
class Matrix {
private:
    const size_t mRow, mCol, mZ, mSize;
    std::vector<T> mCell;

public:
    Matrix (size_t x, size_t y = 1, size_t z = 1)
        : mRow(x), mCol(y), mZ(z), mSize(mRow * mCol * mZ),
          mCell(mSize) {}

    Matrix (alps::hdf5::archive& ar)
        : mRow(detail::get_ar<size_t>(ar, "dim/row")),
          mCol(detail::get_ar<size_t>(ar, "dim/col")),
          mZ(detail::get_ar<size_t>(ar, "dim/z")),
          mSize(detail::get_ar<size_t>(ar, "dim/size")),
          mCell(detail::get_ar<std::vector<T>>(ar, "data")) {
        if (mRow * mCol * mZ != mSize || mCell.size() != mSize) {
            throw std::runtime_error("inconsistent data in HDF5 serialization of Matrix");
        }
    }

    Matrix& operator= (const T& val) {
        for (size_t i = 0; i < mSize; i++)
            mCell[i] = val;
        return *this;
    }

    Matrix& operator+= (const Matrix& rhs) {
        if (rhs.mSize != mSize) {
            throw std::invalid_argument("RHS matrix has incompatible dimensions");
        }
        for (size_t i = 0; i < mSize; i++) {
            mCell[i] += rhs.mCell[i];
        }
        return *this;
    }

    Matrix& operator-= (const Matrix& rhs) {
        if (rhs.mSize != mSize) {
            throw std::invalid_argument("RHS matrix has incompatible dimensions");
        }
        for (size_t i = 0; i < mSize; i++) {
            mCell[i] -= rhs.mCell[i];
        }
        return *this;
    }

    Matrix& operator*= (const T& alpha) {
        for (size_t i = 0; i < mSize; i++)
            mCell[i] *= alpha;
        return *this;
    }

    Matrix& operator/= (const T& alpha) {
        for (size_t i = 0; i < mSize; i++)
            mCell[i] /= alpha;
        return *this;
    }

    friend std::ostream &operator<< (std::ostream &os, const Matrix& rhs) {
        for (size_t i = 0; i < rhs.mSize; i++)
            os << rhs.mCell[i] << "\t";
        os << "\n";
        return os;
    };

    friend std::istream& operator>>(std::istream& is, Matrix& rhs) {
        for (size_t i = 0; i < rhs.mSize; i++)
            is >> rhs.mCell[i];
        return is;
    };

    // save to HDF5
    void save(alps::hdf5::archive& ar) const {
        ar["dim/row"] << mRow;
        ar["dim/col"] << mCol;
        ar["dim/z"] << mZ;
        ar["data"] << mCell;
    }

    // load from HDF5
    void load(alps::hdf5::archive& ar) {
        size_t x, y, z;
        std::vector<T> data;
        ar["dim/row"] >> x;
        ar["dim/col"] >> y;
        ar["dim/z"] >> z;
        ar["data"] >> data;
        if (x * y * z != mSize || data.size() != mSize) {
            throw std::invalid_argument("loaded matrix has incompatible dimensions");
        }
        mCell = std::move(data);
    }

    size_t size() const {return mSize;}
    size_t dim1() const {return mRow;}
    size_t dim2() const {return mCol;}
    size_t dim3() const {return mZ;}


    T& operator() (const size_t x, const size_t y) {
        return mCell[x*mCol +y];
    }

    const T& operator() (const size_t x, const size_t y) const {
        return mCell[x*mCol +y];
    }

    const T& operator() (const size_t x, const size_t y, const size_t z) const {
        return mCell[z + mZ*(y + x*mCol)];
    }

    T& operator() (const size_t x, const size_t y, const size_t z) {
        return mCell[z + mZ*(y + x*mCol)];
    }

    std::vector<T>& data () {
        return mCell;
    }

    const std::vector<T>& data () const {
        return mCell;
    }
};
