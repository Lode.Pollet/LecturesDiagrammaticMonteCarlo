// Lecture Notes on Diagrammatic Monte Carlo
// Copyright (C) 2017  Jonas Greitemann and Lode Pollet

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "diagmc.hpp"


void diagmc::update() {
    reset_hists();
    for (size_t j = 0; j < N_loop; ++j) {
        for (size_t i = 0; i < N_hist; ++i) {
            double q = random();
            if (order == 0) { // fake diagram updates only
                try_log_update<from_fake, diagmc, &diagmc::FROM_FAKE>(q)
                    || try_log_update<change_p, diagmc, &diagmc::CHANGE_P>(q)
                    || try_log_update<change_time, diagmc, &diagmc::CHANGE_TIME>(q);
            } else { // real diagram updates only
                try_log_update<insert, diagmc, &diagmc::INSERT>(q)
                    || try_log_update<remove, diagmc, &diagmc::REMOVE>(q)
                    || try_log_update<dress, diagmc, &diagmc::DRESS>(q)
                    || try_log_update<undress, diagmc, &diagmc::UNDRESS>(q)
                    || try_log_update<swap, diagmc, &diagmc::SWAP>(q)
                    || try_log_update<extend, diagmc, &diagmc::EXTEND>(q)
                    || try_log_update<to_fake, diagmc, &diagmc::TO_FAKE>(q);
            }
#ifdef DEBUGMODE
            print_diagram(diagram);
            try {
                test_conf(diagram);
            }
            catch (const char* e) {
                std::cerr << e << std::endl;
                print_diagram(diagram);
                exit(1);
            }
#endif
        }
        fill_hists();
    }
}

statistics_tag diagmc::INSERT() {
#ifdef DEBUGMODE
    if (order == 0)
        std::cerr << "\nINSERT in fake diagram?\n";
#endif
    if (order >= max_order-1)
        return impossible;
    size_t nr_int = 2*order-1;
    size_t n_int = static_cast<size_t>(random()*(nr_int));

    Diagram_type::iterator it_left = diagram.begin();
    for (size_t j=0; j < n_int; j++)
        ++it_left;
    double t_init = it_left->time();
    Diagram_type::iterator it_right = it_left; ++it_right;
    double t_fin = it_right->time();
    double tau_intv = t_fin - t_init;
    double t1 = random() * tau_intv + t_init;
    double t2 = t1 -  log(random())/omega_p;
    if (t2 > t_fin)
        return impossible;
    double dt = t2 - t1;
    Momentum_t q;
    std::normal_distribution<double> gauss(0, sqrt(mass/dt));
    for (int i = 0; i < dim; i++)
        q(i) = gauss(rng);
    Momentum_t p_ext = it_left->out();
    Momentum_t p_prime = p_ext - q;

    double wx = bare_propagator(p_ext, mu, dt); // BOLD makes no sense for this update so let us not consider it
    double wy = bare_propagator(p_prime, mu, dt)
        * exp(-omega_p * dt) * vertex_amplitude_sq / q.r_sq()
        / pow(2*M_PI, dim);

    double pxy = update_prob[insert] /(2*order - 1.)
        * omega_p * exp(-omega_p * dt)
        * exp ( - q.r_sq() * dt / (2*mass))
        * pow(2 * M_PI * mass/dt, -dim/2.) / tau_intv;
    double pyx = update_prob[remove] /(2.*order);

    double ratio = wy * pyx / wx / pxy;

    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "# INSERT accepted; ratio : " << ratio <<  "\n";
        char ch; std::cin >> ch;
#endif
        vertex_type v2(t2, p_prime, p_ext, q);
        vertex_type v1(t1, p_ext, p_prime, q);
        Diagram_type::iterator it2 = diagram.insert(it_right, v2);
        Diagram_type::iterator it1 = diagram.insert(it2, v1);
        it1->link(it2);
        it2->link(it1);
        order++;
#ifdef DEBUGMODE
        diagram_weight *= wy/wx;
#endif
        return accepted;
    } else {
        return rejected;
    }
}

statistics_tag diagmc::REMOVE() {
#ifdef DEBUGMODE
    if (order == 0)
        std::cerr << "\nREMOVE in fake diagram?\n";
#endif
    if (order == 1)
        return impossible; // self-energy should contain at least one wiggly line
#ifdef DEBUGMODE
    std::cout << "\n# Welcome to REMOVE.\n";
#endif
    size_t n = static_cast<size_t>(random() * (2*order - 2));
    Diagram_type::iterator it1 = diagram.begin();
    ++it1;
    for (size_t j=0; j < n; j++)
        ++it1;
    Diagram_type::iterator it2 = it1;
    ++it2;
    if (it1->link() != it2)
        return impossible; // not a removable arc
    double dt = it2->time() - it1->time();
    Momentum_t p_ext = it1->in();
    Momentum_t q = it1->v();
    Momentum_t p_prime = it1->out();
    double wy = bare_propagator(p_ext, mu, dt); // BOLD makes no sense for this update so let us not consider it
    double wx = bare_propagator(p_prime, mu, dt)
        * exp(-omega_p * dt) * vertex_amplitude_sq / q.r_sq()
        / pow(2*M_PI, dim);
    Diagram_type::iterator it_left = it1; --it_left;
    Diagram_type::iterator it_right = it2; ++it_right;
    double tau_intv = it_right->time() - it_left->time();

    double pyx = update_prob[insert] / (2*order - 3.) * omega_p
        * exp(-omega_p * dt) * exp ( - q.r_sq() * dt / (2*mass))
        * pow(2 * M_PI * mass/dt, -dim/2.) / tau_intv;
    double pxy = update_prob[remove] / (2*order - 2.);
    double ratio = wy * pyx / wx / pxy;
#ifdef DEBUGMODE
    std::cout << "# REMOVE ratio : " << ratio << "\t" << 1./ratio << "\n";
    char ch; std::cin >> ch;
#endif
    if (Metropolis(ratio)) {
        diagram.erase(it2);
        diagram.erase(it1);
        order--;
#ifdef DEBUGMODE
        diagram_weight *= wy/wx;
#endif
        return accepted;
    } else {
        return rejected;
    }
}


statistics_tag diagmc::DRESS() { // insert new arc dressing one existing vertex
#ifdef DEBUGMODE
    if (order == 0)
        std::cerr << "\nDRESS in fake diagram?\n";
#endif
    if (order >= max_order-1)
        return impossible;


    size_t n_v = static_cast<size_t>(random()*(2*order - 1)); // twice as many vertices as arcs, excluding first one
    Diagram_type::iterator itm = diagram.begin(); ++itm; // vertex in the middle
    for (size_t j=0 ; j < n_v; j++) ++itm;

    Diagram_type::iterator it_left  = itm; --it_left;
    Diagram_type::iterator it_right = itm; ++it_right; // it is possible that it_right is diagram.end()  !!!
    double dt1 = itm->time() - it_left->time();
    double t1 = it_left->time() + random() * dt1; // time of vertex on the left of the new arc

    double dt2 = (it_right == diagram.end()
                  ? diagram_time - itm->time()
                  : it_right->time() - itm->time());
    double t2 = itm->time() + random() * dt2; // time of vertex on the right of new arc

    double dt = t2 - t1; // duration of new arc
#ifdef DEBUGMODE
    std::cout << "# DRESS diagram_time : " << diagram_time << "\n";
    std::cout << "# DRESS order , n_v " << order << " " << n_v << " itm, it_left, it_right, t1, t2 : " << itm->time() << " " << it_left->time() << " " << (it_right == diagram.end() ? diagram_time : it_right->time()) << " "  << t1 << " " << t2 << "\n";
#endif
    // generate new momentum
    Momentum_t q;
    std::normal_distribution<double> gauss(0, sqrt(mass/dt));
    for (int i = 0; i < dim; i++)
        q(i) = gauss(rng);


    Momentum_t p_a = itm->in() - q;
    Momentum_t p_b = itm->out() - q;

    double wx = bare_propagator(itm->in(), mu, itm->time() - t1)
        * bare_propagator(itm->out(), mu, t2 - itm->time());
    double wy = exp(-omega_p * dt) * bare_propagator(p_a, mu, itm->time() - t1)
        * bare_propagator(p_b, mu, t2 - itm->time())
        * vertex_amplitude_sq / q.r_sq() * pow (2 *M_PI, -dim*1.);
    double pxy = update_prob[dress] / pow(sqrt( 2 * M_PI * mass/dt), dim)
        * exp(- q.r_sq() * dt / (2*mass)) / dt1 / dt2 / (2*order - 1.);
    double pyx = update_prob[undress] / (2. * order );
    double ratio = wy*pyx/wx/pxy;

#ifdef DEBUGMODE
    std::cout << "# DRESS wx = " << wx << "\t"
              << bare_propagator(itm->in(), mu, dt2) << "\t"
              << bare_propagator(itm->out(), mu, t2 - itm->time()) << "\n";
    std::cout << "# DRESS wy = " << wy << "\t" << exp(-omega_p * dt) << "\t"
              << bare_propagator(p_a, mu, itm->time() - t1) << "\t"
              << bare_propagator(p_b, mu, t2 - itm->time()) << "\t"
              << vertex_amplitude_sq / q.r_sq() * pow (2 *M_PI, -dim*1.) << "\n";
    std::cout << "# DRESS pxy = " << pxy << "\n";
    std::cout << "# DRESS pyx = " << pyx << "\t" << order << "\t"
              << dt1 << "\t" << dt2 <<  "\n";
    std::cout << "# DRESS ratio = " << ratio << "\n";
#endif

    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "# DRESS accepted, ratio = " << ratio << "\n";
        std::cout << "# DRESS old diagram weight " << diagram_weight
                  << "\t wy,wx, wy/wx : " << wy << "\t" << wx << "\t"
                  << wy/wx << "\n";
#endif
        vertex_type v2(t2, p_b, itm->out(), q);
        vertex_type v1(t1, itm->in(), p_a, q);
        Diagram_type::iterator it2 = diagram.insert(it_right, v2);
        Diagram_type::iterator it1 = diagram.insert(itm, v1);
        itm->out(p_b);
        itm->in(p_a);
        it1->link(it2);
        it2->link(it1);
#ifdef DEBUGMODE
        diagram_weight *= wy / wx;
#endif
        order++;
        return accepted;
    } else {
        return rejected;
    }
}

statistics_tag diagmc::UNDRESS() {

    if (order == 0) std::cerr << "UNDRESS in fake diagram?\n";
    if (order == 1) return impossible; // self-energy should contain at least one wiggly line

    size_t n_v = static_cast<size_t>(random()*(2*order - 2)); // twice as many orders (# vertices) as arcs (order), excluding first and last one
    Diagram_type::iterator itm = diagram.begin(); ++itm; // vertex in the "middle"
    for (size_t j=0 ; j < n_v; j++)
        ++itm;
    Diagram_type::iterator ite = diagram.end();
    --ite;
    if (itm == ite) {
        std::cerr << "itm == ite in UNDRESS???\n";
    }
    Diagram_type::iterator it1 = itm; --it1; // left vertex
    Diagram_type::iterator it2 = itm; ++it2; // right vertex

    if (it2 != it1->link())
        return impossible; // not removable
    if (it1 == diagram.begin())
        return impossible; // not removable

    Diagram_type::iterator it_left = it1; --it_left;
    Diagram_type::iterator it_right = it2; ++it_right;
    double dt1 = itm->time() - it_left->time();
    double dt2 = (it_right == diagram.end()
                  ? diagram_time - itm->time()
                  : it_right->time() - itm->time());

    Momentum_t p_left = it1->in();
    Momentum_t p_right = it2->out();
    Momentum_t p_a = it1->out();
    Momentum_t p_b = it2->in();
    Momentum_t q = it1->v();

    double dt = it2->time() - it1->time();
    double wx = exp(-omega_p * dt)
        * bare_propagator(p_a, mu, itm->time() - it1->time())
        * bare_propagator(p_b, mu, it2->time() - itm->time())
        * vertex_amplitude_sq / q.r_sq() * pow (2 *M_PI, -dim*1.); // weight of arc and 2 propagators
    double wy = bare_propagator(p_left, mu, itm->time() - it1->time())
        * bare_propagator(p_right, mu, it2->time() - itm->time()); // modified propagator weight
    double pxy = update_prob[undress]/ (2*order - 2.);
    double pyx = update_prob[dress] / pow(sqrt( 2 * M_PI * mass/dt), dim)
        * exp(- q.r_sq() * dt / (2*mass)) / dt1 / dt2 / (2*order - 3.);
    double ratio = wy * pyx/wx/pxy;

#ifdef DEBUGMODE
    std::cout << "# UNDRESS itm, it1, it2 : " << itm->time() << "\t"
              << it1->time() << "\t" << it2->time() << "\n";
    std::cout << "# UNDRESS wx = " << wx << "\n";
    std::cout << "# UNDRESS wy = " << wy << "\n";
    std::cout << "# UNDRESS pxy = " << pxy << "\n";
    std::cout << "# UNDRESS pyx = " << pyx << "\t" << order << "\t"
              << dt1 << "\t" << dt2 << "\n";
    std::cout << "# UNDRESS ratio = " << ratio << "\t" << 1./ratio << "\n";
#endif

    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "# UNDRESS accepted, ratio = " << ratio << "\n";
#endif
        itm->in(p_left);
        itm->out(p_right);
        diagram.erase(it2);
        diagram.erase(it1);
        order--;
#ifdef DEBUGMODE
        diagram_weight *= wy/wx;
#endif
        return accepted;
    } else {
#ifdef DEBUGMODE
        std::cout << "UNDRESS rejection\n";
#endif
        return rejected;
    }
}


statistics_tag diagmc::SWAP() {
#ifdef DEBUGMODE
    if (order == 0) std::cerr << "\nSWAP in fake diagram?\n";
#endif
    if (order < 2)
        return impossible; // pointless

    // find arbitrary 2 neighbors;
    int n_v = static_cast<int>(random()*(2*order-1));
    Diagram_type::iterator it1 = diagram.begin();
    for (int i = 0; i < n_v; i++) ++it1;
    Diagram_type::iterator it2 = it1;
    ++it2;
    if (it1->link() == it2)  return impossible;
    double dt = it2->time() - it1->time();
    Momentum_t p_old = it1->out();
    Diagram_type::iterator it_link1 = it1->link();
    Diagram_type::iterator it_link2 = it2->link();

#ifdef DEBUGMODE
    std::cout << "# welcome to SWAP : " << it1->time() << "\t" << it2->time()
              << "\t" << it_link1->time() << "\t" << it_link2->time() << "\n";
    //print_diagram(diagram);
#endif
    Momentum_t p1 = (it_link2->time() > it2->time()
                     ? it1->in() - it2->v()
                     : it1->in() + it2->v());

    if ((p1 - diagram_p).r_sq() < 1e-10) {
        // reducible diagram in SWAP
        return impossible;
    }

    double delta_old = (std::fabs(it1->time() - it_link1->time())
                        + std::fabs(it2->time() - it_link2->time()));
    double delta_new = (std::fabs(it1->time() - it_link2->time())
                        + std::fabs(it2->time() - it_link1->time()));

    double ratio = bare_propagator(p1, mu, dt) * exp(-omega_p * delta_new )
        / bare_propagator(p_old, mu, dt) / exp(-omega_p * delta_old);

#ifdef DEBUGMODE
    std::cout << "\n#Ratio in SWAP : " << ratio << "\n";
#endif
    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "\n# Swap accepted.\n";
#endif
        it1->out(p1);
        it2->in(p1);
        it1->v(it_link2->v());
        it2->v(it_link1->v());
        it1->link(it_link2);
        it2->link(it_link1);
        it_link2->link(it1);
        it_link1->link(it2);
#ifdef DEBUGMODE
        diagram_weight *= ratio;
#endif
        return accepted;
    } else {
#ifdef DEBUGMODE
        std::cout << "\n# Swap Rejection.\n";
#endif
        return rejected;
    }

}

statistics_tag diagmc::EXTEND() {
#ifdef DEBUGMODE
    if (order == 0)
        std::cerr << "\nEXTEND in fake diagram?\n";
#endif
    Diagram_type::iterator it = diagram.end();
    --it;
    double t0 = it->time();
    double En = dispersion(diagram_p, mu);
    double tnew = t0 -log(random())/En;
    //if (tnew > max_time) return impossible;
#ifdef DEBUGMODE
    diagram_weight *= bare_propagator(diagram_p, mu, tnew - t0)
        / bare_propagator(diagram_p, mu, diagram_time - t0);
#endif
    diagram_time = tnew;
    return accepted;
}

statistics_tag diagmc::TO_FAKE() {
#ifdef DEBUGMODE
    if (order == 0) std::cerr << "\nTO_FAKE in fake diagram?\n";
#endif
    if (order > 1) return impossible;
    Diagram_type::iterator it = diagram.begin();
    Momentum_t p_prime = it->out();
    Momentum_t q = it->v();
    Diagram_type::iterator itn = it; ++itn;
    double dt = itn->time() - it->time();

    double wy = bare_propagator(diagram_p, mu, diagram_time);
    double wx = vertex_amplitude_sq / q.r_sq() * exp(-omega_p * dt)
        * pow(2*M_PI, -dim*1.) * bare_propagator(p_prime, mu, dt)
        * bare_propagator(diagram_p, mu, diagram_time - itn->time());
    double pyx = update_prob[from_fake] * exp( - q.r_sq() * dt / (2*mass))
        * pow(dt / (2*mass*M_PI), dim*0.5) * omega_p * exp(-omega_p * dt);
    double pxy = update_prob[to_fake];
    double ratio = wy * pyx / wx / pxy;

#ifdef DEBUGMODE
    std::cout << "# TO_FAKE : " << pow(sqrt(2.*mass*omega_p)/M_PI, dim)
              << "\tomega_p : " << omega_p << "\tdiagram_time : "
              << diagram_time << "\n";
    std::cout << "# TO_FAKE : propagator p_prime - p : "
              << bare_propagator(p_prime, mu, dt) << "\t"
              << bare_propagator(diagram_p, mu, dt) << "\n";
    std::cout << "# TO_FAKE : q " << q << "\t |q|^2 : " << q.r_sq()
              << "\tvertex ampl sq : " << vertex_amplitude_sq << "\n";
    std::cout << "# TO_FAKE : ratio " << 1./ratio << "\t" << ratio << "\n";
#endif
    if (Metropolis(ratio)) {
#ifdef DEBUGMODE
        std::cout << "# TO_FAKE accepted : " << it->time() << "\t"
                  << itn->time() << "\n";
#endif
        diagram.erase(itn);
        diagram.erase(it);
        order = 0;
#ifdef DEBUGMODE
        diagram_weight *= wy/wx;
#endif
        return accepted;
    } else {
        return rejected;
    }
}

statistics_tag diagmc::FROM_FAKE() {
#ifdef DEBUGMODE
    if (order != 0)
        std::cerr << "\nFROM_FAKE in real diagram?\n";
#endif
    double dt = -log(random())/omega_p;
    double t2 = dt;
    if (t2 > diagram_time)
        return impossible;
    double t1 = 0.; // selfenergy
    Momentum_t q;
    std::normal_distribution<double> gauss(0, sqrt(mass/dt) );
    for (int i = 0; i < dim; i++)
        q(i) = gauss(rng);

    Momentum_t p_ext = diagram_p;
    Momentum_t p_prime = p_ext - q;

    double wx = bare_propagator(diagram_p, mu, diagram_time);
    double wy = vertex_amplitude_sq / q.r_sq() * exp(-omega_p * dt)
        * pow(2*M_PI, -dim*1.) * bare_propagator(p_prime, mu, dt)
        * bare_propagator(diagram_p, mu, diagram_time - t2);
    double pxy = update_prob[from_fake] * exp( - q.r_sq() * dt / (2*mass))
        * pow(dt / (2*mass*M_PI), dim*0.5) * omega_p * exp(-omega_p * dt);
    double pyx = update_prob[to_fake];
    double ratio = wy * pyx / wx / pxy;
#ifdef DEBUGMODE
    std::cout << "# FROM_FAKE : " << pow(sqrt(2.*mass*omega_p)/M_PI, dim)
              << "\tomega_p : " << omega_p << "\tdiagram_time : "
              << diagram_time << "\n";
    std::cout << "# FROM_FAKE : propagator p_prime - p : "
              << bare_propagator(p_prime, mu, dt) << "\t"
              << bare_propagator(diagram_p, mu, dt) << "\tdt : " << dt << "\n";
    std::cout << "# FROM_FAKE : q " << q << "\t |q|^2 : " << q.r_sq()
              << "\tvertex ampl sq : " << vertex_amplitude_sq << "\n";

    std::cout << "\n# FROM_FAKE ratio : " << ratio << "\n";
#endif

    if (Metropolis(ratio)) {
        vertex_type v1(t1, diagram_p, p_prime, q);
        vertex_type v2(t2, p_prime, diagram_p, q);
        Diagram_type::iterator it;
        Diagram_type::iterator itn;
        itn = diagram.insert(diagram.end(), v2);
        it = diagram.insert(itn, v1);
        itn->link(it);
        it->link(itn);
        order = 1;
#ifdef DEBUGMODE
        diagram_weight *= wy/wx;
#endif
        return accepted;
    } else {
        return rejected;
    }
}

statistics_tag diagmc::CHANGE_P() {
#ifdef DEBUGMODE
    if (order != 0) std::cerr << "\nCHANGE_P in real diagram?\n";
#endif
    Momentum_t pnew;
    size_t indx = static_cast<size_t>(random() * Momentum_self_grid.get_N() );
    pnew(0) = Momentum_self_grid(indx);
    for (size_t idim = 1; idim < dim; idim++) pnew(idim) = 0.;
    double ratio = bare_propagator(pnew, mu, diagram_time)
        / bare_propagator(diagram_p, mu, diagram_time);
    if (Metropolis(ratio)) {
        diagram_p = pnew;
        diagram_p_index = indx;
#ifdef DEBUGMODE
        diagram_weight *= ratio;
#endif
        return accepted;
    } else {
        return rejected;
    }
}

statistics_tag diagmc::CHANGE_TIME() {
#ifdef DEBUGMODE
    if (order != 0) std::cerr << "\nCHANGE_TIME in real diagram?\n";
#endif
    double En = dispersion(diagram_p, mu);
    double tnew = -log(random())/En;
    //if (tnew > max_time) return impossible;
    if (tnew < 1e-14) return impossible;
#ifdef DEBUGMODE
    diagram_weight *= bare_propagator(diagram_p, mu, tnew)
        / bare_propagator(diagram_p, mu, diagram_time);
#endif
    diagram_time = tnew;
    return accepted;
}
